ng build --prod --deploy-url /app/

# Dependencies

pydantic-sqlalchemy==0.0.8.post1
SQLAlchemy==1.3.20
pydantic==1.7.1
fastapi==0.61.1
starlette==0.13.6
aiofiles==0.6.0
bcrypt==3.2.0
python-jose
uvicorn
passlib
python-multipart

## yarn 

yarn add @videojs/vhs-utils@2.2.1

## passwords

you must create a file named *pass* in `backend/` with the following format :

```
username password
username password
username password
...
```

## Change URL

you must change the url with your url server in the following files :

* **map.component.scss**
* **api.service.ts**