from pathlib import Path
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from typing import List
from sqlalchemy.sql.expression import true
from starlette.responses import JSONResponse, Response
from core.db.models.tile import PydanticTileFront
from core.controllers.tile import TileController
from core.dbmanager import DatabaseManager

router = APIRouter()

@router.get("/tile/all", response_model=List[PydanticTileFront], response_model_exclude_unset=True)
async def get_all_tiles(db : Session = Depends(DatabaseManager().get_session)):
    tiles = TileController.all(db)
    return JSONResponse(content={"tiles":[PydanticTileFront.from_orm(tile).__dict__ for tile in tiles if tile.state == "completed"]})