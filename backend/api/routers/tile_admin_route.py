import requests
import uuid
from pathlib import Path
from fastapi import APIRouter, Depends, Form, UploadFile, File
from sqlalchemy.orm import Session
from typing import List
from fastapi import HTTPException, status
from fastapi import FastAPI, File
from sqlalchemy.sql.expression import true
from starlette.responses import JSONResponse, Response
from core.auth import get_current_user
from core.db.models.tile import PydanticTileFront, PydanticTile, PydanticTileCreate, PydanticTileUpdate, PydanticTileDelete, PydanticTileUpdateUrl
from core.db.models.user import PydanticUser
from core.controllers.tile import TileController
from core.dbmanager import DatabaseManager

import images

router = APIRouter()

@router.post('/tile/create', response_model_exclude_unset=True)
async def create_tile(  
    centerLat = Form(...), centerLng = Form(...), file = File(...), 
    db : Session = Depends(DatabaseManager().get_session),
    current_user: PydanticUser = Depends(get_current_user)):

    tile = PydanticTileCreate(centerLat=centerLat, centerLng=centerLng,)
    new_tile = TileController.create(db, tile, file, current_user)
    if(new_tile):
        return JSONResponse(content={"tile_id":new_tile.id,"message":"Image successfully upload"})
    raise HTTPException(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        detail=f"Error while creating new tile"
    )

@router.post('/tile/update', response_model_exclude_unset=True)
async def update_tile(tile_update: PydanticTileUpdate, 
    db : Session = Depends(DatabaseManager().get_session),
    current_user: PydanticUser = Depends(get_current_user)):
    tile = TileController.get(db, tile_update.id)

    if tile.user_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Not your tile"
        )

    if tile.state != "waiting_update":
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Bad tile ID. Stop this."
        )
    new_tile = TileController.update(db, tile_update, tile)
    if new_tile:
        return JSONResponse(content={"tile":PydanticTile.from_orm(new_tile).__dict__})
    raise HTTPException(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        detail=f"Error while creating this collaborator"
    )

@router.post('/tile/updateurl', response_model_exclude_unset=True)
async def update_tile_url(tile_url: PydanticTileUpdateUrl, 
    db : Session = Depends(DatabaseManager().get_session),
    current_user: PydanticUser = Depends(get_current_user)):
    tile = TileController.get(db, tile_url.id)

    if tile.user_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Not your tile"
        )

    #Check state of the tile asked...
    if tile.state != "waiting_video_url":
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Bad tile ID. Stop this."
        )

    if tile_url.videoUrl == "" or tile_url.videoUrl == None: 
        TileController.updateUrl(db, None, None, None, tile)
        return JSONResponse(content={"skipped":True})

    #Check if it's a real peertube link
    splited = tile_url.videoUrl.split("://")
    protocol = splited[0]
    url = tile_url.videoUrl
    if len(splited)!=1:
        url = splited[1]
    if "/videos/watch/" in url:
        splittedUrl = url.split("/")
        peertubeAddr = splittedUrl[0]
        videoIdSplitted = url.split("/videos/watch/")
        if len(videoIdSplitted) > 1:
            videoId = videoIdSplitted[1]
            previewAddr = peertubeAddr+"/lazy-static/previews/"+videoId+".jpg"
            response = requests.get(protocol+"://"+previewAddr, stream=True)
            if response.status_code == 200:
                uid = str(uuid.uuid4())
                preview_file_name = "preview_"+uid+"_"+videoId+".jpg"
                with open(str(images.__path__[0])+"/"+preview_file_name, "wb") as f:
                    for chunck in response:
                        f.write(chunck)
                new_tile = TileController.updateUrl(db, protocol+"://"+peertubeAddr, videoId, preview_file_name, tile)
                return JSONResponse(content={"previewVideoUrl":preview_file_name, "imageUrl":tile.imageName})
    raise HTTPException(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        detail="Not a valid peertube video URL"
    )


@router.post('/tile/delete', response_model_exclude_unset=True)
async def delete_tile(tile_delete: PydanticTileDelete, 
    db : Session = Depends(DatabaseManager().get_session),
    current_user: PydanticUser = Depends(get_current_user)):
    tile = TileController.get(db, tile_delete.id)
    if tile.user_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Not your tile"
        )

    if(TileController.delete(db, tile_delete)):
        return JSONResponse()
    raise HTTPException(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        detail=f"An error occured"
    )

@router.get("/tile/mine", response_model=List[PydanticTileFront], response_model_exclude_unset=True)
async def get_my_tiles(db : Session = Depends(DatabaseManager().get_session), current_user: PydanticUser = Depends(get_current_user)):
    tiles = TileController.mine(db, current_user)
    return JSONResponse(content={"tiles":[tile.id for tile in tiles]})