import core.auth as auth

from fastapi.security import OAuth2PasswordRequestForm
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.responses import JSONResponse
from core.db.models.token import PydanticToken
from core.dbmanager import DatabaseManager
from sqlalchemy.orm import Session

router = APIRouter()

@router.post("/login", response_model=PydanticToken)
async def token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(DatabaseManager().get_session)):
    user = await auth.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    
    headers = {"Access-Control-Allow-Origin":"*"}
    token = await auth.generate_access_token(user)
    return JSONResponse(content=token.__dict__, headers=headers)