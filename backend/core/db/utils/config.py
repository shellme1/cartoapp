#This file should be modify depending of the server configuration
DATABASE_URI = f"sqlite:///./app.db?check_same_thread=False"

#Change these parameters also in the frontend
SIZE_LAT = 0.021
SIZE_LNG = 0.0297

WIDTH = 1000
HEIGHT = 1404
PIXEL_LAT = SIZE_LAT/WIDTH
PIXEL_LNG = SIZE_LNG/HEIGHT