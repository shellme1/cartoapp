from passlib.context import CryptContext
from core.db.models.user import User
from core.dbmanager import DatabaseManager
from core.db import DatabaseModel
from pathlib import Path
import sys
#Creating dummy user... TODO: remove it for production
def _create_users():
    dbm = DatabaseManager()
    sessions = dbm.get_session()
    session = next(sessions)
    DatabaseModel.metadata.create_all(DatabaseManager._instance._engine)
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    with open("pass","r") as f:
        USERS = f.readlines()
    for user in USERS:
        user = user.strip().split(" ")
        existing_user = session.query(User).filter(User.username == user[0]).first()
        if existing_user:
            existing_user.password = pwd_context.hash(user[1])
        else:
            new_user = User(username=user[0],  password=pwd_context.hash(user[1]))
            session.add(new_user)
        session.commit()

def init_db():
    dbm = DatabaseManager()
    #dbm.recreate_db(DatabaseModel)
    _create_users()

