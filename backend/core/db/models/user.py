from datetime import datetime
from pydantic import BaseModel
from typing import Optional
from sqlalchemy import Column, String, Integer
from sqlalchemy.sql.sqltypes import DateTime
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.schema import ForeignKey
from pydantic_sqlalchemy import sqlalchemy_to_pydantic
from core.db import DatabaseModel

class User(DatabaseModel):
    __tablename__ = 'user'
    #Each username should be unique, don't need an id
    id = Column(Integer, primary_key=True)
    username = Column(String(256), unique=True)
    password = Column(String(256))

PydanticUser = sqlalchemy_to_pydantic(User)