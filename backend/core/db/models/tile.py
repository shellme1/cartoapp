from typing import ByteString
from sqlalchemy import Column, String, Integer, Float
from pydantic.main import BaseModel, Optional
from pydantic_sqlalchemy import sqlalchemy_to_pydantic
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship
from core.db import DatabaseModel
from core.db.models.user import User

class Tile(DatabaseModel):
    __tablename__ = 'tile'
    #Each username should be unique, don't need an id
    id = Column(Integer, primary_key=True)
    state = Column(String)

    centerLat = Column(Float)
    centerLng = Column(Float)
    #The following properties are computed by the server :
    imageName = Column(String(256))
    
    videoUrl = Column(String(256), nullable=True)
    videoId = Column(String(256), nullable=True)
    videoPoster = Column(String(256), nullable=True)
    vidTopLeftX= Column(Float, nullable=True)
    vidTopLeftY= Column(Float, nullable=True)
    vidWidth= Column(Float, nullable=True)
    vidHeight= Column(Float, nullable=True)
    

    user_id = Column(Integer, ForeignKey("user.id"))
    user = relationship(User, backref="tiles")

class PydanticTileDelete(BaseModel):
    id : int

class PydanticTileCreate(BaseModel):
    centerLat : float
    centerLng : float

class PydanticTileUpdate(BaseModel):
    id : int
    videoX : Optional[int]
    videoY : Optional[int]
    videoHeight : Optional[float]
    videoWidth : Optional[float]
    currentImageWidth : Optional[float]
    currentImageHeight : Optional[float]

class PydanticTileUpdateUrl(BaseModel):
    id : int
    videoUrl : Optional[str]


PydanticTile = sqlalchemy_to_pydantic(Tile)
PydanticTileFront = sqlalchemy_to_pydantic(Tile, exclude=["state", "user_id", "user"])