from pydantic import BaseModel

#JWT Token
class PydanticToken(BaseModel):
    access_token: str
    token_type: str

#Data in a JWT Token
class PydanticTokenData(BaseModel):
    username: str