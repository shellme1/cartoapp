from datetime import datetime, timedelta
from typing import Optional
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext

from core.db.models.user import User, PydanticUser
from core.db.models.token import PydanticToken, PydanticTokenData
from core.controllers.user import UserController
from core.dbmanager import DatabaseManager

#Authentification use the following things :
# *JWT For access token (signed with our private key)
# *Oauth2 for authentication
# *Bcrypt for password verification, heavy algorithm, long to bruteforce

# openssl rand -hex 32
JWT_SECRET = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
JWT_ALGORITHM = "HS256" # HMAC With sha256
#Time of expire Acces Token
ACCESS_TOKEN_EXPIRE_MINUTES = 30

#The scheme authentification
OAuth2Token = OAuth2PasswordBearer(tokenUrl="token")

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def generate_hash(password):
    return pwd_context.hash(password)

async def verify_password(password, hashed_password):
    return pwd_context.verify(password, hashed_password)

async def authenticate_user(db:Session, username: str, password: str):
    user = UserController.get(db, username)
    if not user:
        return False
    if not await verify_password(password, user.password):
        return False
    return user


async def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    #Make copy cause we will modify it
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:#Default, set it to 15 min
        expire = datetime.utcnow() + timedelta(minutes=15)
    #Add an expiration date to our jwt token
    to_encode.update({"expiration": str(expire)})
    encoded_jwt = jwt.encode(to_encode, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return encoded_jwt


async def get_current_user(db: Session = Depends(DatabaseManager().get_session), token: str = Depends(OAuth2Token)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Could not validate credentials',
        headers={'WWW-Authenticate': 'Bearer'},
    )
    try:
        payload = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        username: str = payload.get('username')
        if username is None:
            raise credentials_exception
    except JWTError as e:
        raise credentials_exception
    user = UserController.get(db, username=username)
    if user is None:
        raise credentials_exception
    return user

async def generate_access_token(user: User) -> PydanticToken:
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = await create_access_token(data={'username': user.username}, expires_delta=access_token_expires)
    return PydanticToken(access_token=access_token, token_type='bearer')

async def verify_token(token: str = Depends(OAuth2Token)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Could not validate credentials',
        headers={'WWW-Authenticate': 'Bearer'},
    )
    try:
        payload = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        username: str = payload.get('username')
        if username is None:
            raise credentials_exception
        return username
    except JWTError:
        raise credentials_exception