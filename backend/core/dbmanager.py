from sqlalchemy.ext.declarative.api import DeclarativeMeta
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from core.db.utils.config import DATABASE_URI
#This class aim to manage the database
class DatabaseManager(object):
    _instance = None
        
    #Don't create __init__ function to have a single instance of this object
    #Singleton
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._instance._engine = create_engine(DATABASE_URI)
            cls._instance._session = scoped_session(sessionmaker(bind=cls._instance._engine))
        return cls._instance
        

    def recreate_db(self, db: DeclarativeMeta) -> None:
        self.drop_db(db)
        self.create_db(db)

    def drop_db(self, db: DeclarativeMeta) -> None:
        db.metadata.drop_all(self._engine)

    def create_db(self, db: DeclarativeMeta) -> None:
        db.metadata.create_all(self._engine)

    def get_session(self):
        db = self._session()
        try:
            yield db
        finally:
            db.close()