import uuid
import requests
import core.db.utils.config as conf
from pathlib import Path
from typing import List
from sqlalchemy.orm import Session
from core.db.models.tile import Tile, PydanticTile, PydanticTileDelete, PydanticTileCreate, PydanticTileUpdate, PydanticTileUpdateUrl
from core.db.models.user import PydanticUser
import images

class TileController(object):
    @staticmethod
    def create(db: Session, tile: PydanticTileCreate, image, creator: PydanticUser) -> PydanticTile:
        image_filename = str(uuid.uuid4())
        with open(Path(images.__path__[0]+f"/"+image_filename),"wb") as f:
            f.write(image.file.read())
        data_create = tile.dict()
        data_create["imageName"] = image_filename
        data_create["state"] = "waiting_video_url"
        new_tile = Tile(**data_create)
        new_tile.user = creator
        db.add(new_tile)
        db.commit()
        return PydanticTile.from_orm(new_tile)

    @staticmethod
    def delete(db: Session, tile: PydanticTileDelete) -> bool:
        tile = db.query(Tile).filter(Tile.id == tile.id).one()
        if tile:
            db.delete(tile)
            db.commit()
            return True
        
        return False
    
    @staticmethod
    def update(db: Session, tile_update: PydanticTileUpdate, tile: Tile) -> bool:
        height_ratio = conf.HEIGHT / tile_update.currentImageHeight
        width_ratio = conf.WIDTH / tile_update.currentImageWidth
        tile.vidTopLeftX = tile_update.videoX * width_ratio
        tile.vidTopLeftY = tile_update.videoY * height_ratio
        tile.vidHeight = tile_update.videoHeight * height_ratio
        tile.vidWidth = tile_update.videoWidth * width_ratio
        tile.state = "completed"
        db.commit()
        return tile

    @staticmethod
    def updateUrl(db: Session, video_url: str, video_id:str, file_name:str,  tile: Tile) -> bool:
        if(video_url == None):
            tile.state = "completed"
            db.commit()
            return True
        tile.state = "waiting_update"
        tile.videoUrl = video_url
        tile.videoId = video_id
        tile.videoPoster = file_name
        db.commit()
        return True

    @staticmethod
    def all(db: Session) -> List[Tile]:
        return db.query(Tile).all()

    @staticmethod
    def mine(db: Session, user: PydanticUser) -> List[Tile]:
        return db.query(Tile).filter(Tile.user_id == user.id).all()

    @staticmethod
    def get(db: Session, id: int) -> PydanticTile:
        return db.query(Tile).filter(Tile.id == id).first()
