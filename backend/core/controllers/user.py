from typing import List
from sqlalchemy.orm import Session
from core.db.models.user import User, PydanticUser

class UserController(object):
    
    @staticmethod
    def all(db: Session) -> List[PydanticUser]:
        return db.query(User).all()

    @staticmethod
    def exists(db: Session, username: str) -> bool:
        result = db.query(User).filter(User.username == username)
        if result.count() != 0:
            return True
        return False

    @staticmethod
    def get(db: Session, username: str) -> PydanticUser:
        return db.query(User).filter(User.username == username).first()
