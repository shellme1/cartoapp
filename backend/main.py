import uvicorn
import sys
import core.db.utils.populatedb as populatedb

from typing import Optional
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.responses import FileResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from api.routers import auth_route, tile_route, tile_admin_route
from core.auth import verify_token

app = FastAPI()

#Our react frontend will make it's requests with /app for static files
app.mount("/app", StaticFiles(directory="../frontend/dist/CartoApp/"), name="static")
app.mount("/api/images", StaticFiles(directory="images/"), name="static")
app.mount("/res", StaticFiles(directory="res/"), name="static")
#For dev purpose, accept cross site requests origin
origins = [
    "http://localhost:*",
    "http://127.0.0.1:*",
]

app.add_middleware(
    CORSMiddleware,
    #allow_origins=origins,
    allow_origin_regex='https?://.*',
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

###Example routes###
@app.get("/")
async def root():
    return FileResponse("../frontend/dist/CartoApp/index.html", media_type="text/html")

@app.get("/app")
async def front():
    return FileResponse("../frontend/dist/CartoApp/index.html", media_type="text/html")

##################

#Include some routers
app.include_router(auth_route.router, prefix="/api", tags=["auth"])
app.include_router(tile_admin_route.router, prefix="/api", tags=["tile_admin"], dependencies=[Depends(verify_token)])
app.include_router(tile_route.router, prefix="/api", tags=["tile"])

#Or, we can also add a dependency in order to ensure the authentication of the use when accessing this routes !
#app.include_router(manage_route.route, prefix="/api/manage", tags=["manage"], dependencies=[get_current_user])

if __name__ == "__main__":    
    populatedb.init_db()
    uvicorn.run("main:app", host="127.0.0.1", reload=True, port=8888, debug=True)
