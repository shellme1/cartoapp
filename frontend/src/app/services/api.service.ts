import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseURL: string = "http://127.0.0.1:8888/api/";

  constructor(private http: HttpClient) {

  }

  getUrl(){
    return this.baseURL;
  }

  _getHeaders(){
    return  new HttpHeaders({
      "Content-Type" : "application/json",
      'accept':  'application/json',
      Authorization: "Bearer "+localStorage.getItem("access_token")
    });
  }

  _getFormDataHeaders(){
    return  new HttpHeaders({
      'accept':  'application/json',
      Authorization: "Bearer "+localStorage.getItem("access_token")
    });
  }

  _getLoginHeaders(){
    return  new HttpHeaders({
      "Content-Type" : "application/x-www-form-urlencoded",
      'accept':  'application/json',
    });
  }

  loginRequest(username:string, password:string): Observable<any>{
    let body = `username=${username}&password=${password}`;
    return this.http.post(this.baseURL + 'login', body, {"headers":this._getLoginHeaders()});
  }

  newTileRequest(formData){
    let myHdrs = this._getFormDataHeaders()
    return this.http.post(this.baseURL + 'tile/create', formData, {"headers":myHdrs});
  }

  sendVideoUrl(tileId, videoUrl){
    return this.http.post(this.baseURL + 'tile/updateurl', JSON.stringify({"id":tileId, "videoUrl":videoUrl}), {"headers":this._getHeaders()});
  }

  updateTileRequest(tileId, videoX, videoY, videoHeight, videoWidth, currentImageWidth, currentImageHeight){
    return this.http.post(this.baseURL + 'tile/update', 
    JSON.stringify({"id":tileId, "videoX":videoX, "videoY":videoY, "videoHeight":videoHeight, "videoWidth":videoWidth, "currentImageWidth":currentImageWidth, "currentImageHeight":currentImageHeight}),
     {"headers":this._getHeaders()});
  }

  getTilesRequest(){
    return this.http.get(this.baseURL + 'tile/all', {params:{}, headers:this._getHeaders()});
  }

  getMyTilesRequest(){
    return this.http.get(this.baseURL + 'tile/mine', {params:{}, headers:this._getHeaders()});
  }

  removeTileRequest(tileId){
    return this.http.post(this.baseURL + 'tile/delete', JSON.stringify({id:tileId}), {headers:this._getHeaders()});
  }



}
