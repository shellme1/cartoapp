import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service'
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isAuthenticated = new BehaviorSubject<boolean>(false);

  constructor(private router: Router, private api: ApiService) {
  }

  signIn(username, password){
    return new Promise((success, fail) => {
      this.api.loginRequest(username, password)
      .subscribe(
        (response) => {
          if(response.hasOwnProperty("access_token")){
            this.setCookie(response.access_token);
          }
          success(1);
        },
        (error) => {
          fail(error);
        }
      )
    });
  }

  checkAuthenticated(){
    if(localStorage.getItem("access_token")===null){
      return false;
    }
    return true;
  }

  signOut(){
    localStorage.removeItem("access_token");
  }

  setCookie(sessionToken){
    localStorage.setItem("access_token", sessionToken);
  }
}