import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  getMaxZoom(){
    return 15;
  }

  getMinZoom(){
    return 4;
  }

  getMaxWidth(){
    return 1000;
  }

  getMaxHeight(){
    return 1404;
  }
}


