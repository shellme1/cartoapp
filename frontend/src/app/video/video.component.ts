import { Component } from '@angular/core';
import './embed.scss'
import videojs from 'video.js'
import { peertubeTranslate } from './shared/core-utils/i18n'
import {
  ResultList,
  ServerConfig,
  UserRefreshToken,
  VideoCaption,
  VideoDetails,
  VideoPlaylist,
  VideoPlaylistElement,
  VideoStreamingPlaylistType,
  PluginType,
  ClientHookName
} from './shared/models'
import { HttpStatusCode } from './shared/core-utils/miscs/http-error-codes'
import { P2PMediaLoaderOptions, PeertubePlayerManagerOptions, PlayerMode } from './assets/player/peertube-player-manager'
import { VideoJSCaption } from './assets/player/peertube-videojs-typings'
import { TranslationsManager } from './assets/player/translations-manager'
import { Hooks, loadPlugin, runHook } from './root-helpers/plugins'
import { Tokens } from './root-helpers/users'
import { peertubeLocalStorage } from './root-helpers/peertube-web-storage'
import { objectToUrlEncoded } from './root-helpers/utils'
import { PeerTubeEmbedApi } from './embed-api'
import { RegisterClientHelpers } from './types/register-client-option.model'
import { environment } from './environments/environment'

if (global === undefined) {
  var global = window;
}

type Translations = { [ id: string ]: string }
export class VideoComponent {
  playerElement: HTMLVideoElement
  player: videojs.Player
  api: PeerTubeEmbedApi = null

  autoplay: boolean
  controls: boolean
  muted: boolean
  loop: boolean
  subtitle: string
  enableApi = false
  startTime: number | string = 0
  stopTime: number | string

  title: boolean
  warningTitle: boolean
  peertubeLink: boolean
  bigPlayBackgroundColor: string
  foregroundColor: string

  mode: PlayerMode
  scope = 'peertube'

  userTokens: Tokens
  headers = new Headers()
  LOCAL_STORAGE_OAUTH_CLIENT_KEYS = {
    CLIENT_ID: 'client_id',
    CLIENT_SECRET: 'client_secret'
  }

  private translationsPromise: Promise<{ [id: string]: string }>
  private configPromise: Promise<ServerConfig>
  private PeertubePlayerManagerModulePromise: Promise<any>

  private playlist: VideoPlaylist
  private playlistElements: VideoPlaylistElement[]
  private currentPlaylistElement: VideoPlaylistElement

  private wrapperElement: HTMLElement

  private peertubeHooks: Hooks = {}
  private loadedScripts = new Set<string>()

  static async main () {
    const videoContainerId = 'video-wrapper'
    // const embed = new VideoComponent(videoContainerId)
    // await embed.init()
  }

  constructor (private videoWrapperId: any, private apiUrl: string, private videoId: string) {
    //this.playerElement = videoElement
    //this.wrapperElement = this.videoWrapperId
  }

  getVideoUrl () {
    return this.apiUrl + '/api/v1/videos/' + this.videoId
  }

  refreshFetch (url: string, options?: RequestInit) {
    return fetch(url, options)
      .then((res: Response) => {
        if (res.status !== HttpStatusCode.UNAUTHORIZED_401) return res

        const refreshingTokenPromise = new Promise((resolve, reject) => {
          const clientId: string = peertubeLocalStorage.getItem(this.LOCAL_STORAGE_OAUTH_CLIENT_KEYS.CLIENT_ID)
          const clientSecret: string = peertubeLocalStorage.getItem(this.LOCAL_STORAGE_OAUTH_CLIENT_KEYS.CLIENT_SECRET)

          const headers = new Headers()
          headers.set('Content-Type', 'application/x-www-form-urlencoded')

          const data = {
            refresh_token: this.userTokens.refreshToken,
            client_id: clientId,
            client_secret: clientSecret,
            response_type: 'code',
            grant_type: 'refresh_token'
          }

          fetch('/api/v1/users/token', {
            headers,
            method: 'POST',
            body: objectToUrlEncoded(data)
          }).then(res => {
            if (res.status === HttpStatusCode.UNAUTHORIZED_401) return undefined

            return res.json()
          }).then((obj: UserRefreshToken & { code: 'invalid_grant'}) => {
            if (!obj || obj.code === 'invalid_grant') {
              Tokens.flush()
              this.removeTokensFromHeaders()

              return resolve()
            }

            this.userTokens.accessToken = obj.access_token
            this.userTokens.refreshToken = obj.refresh_token
            this.userTokens.save()

            this.setHeadersFromTokens()

            resolve()
          }).catch((refreshTokenError: any) => {
            reject(refreshTokenError)
          })
        })

        return refreshingTokenPromise
          .catch(() => {
            Tokens.flush()

            this.removeTokensFromHeaders()
          }).then(() => fetch(url, {
            ...options,
            headers: this.headers
          }))
      })
  }

  loadVideoInfo (): Promise<Response> {
    return this.refreshFetch(this.getVideoUrl(), { headers: this.headers })
  }

  loadVideoCaptions (): Promise<Response> {
    return this.refreshFetch(this.getVideoUrl() + '/captions', { headers: this.headers })
  }

  loadConfig (): Promise<ServerConfig> {
    return this.refreshFetch(this.apiUrl+'/api/v1/config')
      .then(res => res.json())
  }

  removeElement (element: HTMLElement) {
    element.parentElement.removeChild(element)
  }

  displayError (text: string, translations?: Translations) {
    // Remove video element
    if (this.playerElement) {
      this.removeElement(this.playerElement)
      this.playerElement = undefined
    }

    const translatedText = peertubeTranslate(text, translations)
    const translatedSorry = peertubeTranslate('Sorry', translations)

    document.title = translatedSorry + ' - ' + translatedText

    const errorBlock = document.getElementById('error-block')
    errorBlock.style.display = 'flex'

    const errorTitle = document.getElementById('error-title')
    errorTitle.innerHTML = peertubeTranslate('Sorry', translations)

    const errorText = document.getElementById('error-content')
    errorText.innerHTML = translatedText
  }

  videoNotFound (translations?: Translations) {
    const text = 'This video does not exist.'
    this.displayError(text, translations)
  }

  videoFetchError (translations?: Translations) {
    const text = 'We cannot fetch the video. Please try again later.'
    this.displayError(text, translations)
  }

  getParamToggle (params: URLSearchParams, name: string, defaultValue?: boolean) {
    return params.has(name) ? (params.get(name) === '1' || params.get(name) === 'true') : defaultValue
  }

  getParamString (params: URLSearchParams, name: string, defaultValue?: string) {
    return params.has(name) ? params.get(name) : defaultValue
  }

  getCurrentPosition () {
    if (!this.currentPlaylistElement) return -1

    return this.currentPlaylistElement.position
  }

  async init () {
    try {
      this.userTokens = Tokens.load()
      await this.initCore()
    } catch (e) {
      console.error(e)
    }
  }

  private initializeApi () {
    if (!this.enableApi) return

    this.api = new PeerTubeEmbedApi(this)
    this.api.initialize()
  }

  private loadParams (video: VideoDetails) {
    try {
      const params = new URL(window.location.toString()).searchParams

      this.autoplay = this.getParamToggle(params, 'autoplay', false)
      this.controls = this.getParamToggle(params, 'controls', true)
      this.muted = this.getParamToggle(params, 'muted', undefined)
      this.loop = this.getParamToggle(params, 'loop', false)
      this.title = this.getParamToggle(params, 'title', true)
      this.enableApi = this.getParamToggle(params, 'api', this.enableApi)
      this.warningTitle = this.getParamToggle(params, 'warningTitle', true)
      this.peertubeLink = this.getParamToggle(params, 'peertubeLink', true)

      this.scope = this.getParamString(params, 'scope', this.scope)
      this.subtitle = this.getParamString(params, 'subtitle')
      this.startTime = this.getParamString(params, 'start')
      this.stopTime = this.getParamString(params, 'stop')

      this.bigPlayBackgroundColor = this.getParamString(params, 'bigPlayBackgroundColor')
      this.foregroundColor = this.getParamString(params, 'foregroundColor')

      const modeParam = this.getParamString(params, 'mode')

      if (modeParam) {
        if (modeParam === 'p2p-media-loader') this.mode = 'p2p-media-loader'
        else this.mode = 'webtorrent'
      } else {
        if (Array.isArray(video.streamingPlaylists) && video.streamingPlaylists.length !== 0) this.mode = 'p2p-media-loader'
        else this.mode = 'webtorrent'
      }
    } catch (err) {
      console.error('Cannot get params from URL.', err)
    }
  }

  private async loadVideo () {
    const videoPromise = this.loadVideoInfo()

    let videoResponse: Response
    let isResponseOk: boolean

    try {
      videoResponse = await videoPromise
      isResponseOk = videoResponse.status === HttpStatusCode.OK_200
    } catch (err) {
      console.error(err)

      isResponseOk = false
    }

    if (!isResponseOk) {
      const serverTranslations = await this.translationsPromise

      if (videoResponse?.status === HttpStatusCode.NOT_FOUND_404) {
        this.videoNotFound(serverTranslations)
        return undefined
      }

      this.videoFetchError(serverTranslations)
      return undefined
    }

    const captionsPromise = this.loadVideoCaptions()

    return { captionsPromise, videoResponse }
  }

  private async loadVideoAndBuildPlayer () {
    const res = await this.loadVideo()
    if (res === undefined) return

    return this.buildVideoPlayer(res.videoResponse, res.captionsPromise)
  }

  private async buildVideoPlayer (videoResponse: Response, captionsPromise: Promise<Response>) {
    let alreadyHadPlayer = false

    if (this.player) {
      this.player.dispose()
      alreadyHadPlayer = true
    }

    this.playerElement = document.createElement('video')
    // this.playerElement.setAttribute('playsinline', 'true')
    // this.playerElement.setAttribute('controls',"1")

    const videoInfoPromise = videoResponse.json()
      .then((videoInfo: VideoDetails) => {
        if (!alreadyHadPlayer) this.loadPlaceholder(videoInfo)

        return videoInfo
      })

    const [ videoInfoTmp, serverTranslations, captionsResponse, config, PeertubePlayerManagerModule ] = await Promise.all([
      videoInfoPromise,
      this.translationsPromise,
      captionsPromise,
      this.configPromise,
      this.PeertubePlayerManagerModulePromise
    ])

    await this.ensurePluginsAreLoaded(config, serverTranslations)

    const videoInfo: VideoDetails = videoInfoTmp

    const PeertubePlayerManager = PeertubePlayerManagerModule.PeertubePlayerManager
    const videoCaptions = []
    this.loadParams(videoInfo)

    const options: PeertubePlayerManagerOptions = {
      common: {
        // Autoplay in playlist mode
        autoplay: alreadyHadPlayer ? true : this.autoplay,
        controls: this.controls,
        muted: this.muted,
        loop: this.loop,

        captions: videoCaptions.length !== 0,
        subtitle: this.subtitle,

        startTime: this.startTime,
        stopTime: this.stopTime,

        nextVideo: undefined,
        hasNextVideo: undefined,

        previousVideo: undefined,
        hasPreviousVideo: undefined,

        playlist: undefined,

        videoCaptions,
        inactivityTimeout: 2500,
        videoViewUrl: undefined,

        isLive: videoInfo.isLive,

        playerElement: this.playerElement,
        onPlayerElementChange: (element: HTMLVideoElement) => this.playerElement = element,

        videoDuration: videoInfo.duration,
        enableHotkeys: true,
        peertubeLink: this.peertubeLink,
        poster: window.location.origin + videoInfo.previewPath,
        theaterButton: false,

        serverUrl: window.location.origin,
        language: navigator.language,
        embedUrl: window.location.origin + videoInfo.embedPath
      },

      webtorrent: {
        videoFiles: videoInfo.files
      }
    }

    if (this.mode === 'p2p-media-loader') {
      const hlsPlaylist = videoInfo.streamingPlaylists.find(p => p.type === VideoStreamingPlaylistType.HLS)

      Object.assign(options, {
        p2pMediaLoader: {
          playlistUrl: hlsPlaylist.playlistUrl,
          segmentsSha256Url: hlsPlaylist.segmentsSha256Url,
          redundancyBaseUrls: hlsPlaylist.redundancies.map(r => r.baseUrl),
          trackerAnnounce: videoInfo.trackerUrls,
          videoFiles: hlsPlaylist.files
        } as P2PMediaLoaderOptions
      })
    }

    this.player = await PeertubePlayerManager.initialize(this.mode, options, (player: videojs.Player) => this.player = player)
    this.player.on('customError', (event: any, data: any) => this.handleError(data.err, serverTranslations))

    //window[ 'videojsPlayer' ] = this.player

    //this.buildCSS()

    //await this.buildDock(videoInfo, config)

    //this.initializeApi()

    //this.removePlaceholder()

    this.runHook('action:embed.player.loaded', undefined, { player: this.player, videojs, video: videoInfo })
  }

  private async initCore () {
    if (this.userTokens) this.setHeadersFromTokens()

    this.configPromise = this.loadConfig()
    this.translationsPromise = undefined
    this.PeertubePlayerManagerModulePromise = import('./assets/player/peertube-player-manager')

    return this.loadVideoAndBuildPlayer()
  }

  private handleError (err: Error, translations?: { [ id: string ]: string }) {
    if (err.message.indexOf('from xs param') !== -1) {
      this.player.dispose()
      this.playerElement = null
      this.displayError('This video is not available because the remote instance is not responding.', translations)
      return
    }
  }

  private async buildDock (videoInfo: VideoDetails, config: ServerConfig) {
    if (!this.controls) return

    // On webtorrent fallback, player may have been disposed
    if (!this.player.player_) return

    const title = this.title ? videoInfo.name : undefined

    const description = config.tracker.enabled && this.warningTitle
      ? '<span class="text">' + peertubeTranslate('Watching this video may reveal your IP address to others.') + '</span>'
      : undefined

    if (title || description) {
      this.player.dock({
        title,
        description
      })
    }
  }

  private buildCSS () {
    const body = document.getElementById('custom-css')

    if (this.bigPlayBackgroundColor) {
      body.style.setProperty('--embedBigPlayBackgroundColor', this.bigPlayBackgroundColor)
    }

    if (this.foregroundColor) {
      body.style.setProperty('--embedForegroundColor', this.foregroundColor)
    }
  }

  private async buildCaptions (serverTranslations: any, captionsResponse: Response): Promise<VideoJSCaption[]> {
    if (captionsResponse.ok) {
      const { data } = (await captionsResponse.json()) as ResultList<VideoCaption>

      return data.map(c => ({
        label: peertubeTranslate(c.language.label, serverTranslations),
        language: c.language.id,
        src: window.location.origin + c.captionPath
      }))
    }

    return []
  }

  private loadPlaceholder (video: VideoDetails) {
    // const placeholder = this.getPlaceholderElement()

    // const url = window.location.origin + video.previewPath
    // placeholder.style.backgroundImage = `url("${url}")`
    // placeholder.style.display = 'block'
  }

  private removePlaceholder () {
    const placeholder = this.getPlaceholderElement()
    //placeholder.style.display = 'none'
  }

  private getPlaceholderElement () {
    // return document.getElementById('placeholder-preview')
  }

  private setHeadersFromTokens () {
    this.headers.set('Authorization', `${this.userTokens.tokenType} ${this.userTokens.accessToken}`)
  }

  private removeTokensFromHeaders () {
    this.headers.delete('Authorization')
  }

  private getResourceId () {
    const urlParts = window.location.pathname.split('/')
    return urlParts[ urlParts.length - 1 ]
  }

  private async ensurePluginsAreLoaded (config: ServerConfig, translations?: { [ id: string ]: string }) {
    if (config.plugin.registered.length === 0) return

    for (const plugin of config.plugin.registered) {
      for (const key of Object.keys(plugin.clientScripts)) {
        const clientScript = plugin.clientScripts[key]

        if (clientScript.scopes.includes('embed') === false) continue

        const script = `/plugins/${plugin.name}/${plugin.version}/client-scripts/${clientScript.script}`

        if (this.loadedScripts.has(script)) continue

        const pluginInfo = {
          plugin,
          clientScript: {
            script,
            scopes: clientScript.scopes
          },
          pluginType: PluginType.PLUGIN,
          isTheme: false
        }

        await loadPlugin({
          hooks: this.peertubeHooks,
          pluginInfo,
          peertubeHelpersFactory: _ => this.buildPeerTubeHelpers(translations)
        })
      }
    }
  }

  private buildPeerTubeHelpers (translations?: { [ id: string ]: string }): RegisterClientHelpers {
    function unimplemented (): any {
      throw new Error('This helper is not implemented in embed.')
    }

    return {
      getBaseStaticRoute: unimplemented,

      getSettings: unimplemented,

      isLoggedIn: unimplemented,

      notifier: {
        info: unimplemented,
        error: unimplemented,
        success: unimplemented
      },

      showModal: unimplemented,

      markdownRenderer: {
        textMarkdownToHTML: unimplemented,
        enhancedMarkdownToHTML: unimplemented
      },

      translate: (value: string) => {
        return Promise.resolve(peertubeTranslate(value, translations))
      }
    }
  }

  private runHook <T> (hookName: ClientHookName, result?: T, params?: any): Promise<T> {
    return runHook(this.peertubeHooks, hookName, result, params)
  }
}

VideoComponent.main()
  .catch(err => console.error('Cannot init embed.', err))
