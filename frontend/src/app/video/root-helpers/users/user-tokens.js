"use strict";
exports.__esModule = true;
exports.Tokens = void 0;
var peertube_web_storage_1 = require("../peertube-web-storage");
// Private class only used by User
var Tokens = /** @class */ (function () {
    function Tokens(hash) {
        if (hash) {
            this.accessToken = hash.accessToken;
            this.refreshToken = hash.refreshToken;
            if (hash.tokenType === 'bearer') {
                this.tokenType = 'Bearer';
            }
            else {
                this.tokenType = hash.tokenType;
            }
        }
    }
    Tokens.load = function () {
        var accessTokenLocalStorage = peertube_web_storage_1.peertubeLocalStorage.getItem(this.KEYS.ACCESS_TOKEN);
        var refreshTokenLocalStorage = peertube_web_storage_1.peertubeLocalStorage.getItem(this.KEYS.REFRESH_TOKEN);
        var tokenTypeLocalStorage = peertube_web_storage_1.peertubeLocalStorage.getItem(this.KEYS.TOKEN_TYPE);
        if (accessTokenLocalStorage && refreshTokenLocalStorage && tokenTypeLocalStorage) {
            return new Tokens({
                accessToken: accessTokenLocalStorage,
                refreshToken: refreshTokenLocalStorage,
                tokenType: tokenTypeLocalStorage
            });
        }
        return null;
    };
    Tokens.flush = function () {
        peertube_web_storage_1.peertubeLocalStorage.removeItem(this.KEYS.ACCESS_TOKEN);
        peertube_web_storage_1.peertubeLocalStorage.removeItem(this.KEYS.REFRESH_TOKEN);
        peertube_web_storage_1.peertubeLocalStorage.removeItem(this.KEYS.TOKEN_TYPE);
    };
    Tokens.prototype.save = function () {
        peertube_web_storage_1.peertubeLocalStorage.setItem(Tokens.KEYS.ACCESS_TOKEN, this.accessToken);
        peertube_web_storage_1.peertubeLocalStorage.setItem(Tokens.KEYS.REFRESH_TOKEN, this.refreshToken);
        peertube_web_storage_1.peertubeLocalStorage.setItem(Tokens.KEYS.TOKEN_TYPE, this.tokenType);
    };
    Tokens.KEYS = {
        ACCESS_TOKEN: 'access_token',
        REFRESH_TOKEN: 'refresh_token',
        TOKEN_TYPE: 'token_type'
    };
    return Tokens;
}());
exports.Tokens = Tokens;
