"use strict";
exports.__esModule = true;
exports.flushUserInfoFromLocalStorage = exports.saveUserInfoIntoLocalStorage = exports.getUserInfoFromLocalStorage = void 0;
var peertube_web_storage_1 = require("../peertube-web-storage");
var user_local_storage_keys_1 = require("./user-local-storage-keys");
function getUserInfoFromLocalStorage() {
    var usernameLocalStorage = peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.USERNAME);
    if (!usernameLocalStorage)
        return undefined;
    return {
        id: parseInt(peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.ID), 10),
        username: peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.USERNAME),
        email: peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.EMAIL),
        role: parseInt(peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.ROLE), 10),
        nsfwPolicy: peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.NSFW_POLICY),
        webTorrentEnabled: peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.WEBTORRENT_ENABLED) === 'true',
        autoPlayVideo: peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.AUTO_PLAY_VIDEO) === 'true',
        videosHistoryEnabled: peertube_web_storage_1.peertubeLocalStorage.getItem(user_local_storage_keys_1.UserLocalStorageKeys.VIDEOS_HISTORY_ENABLED) === 'true'
    };
}
exports.getUserInfoFromLocalStorage = getUserInfoFromLocalStorage;
function flushUserInfoFromLocalStorage() {
    peertube_web_storage_1.peertubeLocalStorage.removeItem(user_local_storage_keys_1.UserLocalStorageKeys.ID);
    peertube_web_storage_1.peertubeLocalStorage.removeItem(user_local_storage_keys_1.UserLocalStorageKeys.USERNAME);
    peertube_web_storage_1.peertubeLocalStorage.removeItem(user_local_storage_keys_1.UserLocalStorageKeys.EMAIL);
    peertube_web_storage_1.peertubeLocalStorage.removeItem(user_local_storage_keys_1.UserLocalStorageKeys.ROLE);
    peertube_web_storage_1.peertubeLocalStorage.removeItem(user_local_storage_keys_1.UserLocalStorageKeys.NSFW_POLICY);
    peertube_web_storage_1.peertubeLocalStorage.removeItem(user_local_storage_keys_1.UserLocalStorageKeys.WEBTORRENT_ENABLED);
    peertube_web_storage_1.peertubeLocalStorage.removeItem(user_local_storage_keys_1.UserLocalStorageKeys.AUTO_PLAY_VIDEO);
    peertube_web_storage_1.peertubeLocalStorage.removeItem(user_local_storage_keys_1.UserLocalStorageKeys.VIDEOS_HISTORY_ENABLED);
}
exports.flushUserInfoFromLocalStorage = flushUserInfoFromLocalStorage;
function saveUserInfoIntoLocalStorage(info) {
    peertube_web_storage_1.peertubeLocalStorage.setItem(user_local_storage_keys_1.UserLocalStorageKeys.ID, info.id.toString());
    peertube_web_storage_1.peertubeLocalStorage.setItem(user_local_storage_keys_1.UserLocalStorageKeys.USERNAME, info.username);
    peertube_web_storage_1.peertubeLocalStorage.setItem(user_local_storage_keys_1.UserLocalStorageKeys.EMAIL, info.email);
    peertube_web_storage_1.peertubeLocalStorage.setItem(user_local_storage_keys_1.UserLocalStorageKeys.ROLE, info.role.toString());
    peertube_web_storage_1.peertubeLocalStorage.setItem(user_local_storage_keys_1.UserLocalStorageKeys.NSFW_POLICY, info.nsfwPolicy.toString());
    peertube_web_storage_1.peertubeLocalStorage.setItem(user_local_storage_keys_1.UserLocalStorageKeys.WEBTORRENT_ENABLED, JSON.stringify(info.webTorrentEnabled));
    peertube_web_storage_1.peertubeLocalStorage.setItem(user_local_storage_keys_1.UserLocalStorageKeys.AUTO_PLAY_VIDEO, JSON.stringify(info.autoPlayVideo));
}
exports.saveUserInfoIntoLocalStorage = saveUserInfoIntoLocalStorage;
