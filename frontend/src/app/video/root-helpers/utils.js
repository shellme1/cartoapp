"use strict";
exports.__esModule = true;
exports.wait = exports.objectToUrlEncoded = exports.importModule = exports.copyToClipboard = void 0;
var environment_1 = require("../environments/environment");
function objectToUrlEncoded(obj) {
    var str = [];
    for (var _i = 0, _a = Object.keys(obj); _i < _a.length; _i++) {
        var key = _a[_i];
        str.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
    }
    return str.join('&');
}
exports.objectToUrlEncoded = objectToUrlEncoded;
function copyToClipboard(text) {
    var el = document.createElement('textarea');
    el.value = text;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}
exports.copyToClipboard = copyToClipboard;
// Thanks: https://github.com/uupaa/dynamic-import-polyfill
function importModule(path) {
    return new Promise(function (resolve, reject) {
        var vector = '$importModule$' + Math.random().toString(32).slice(2);
        var script = document.createElement('script');
        var destructor = function () {
            delete window[vector];
            script.onerror = null;
            script.onload = null;
            script.remove();
            URL.revokeObjectURL(script.src);
            script.src = '';
        };
        script.defer = true;
        script.type = 'module';
        script.onerror = function () {
            reject(new Error("Failed to import: " + path));
            destructor();
        };
        script.onload = function () {
            resolve(window[vector]);
            destructor();
        };
        var absURL = (environment_1.environment.apiUrl || window.location.origin) + path;
        var loader = "import * as m from \"" + absURL + "\"; window." + vector + " = m;"; // export Module
        var blob = new Blob([loader], { type: 'text/javascript' });
        script.src = URL.createObjectURL(blob);
        document.head.appendChild(script);
    });
}
exports.importModule = importModule;
function wait(ms) {
    return new Promise(function (res) {
        setTimeout(function () { return res(); }, ms);
    });
}
exports.wait = wait;
