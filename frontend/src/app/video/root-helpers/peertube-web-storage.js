"use strict";
// Thanks: https://github.com/capaj/localstorage-polyfill
exports.__esModule = true;
exports.peertubeSessionStorage = exports.peertubeLocalStorage = void 0;
var valuesMap = new Map();
function proxify(instance) {
    return new Proxy(instance, {
        set: function (obj, prop, value) {
            if (MemoryStorage.prototype.hasOwnProperty(prop)) {
                instance[prop] = value;
            }
            else {
                instance.setItem(prop, value);
            }
            return true;
        },
        get: function (target, name) {
            if (MemoryStorage.prototype.hasOwnProperty(name)) {
                return instance[name];
            }
            if (valuesMap.has(name)) {
                return instance.getItem(name);
            }
        }
    });
}
var MemoryStorage = /** @class */ (function () {
    function MemoryStorage() {
    }
    MemoryStorage.prototype.getItem = function (key) {
        var stringKey = String(key);
        if (valuesMap.has(key)) {
            return String(valuesMap.get(stringKey));
        }
        return null;
    };
    MemoryStorage.prototype.setItem = function (key, val) {
        valuesMap.set(String(key), String(val));
    };
    MemoryStorage.prototype.removeItem = function (key) {
        valuesMap["delete"](key);
    };
    MemoryStorage.prototype.clear = function () {
        valuesMap.clear();
    };
    MemoryStorage.prototype.key = function (i) {
        if (arguments.length === 0) {
            throw new TypeError('Failed to execute "key" on "Storage": 1 argument required, but only 0 present.');
        }
        var arr = Array.from(valuesMap.keys());
        return arr[i];
    };
    Object.defineProperty(MemoryStorage.prototype, "length", {
        get: function () {
            return valuesMap.size;
        },
        enumerable: false,
        configurable: true
    });
    return MemoryStorage;
}());
var peertubeLocalStorage;
exports.peertubeLocalStorage = peertubeLocalStorage;
var peertubeSessionStorage;
exports.peertubeSessionStorage = peertubeSessionStorage;
function reinitStorage() {
    var instanceLocalStorage = new MemoryStorage();
    var instanceSessionStorage = new MemoryStorage();
    exports.peertubeLocalStorage = peertubeLocalStorage = proxify(instanceLocalStorage);
    exports.peertubeSessionStorage = peertubeSessionStorage = proxify(instanceSessionStorage);
}
try {
    exports.peertubeLocalStorage = peertubeLocalStorage = localStorage;
    exports.peertubeSessionStorage = peertubeSessionStorage = sessionStorage;
}
catch (err) {
    // support Firefox and other browsers using an exception rather than null
    reinitStorage();
}
// support Brave and other browsers using null rather than an exception
if (peertubeLocalStorage === null || peertubeSessionStorage === null) {
    reinitStorage();
}
