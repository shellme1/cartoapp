"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.runHook = exports.loadPlugin = void 0;
var hooks_1 = require("@shared/core-utils/plugins/hooks");
var models_1 = require("../../../shared/models");
var utils_1 = require("./utils");
function runHook(hooks, hookName, result, params) {
    return __awaiter(this, void 0, void 0, function () {
        var hookType, _loop_1, _i, _a, hook;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!hooks[hookName])
                        return [2 /*return*/, result];
                    hookType = hooks_1.getHookType(hookName);
                    _loop_1 = function (hook) {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    console.log('Running hook %s of plugin %s.', hookName, hook.plugin.name);
                                    return [4 /*yield*/, hooks_1.internalRunHook(hook.handler, hookType, result, params, function (err) {
                                            console.error('Cannot run hook %s of script %s of plugin %s.', hookName, hook.clientScript.script, hook.plugin.name, err);
                                        })];
                                case 1:
                                    result = _a.sent();
                                    return [2 /*return*/];
                            }
                        });
                    };
                    _i = 0, _a = hooks[hookName];
                    _b.label = 1;
                case 1:
                    if (!(_i < _a.length)) return [3 /*break*/, 4];
                    hook = _a[_i];
                    return [5 /*yield**/, _loop_1(hook)];
                case 2:
                    _b.sent();
                    _b.label = 3;
                case 3:
                    _i++;
                    return [3 /*break*/, 1];
                case 4: return [2 /*return*/, result];
            }
        });
    });
}
exports.runHook = runHook;
function loadPlugin(options) {
    var hooks = options.hooks, pluginInfo = options.pluginInfo, peertubeHelpersFactory = options.peertubeHelpersFactory, formFields = options.formFields;
    var plugin = pluginInfo.plugin, clientScript = pluginInfo.clientScript;
    var registerHook = function (options) {
        if (models_1.clientHookObject[options.target] !== true) {
            console.error('Unknown hook %s of plugin %s. Skipping.', options.target, plugin.name);
            return;
        }
        if (!hooks[options.target])
            hooks[options.target] = [];
        hooks[options.target].push({
            plugin: plugin,
            clientScript: clientScript,
            target: options.target,
            handler: options.handler,
            priority: options.priority || 0
        });
    };
    var registerVideoField = function (commonOptions, videoFormOptions) {
        if (!formFields) {
            throw new Error('Video field registration is not supported');
        }
        formFields.video.push({
            commonOptions: commonOptions,
            videoFormOptions: videoFormOptions
        });
    };
    var peertubeHelpers = peertubeHelpersFactory(pluginInfo);
    console.log('Loading script %s of plugin %s.', clientScript.script, plugin.name);
    return utils_1.importModule(clientScript.script)
        .then(function (script) { return script.register({ registerHook: registerHook, registerVideoField: registerVideoField, peertubeHelpers: peertubeHelpers }); })
        .then(function () { return sortHooksByPriority(hooks); })["catch"](function (err) { return console.error('Cannot import or register plugin %s.', pluginInfo.plugin.name, err); });
}
exports.loadPlugin = loadPlugin;
function sortHooksByPriority(hooks) {
    for (var _i = 0, _a = Object.keys(hooks); _i < _a.length; _i++) {
        var hookName = _a[_i];
        hooks[hookName].sort(function (a, b) {
            return b.priority - a.priority;
        });
    }
}
