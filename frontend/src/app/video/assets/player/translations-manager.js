"use strict";
exports.__esModule = true;
exports.TranslationsManager = void 0;
var i18n_1 = require("@shared/core-utils/i18n");
var TranslationsManager = /** @class */ (function () {
    function TranslationsManager() {
    }
    TranslationsManager.getServerTranslations = function (serverUrl, locale) {
        var path = TranslationsManager.getLocalePath(serverUrl, locale);
        // It is the default locale, nothing to translate
        if (!path)
            return Promise.resolve(undefined);
        return fetch(path + '/server.json')
            .then(function (res) { return res.json(); })["catch"](function (err) {
            console.error('Cannot get server translations', err);
            return undefined;
        });
    };
    TranslationsManager.loadLocaleInVideoJS = function (serverUrl, locale, videojs) {
        var path = TranslationsManager.getLocalePath(serverUrl, locale);
        // It is the default locale, nothing to translate
        if (!path)
            return Promise.resolve(undefined);
        var p;
        if (TranslationsManager.videojsLocaleCache[path]) {
            p = Promise.resolve(TranslationsManager.videojsLocaleCache[path]);
        }
        else {
            p = fetch(path + '/player.json')
                .then(function (res) { return res.json(); })
                .then(function (json) {
                TranslationsManager.videojsLocaleCache[path] = json;
                return json;
            })["catch"](function (err) {
                console.error('Cannot get player translations', err);
                return undefined;
            });
        }
        var completeLocale = i18n_1.getCompleteLocale(locale);
        return p.then(function (json) { return videojs.addLanguage(i18n_1.getShortLocale(completeLocale), json); });
    };
    TranslationsManager.getLocalePath = function (serverUrl, locale) {
        var completeLocale = i18n_1.getCompleteLocale(locale);
        if (!i18n_1.is18nLocale(completeLocale) || i18n_1.isDefaultLocale(completeLocale))
            return undefined;
        return serverUrl + '/client/locales/' + completeLocale;
    };
    TranslationsManager.videojsLocaleCache = {};
    return TranslationsManager;
}());
exports.TranslationsManager = TranslationsManager;
