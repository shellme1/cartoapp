"use strict";
exports.__esModule = true;
exports.isSafari = exports.isIOS = exports.bytes = exports.isMobile = exports.videoFileMinByResolution = exports.videoFileMaxByResolution = exports.buildVideoOrPlaylistEmbed = exports.buildVideoLink = exports.buildPlaylistLink = exports.isWebRTCDisabled = exports.secondsToTime = exports.timeToInt = exports.toTitleCase = exports.getRtcConfig = void 0;
function toTitleCase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}
exports.toTitleCase = toTitleCase;
function isWebRTCDisabled() {
    return !!(window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection) === false;
}
exports.isWebRTCDisabled = isWebRTCDisabled;
function isIOS() {
    if (/iPad|iPhone|iPod/.test(navigator.platform)) {
        return true;
    }
    // Detect iPad Desktop mode
    return !!(navigator.maxTouchPoints &&
        navigator.maxTouchPoints > 2 &&
        /MacIntel/.test(navigator.platform));
}
exports.isIOS = isIOS;
function isSafari() {
    return /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
}
exports.isSafari = isSafari;
// https://github.com/danrevah/ngx-pipes/blob/master/src/pipes/math/bytes.ts
// Don't import all Angular stuff, just copy the code with shame
var dictionaryBytes = [
    { max: 1024, type: 'B' },
    { max: 1048576, type: 'KB' },
    { max: 1073741824, type: 'MB' },
    { max: 1.0995116e12, type: 'GB' }
];
function bytes(value) {
    var format = dictionaryBytes.find(function (d) { return value < d.max; }) || dictionaryBytes[dictionaryBytes.length - 1];
    var calc = Math.floor(value / (format.max / 1024)).toString();
    return [calc, format.type];
}
exports.bytes = bytes;
function isMobile() {
    return /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
}
exports.isMobile = isMobile;
function buildVideoLink(options) {
    if (options === void 0) { options = {}; }
    var baseUrl = options.baseUrl;
    var url = baseUrl
        ? baseUrl
        : window.location.origin + window.location.pathname.replace('/embed/', '/watch/');
    var params = generateParams(window.location.search);
    if (options.startTime !== undefined && options.startTime !== null) {
        var startTimeInt = Math.floor(options.startTime);
        params.set('start', secondsToTime(startTimeInt));
    }
    if (options.stopTime) {
        var stopTimeInt = Math.floor(options.stopTime);
        params.set('stop', secondsToTime(stopTimeInt));
    }
    if (options.subtitle)
        params.set('subtitle', options.subtitle);
    if (options.loop === true)
        params.set('loop', '1');
    if (options.autoplay === true)
        params.set('autoplay', '1');
    if (options.muted === true)
        params.set('muted', '1');
    if (options.title === false)
        params.set('title', '0');
    if (options.warningTitle === false)
        params.set('warningTitle', '0');
    if (options.controls === false)
        params.set('controls', '0');
    if (options.peertubeLink === false)
        params.set('peertubeLink', '0');
    return buildUrl(url, params);
}
exports.buildVideoLink = buildVideoLink;
function buildPlaylistLink(options) {
    var baseUrl = options.baseUrl;
    var url = baseUrl
        ? baseUrl
        : window.location.origin + window.location.pathname.replace('/video-playlists/embed/', '/videos/watch/playlist/');
    var params = generateParams(window.location.search);
    if (options.playlistPosition)
        params.set('playlistPosition', '' + options.playlistPosition);
    return buildUrl(url, params);
}
exports.buildPlaylistLink = buildPlaylistLink;
function buildUrl(url, params) {
    var hasParams = false;
    params.forEach(function () { return hasParams = true; });
    if (hasParams)
        return url + '?' + params.toString();
    return url;
}
function generateParams(url) {
    var params = new URLSearchParams(window.location.search);
    // Unused parameters in embed
    params["delete"]('videoId');
    params["delete"]('resume');
    return params;
}
function timeToInt(time) {
    if (!time)
        return 0;
    if (typeof time === 'number')
        return time;
    var reg = /^((\d+)[h:])?((\d+)[m:])?((\d+)s?)?$/;
    var matches = time.match(reg);
    if (!matches)
        return 0;
    var hours = parseInt(matches[2] || '0', 10);
    var minutes = parseInt(matches[4] || '0', 10);
    var seconds = parseInt(matches[6] || '0', 10);
    return hours * 3600 + minutes * 60 + seconds;
}
exports.timeToInt = timeToInt;
function secondsToTime(seconds, full, symbol) {
    if (full === void 0) { full = false; }
    var time = '';
    if (seconds === 0 && !full)
        return '0s';
    var hourSymbol = (symbol || 'h');
    var minuteSymbol = (symbol || 'm');
    var secondsSymbol = full ? '' : 's';
    var hours = Math.floor(seconds / 3600);
    if (hours >= 1)
        time = hours + hourSymbol;
    else if (full)
        time = '0' + hourSymbol;
    seconds %= 3600;
    var minutes = Math.floor(seconds / 60);
    if (minutes >= 1 && minutes < 10 && full)
        time += '0' + minutes + minuteSymbol;
    else if (minutes >= 1)
        time += minutes + minuteSymbol;
    else if (full)
        time += '00' + minuteSymbol;
    seconds %= 60;
    if (seconds >= 1 && seconds < 10 && full)
        time += '0' + seconds + secondsSymbol;
    else if (seconds >= 1)
        time += seconds + secondsSymbol;
    else if (full)
        time += '00';
    return time;
}
exports.secondsToTime = secondsToTime;
function buildVideoOrPlaylistEmbed(embedUrl) {
    return '<iframe width="560" height="315" ' +
        'sandbox="allow-same-origin allow-scripts allow-popups" ' +
        'src="' + embedUrl + '" ' +
        'frameborder="0" allowfullscreen>' +
        '</iframe>';
}
exports.buildVideoOrPlaylistEmbed = buildVideoOrPlaylistEmbed;
function videoFileMaxByResolution(files) {
    var max = files[0];
    for (var i = 1; i < files.length; i++) {
        var file = files[i];
        if (max.resolution.id < file.resolution.id)
            max = file;
    }
    return max;
}
exports.videoFileMaxByResolution = videoFileMaxByResolution;
function videoFileMinByResolution(files) {
    var min = files[0];
    for (var i = 1; i < files.length; i++) {
        var file = files[i];
        if (min.resolution.id > file.resolution.id)
            min = file;
    }
    return min;
}
exports.videoFileMinByResolution = videoFileMinByResolution;
function getRtcConfig() {
    return {
        iceServers: [
            {
                urls: 'stun:stun.stunprotocol.org'
            },
            {
                urls: 'stun:stun.framasoft.org'
            }
        ]
    };
}
exports.getRtcConfig = getRtcConfig;
