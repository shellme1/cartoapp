"use strict";
exports.__esModule = true;
exports.getStoredLastSubtitle = exports.saveLastSubtitle = exports.getAverageBandwidthInStore = exports.saveAverageBandwidth = exports.saveTheaterInStore = exports.saveMuteInStore = exports.saveVolumeInStore = exports.getStoredTheater = exports.getStoredMute = exports.getStoredP2PEnabled = exports.getStoredVolume = void 0;
function getStoredVolume() {
    var value = getLocalStorage('volume');
    if (value !== null && value !== undefined) {
        var valueNumber = parseFloat(value);
        if (isNaN(valueNumber))
            return undefined;
        return valueNumber;
    }
    return undefined;
}
exports.getStoredVolume = getStoredVolume;
function getStoredP2PEnabled() {
    var value = getLocalStorage('webtorrent_enabled');
    if (value !== null && value !== undefined)
        return value === 'true';
    // By default webtorrent is enabled
    return true;
}
exports.getStoredP2PEnabled = getStoredP2PEnabled;
function getStoredMute() {
    var value = getLocalStorage('mute');
    if (value !== null && value !== undefined)
        return value === 'true';
    return undefined;
}
exports.getStoredMute = getStoredMute;
function getStoredTheater() {
    var value = getLocalStorage('theater-enabled');
    if (value !== null && value !== undefined)
        return value === 'true';
    return false;
}
exports.getStoredTheater = getStoredTheater;
function saveVolumeInStore(value) {
    return setLocalStorage('volume', value.toString());
}
exports.saveVolumeInStore = saveVolumeInStore;
function saveMuteInStore(value) {
    return setLocalStorage('mute', value.toString());
}
exports.saveMuteInStore = saveMuteInStore;
function saveTheaterInStore(enabled) {
    return setLocalStorage('theater-enabled', enabled.toString());
}
exports.saveTheaterInStore = saveTheaterInStore;
function saveAverageBandwidth(value) {
    return setLocalStorage('average-bandwidth', value.toString());
}
exports.saveAverageBandwidth = saveAverageBandwidth;
function getAverageBandwidthInStore() {
    var value = getLocalStorage('average-bandwidth');
    if (value !== null && value !== undefined) {
        var valueNumber = parseInt(value, 10);
        if (isNaN(valueNumber))
            return undefined;
        return valueNumber;
    }
    return undefined;
}
exports.getAverageBandwidthInStore = getAverageBandwidthInStore;
function saveLastSubtitle(language) {
    return setLocalStorage('last-subtitle', language);
}
exports.saveLastSubtitle = saveLastSubtitle;
function getStoredLastSubtitle() {
    return getLocalStorage('last-subtitle');
}
exports.getStoredLastSubtitle = getStoredLastSubtitle;
// ---------------------------------------------------------------------------
var KEY_PREFIX = 'peertube-videojs-';
function getLocalStorage(key) {
    try {
        return localStorage.getItem(KEY_PREFIX + key);
    }
    catch (_a) {
        return undefined;
    }
}
function setLocalStorage(key, value) {
    try {
        localStorage.setItem(KEY_PREFIX + key, value);
    }
    catch ( /* empty */_a) { /* empty */
    }
}
