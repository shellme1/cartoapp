"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.videojs = exports.PeertubePlayerManager = void 0;
require("videojs-hotkeys/videojs.hotkeys");
require("videojs-dock");
require("videojs-contextmenu-ui");
require("videojs-contrib-quality-levels");
require("./upnext/end-card");
require("./upnext/upnext-plugin");
require("./bezels/bezels-plugin");
require("./peertube-plugin");
require("./videojs-components/next-previous-video-button");
require("./videojs-components/p2p-info-button");
require("./videojs-components/peertube-link-button");
require("./videojs-components/peertube-load-progress-bar");
require("./videojs-components/resolution-menu-button");
require("./videojs-components/resolution-menu-item");
require("./videojs-components/settings-dialog");
require("./videojs-components/settings-menu-button");
require("./videojs-components/settings-menu-item");
require("./videojs-components/settings-panel");
require("./videojs-components/settings-panel-child");
require("./videojs-components/theater-button");
require("./playlist/playlist-plugin");
var video_js_1 = require("video.js");
exports.videojs = video_js_1["default"];
var i18n_1 = require("@shared/core-utils/i18n");
var redundancy_url_manager_1 = require("./p2p-media-loader/redundancy-url-manager");
var segment_url_builder_1 = require("./p2p-media-loader/segment-url-builder");
var segment_validator_1 = require("./p2p-media-loader/segment-validator");
var peertube_player_local_storage_1 = require("./peertube-player-local-storage");
var translations_manager_1 = require("./translations-manager");
var utils_1 = require("./utils");
var utils_2 = require("../../root-helpers/utils");
// Change 'Playback Rate' to 'Speed' (smaller for our settings menu)
video_js_1["default"].getComponent('PlaybackRateMenuButton').prototype.controlText_ = 'Speed';
var CaptionsButton = video_js_1["default"].getComponent('CaptionsButton');
// Change Captions to Subtitles/CC
CaptionsButton.prototype.controlText_ = 'Subtitles/CC';
// We just want to display 'Off' instead of 'captions off', keep a space so the variable == true (hacky I know)
CaptionsButton.prototype.label_ = ' ';
var PeertubePlayerManager = /** @class */ (function () {
    function PeertubePlayerManager() {
    }
    PeertubePlayerManager.initState = function () {
        PeertubePlayerManager.alreadyPlayed = false;
    };
    PeertubePlayerManager.initialize = function (mode, options, onPlayerChange) {
        return __awaiter(this, void 0, void 0, function () {
            var p2pMediaLoader, videojsOptions, self;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.onPlayerChange = onPlayerChange;
                        this.playerElementClassName = options.common.playerElement.className;
                        if (!(mode === 'webtorrent')) return [3 /*break*/, 2];
                        return [4 /*yield*/, Promise.resolve().then(function () { return require('./webtorrent/webtorrent-plugin'); })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!(mode === 'p2p-media-loader')) return [3 /*break*/, 4];
                        return [4 /*yield*/, Promise.all([
                                Promise.resolve().then(function () { return require('p2p-media-loader-hlsjs'); }),
                                Promise.resolve().then(function () { return require('./p2p-media-loader/p2p-media-loader-plugin'); })
                            ])];
                    case 3:
                        p2pMediaLoader = (_a.sent())[0];
                        _a.label = 4;
                    case 4:
                        videojsOptions = this.getVideojsOptions(mode, options, p2pMediaLoader);
                        return [4 /*yield*/, translations_manager_1.TranslationsManager.loadLocaleInVideoJS(options.common.serverUrl, options.common.language, video_js_1["default"])];
                    case 5:
                        _a.sent();
                        self = this;
                        return [2 /*return*/, new Promise(function (res) {
                                video_js_1["default"](options.common.playerElement, videojsOptions, function () {
                                    var player = this;
                                    var alreadyFallback = false;
                                    player.tech(true).one('error', function () {
                                        if (!alreadyFallback)
                                            self.maybeFallbackToWebTorrent(mode, player, options);
                                        alreadyFallback = true;
                                    });
                                    player.one('error', function () {
                                        if (!alreadyFallback)
                                            self.maybeFallbackToWebTorrent(mode, player, options);
                                        alreadyFallback = true;
                                    });
                                    player.one('play', function () {
                                        PeertubePlayerManager.alreadyPlayed = true;
                                    });
                                    self.addContextMenu(mode, player, options.common.embedUrl);
                                    player.bezels();
                                    return res(player);
                                });
                            })];
                }
            });
        });
    };
    PeertubePlayerManager.maybeFallbackToWebTorrent = function (currentMode, player, options) {
        return __awaiter(this, void 0, void 0, function () {
            var newVideoElement, currentParentPlayerElement, mode, videojsOptions, self;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (currentMode === 'webtorrent')
                            return [2 /*return*/];
                        console.log('Fallback to webtorrent.');
                        newVideoElement = document.createElement('video');
                        newVideoElement.className = this.playerElementClassName;
                        currentParentPlayerElement = options.common.playerElement.parentNode;
                        // Fix on IOS, don't ask me why
                        if (!currentParentPlayerElement)
                            currentParentPlayerElement = document.getElementById(options.common.playerElement.id).parentNode;
                        currentParentPlayerElement.parentNode.insertBefore(newVideoElement, currentParentPlayerElement);
                        options.common.playerElement = newVideoElement;
                        options.common.onPlayerElementChange(newVideoElement);
                        player.dispose();
                        return [4 /*yield*/, Promise.resolve().then(function () { return require('./webtorrent/webtorrent-plugin'); })];
                    case 1:
                        _a.sent();
                        mode = 'webtorrent';
                        videojsOptions = this.getVideojsOptions(mode, options);
                        self = this;
                        video_js_1["default"](newVideoElement, videojsOptions, function () {
                            var player = this;
                            self.addContextMenu(mode, player, options.common.embedUrl);
                            PeertubePlayerManager.onPlayerChange(player);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PeertubePlayerManager.getVideojsOptions = function (mode, options, p2pMediaLoaderModule) {
        var commonOptions = options.common;
        var isHLS = mode === 'p2p-media-loader';
        var autoplay = this.getAutoPlayValue(commonOptions.autoplay);
        var html5 = {};
        var plugins = {
            peertube: {
                mode: mode,
                autoplay: autoplay,
                videoViewUrl: commonOptions.videoViewUrl,
                videoDuration: commonOptions.videoDuration,
                userWatching: commonOptions.userWatching,
                subtitle: commonOptions.subtitle,
                videoCaptions: commonOptions.videoCaptions,
                stopTime: commonOptions.stopTime,
                isLive: commonOptions.isLive
            }
        };
        if (commonOptions.playlist) {
            plugins.playlist = commonOptions.playlist;
        }
        if (commonOptions.enableHotkeys === true) {
            PeertubePlayerManager.addHotkeysOptions(plugins);
        }
        if (isHLS) {
            var hlsjs = PeertubePlayerManager.addP2PMediaLoaderOptions(plugins, options, p2pMediaLoaderModule).hlsjs;
            html5 = hlsjs.html5;
        }
        if (mode === 'webtorrent') {
            PeertubePlayerManager.addWebTorrentOptions(plugins, options);
            // WebTorrent plugin handles autoplay, because we do some hackish stuff in there
            autoplay = false;
        }
        var videojsOptions = {
            html5: html5,
            // We don't use text track settings for now
            textTrackSettings: false,
            controls: commonOptions.controls !== undefined ? commonOptions.controls : true,
            loop: commonOptions.loop !== undefined ? commonOptions.loop : false,
            muted: commonOptions.muted !== undefined
                ? commonOptions.muted
                : undefined,
            autoplay: this.getAutoPlayValue(autoplay),
            poster: commonOptions.poster,
            inactivityTimeout: commonOptions.inactivityTimeout,
            playbackRates: [0.5, 0.75, 1, 1.25, 1.5, 2],
            plugins: plugins,
            controlBar: {
                children: this.getControlBarChildren(mode, {
                    captions: commonOptions.captions,
                    peertubeLink: commonOptions.peertubeLink,
                    theaterButton: commonOptions.theaterButton,
                    nextVideo: commonOptions.nextVideo,
                    hasNextVideo: commonOptions.hasNextVideo,
                    previousVideo: commonOptions.previousVideo,
                    hasPreviousVideo: commonOptions.hasPreviousVideo
                }) // FIXME: typings
            }
        };
        if (commonOptions.language && !i18n_1.isDefaultLocale(commonOptions.language)) {
            Object.assign(videojsOptions, { language: commonOptions.language });
        }
        return videojsOptions;
    };
    PeertubePlayerManager.addP2PMediaLoaderOptions = function (plugins, options, p2pMediaLoaderModule) {
        var p2pMediaLoaderOptions = options.p2pMediaLoader;
        var commonOptions = options.common;
        var trackerAnnounce = p2pMediaLoaderOptions.trackerAnnounce
            .filter(function (t) { return t.startsWith('ws'); });
        var redundancyUrlManager = new redundancy_url_manager_1.RedundancyUrlManager(options.p2pMediaLoader.redundancyBaseUrls);
        var p2pMediaLoader = {
            redundancyUrlManager: redundancyUrlManager,
            type: 'application/x-mpegURL',
            startTime: commonOptions.startTime,
            src: p2pMediaLoaderOptions.playlistUrl
        };
        var consumeOnly = false;
        // FIXME: typings
        if (navigator && navigator.connection && navigator.connection.type === 'cellular') {
            console.log('We are on a cellular connection: disabling seeding.');
            consumeOnly = true;
        }
        var p2pMediaLoaderConfig = {
            loader: {
                trackerAnnounce: trackerAnnounce,
                segmentValidator: segment_validator_1.segmentValidatorFactory(options.p2pMediaLoader.segmentsSha256Url, options.common.isLive),
                rtcConfig: utils_1.getRtcConfig(),
                requiredSegmentsPriority: 1,
                segmentUrlBuilder: segment_url_builder_1.segmentUrlBuilderFactory(redundancyUrlManager),
                useP2P: peertube_player_local_storage_1.getStoredP2PEnabled(),
                consumeOnly: consumeOnly
            },
            segments: {
                swarmId: p2pMediaLoaderOptions.playlistUrl
            }
        };
        var hlsjs = {
            levelLabelHandler: function (level) {
                var resolution = Math.min(level.height || 0, level.width || 0);
                var file = p2pMediaLoaderOptions.videoFiles.find(function (f) { return f.resolution.id === resolution; });
                // We don't have files for live videos
                if (!file)
                    return level.height;
                var label = file.resolution.label;
                if (file.fps >= 50)
                    label += file.fps;
                return label;
            },
            html5: {
                hlsjsConfig: {
                    capLevelToPlayerSize: true,
                    autoStartLoad: false,
                    liveSyncDurationCount: 5,
                    loader: new p2pMediaLoaderModule.Engine(p2pMediaLoaderConfig).createLoaderClass()
                }
            }
        };
        var toAssign = { p2pMediaLoader: p2pMediaLoader, hlsjs: hlsjs };
        Object.assign(plugins, toAssign);
        return toAssign;
    };
    PeertubePlayerManager.addWebTorrentOptions = function (plugins, options) {
        var commonOptions = options.common;
        var webtorrentOptions = options.webtorrent;
        var autoplay = this.getAutoPlayValue(commonOptions.autoplay) === 'play'
            ? true
            : false;
        var webtorrent = {
            autoplay: autoplay,
            videoDuration: commonOptions.videoDuration,
            playerElement: commonOptions.playerElement,
            videoFiles: webtorrentOptions.videoFiles,
            startTime: commonOptions.startTime
        };
        Object.assign(plugins, { webtorrent: webtorrent });
    };
    PeertubePlayerManager.getControlBarChildren = function (mode, options) {
        var _a;
        var settingEntries = [];
        var loadProgressBar = mode === 'webtorrent' ? 'peerTubeLoadProgressBar' : 'loadProgressBar';
        // Keep an order
        settingEntries.push('playbackRateMenuButton');
        if (options.captions === true)
            settingEntries.push('captionsButton');
        settingEntries.push('resolutionMenuButton');
        var children = {};
        if (options.previousVideo) {
            var buttonOptions = {
                type: 'previous',
                handler: options.previousVideo,
                isDisabled: function () {
                    if (!options.hasPreviousVideo)
                        return false;
                    return !options.hasPreviousVideo();
                }
            };
            Object.assign(children, {
                'previousVideoButton': buttonOptions
            });
        }
        Object.assign(children, { playToggle: {} });
        if (options.nextVideo) {
            var buttonOptions = {
                type: 'next',
                handler: options.nextVideo,
                isDisabled: function () {
                    if (!options.hasNextVideo)
                        return false;
                    return !options.hasNextVideo();
                }
            };
            Object.assign(children, {
                'nextVideoButton': buttonOptions
            });
        }
        Object.assign(children, {
            'currentTimeDisplay': {},
            'timeDivider': {},
            'durationDisplay': {},
            'liveDisplay': {},
            'flexibleWidthSpacer': {},
            'progressControl': {
                children: {
                    'seekBar': {
                        children: (_a = {},
                            _a[loadProgressBar] = {},
                            _a['mouseTimeDisplay'] = {},
                            _a['playProgressBar'] = {},
                            _a)
                    }
                }
            },
            'p2PInfoButton': {},
            'muteToggle': {},
            'volumeControl': {},
            'settingsButton': {
                setup: {
                    maxHeightOffset: 40
                },
                entries: settingEntries
            }
        });
        if (options.peertubeLink === true) {
            Object.assign(children, {
                'peerTubeLinkButton': {}
            });
        }
        if (options.theaterButton === true) {
            Object.assign(children, {
                'theaterButton': {}
            });
        }
        Object.assign(children, {
            'fullscreenToggle': {}
        });
        return children;
    };
    PeertubePlayerManager.addContextMenu = function (mode, player, videoEmbedUrl) {
        var content = [
            {
                label: player.localize('Copy the video URL'),
                listener: function () {
                    utils_2.copyToClipboard(utils_1.buildVideoLink());
                }
            },
            {
                label: player.localize('Copy the video URL at the current time'),
                listener: function () {
                    utils_2.copyToClipboard(utils_1.buildVideoLink({ startTime: this.currentTime() }));
                }
            },
            {
                label: player.localize('Copy embed code'),
                listener: function () {
                    utils_2.copyToClipboard(utils_1.buildVideoOrPlaylistEmbed(videoEmbedUrl));
                }
            }
        ];
        if (mode === 'webtorrent') {
            content.push({
                label: player.localize('Copy magnet URI'),
                listener: function () {
                    utils_2.copyToClipboard(this.webtorrent().getCurrentVideoFile().magnetUri);
                }
            });
        }
        player.contextmenuUI({ content: content });
    };
    PeertubePlayerManager.addHotkeysOptions = function (plugins) {
        Object.assign(plugins, {
            hotkeys: {
                skipInitialFocus: true,
                enableInactiveFocus: false,
                captureDocumentHotkeys: true,
                documentHotkeysFocusElementFilter: function (e) {
                    var tagName = e.tagName.toLowerCase();
                    return e.id === 'content' || tagName === 'body' || tagName === 'video';
                },
                enableVolumeScroll: false,
                enableModifiersForNumbers: false,
                fullscreenKey: function (event) {
                    // fullscreen with the f key or Ctrl+Enter
                    return event.key === 'f' || (event.ctrlKey && event.key === 'Enter');
                },
                seekStep: function (event) {
                    // mimic VLC seek behavior, and default to 5 (original value is 5).
                    if (event.ctrlKey && event.altKey) {
                        return 5 * 60;
                    }
                    else if (event.ctrlKey) {
                        return 60;
                    }
                    else if (event.altKey) {
                        return 10;
                    }
                    else {
                        return 5;
                    }
                },
                customKeys: {
                    increasePlaybackRateKey: {
                        key: function (event) {
                            return event.key === '>';
                        },
                        handler: function (player) {
                            var newValue = Math.min(player.playbackRate() + 0.1, 5);
                            player.playbackRate(parseFloat(newValue.toFixed(2)));
                        }
                    },
                    decreasePlaybackRateKey: {
                        key: function (event) {
                            return event.key === '<';
                        },
                        handler: function (player) {
                            var newValue = Math.max(player.playbackRate() - 0.1, 0.10);
                            player.playbackRate(parseFloat(newValue.toFixed(2)));
                        }
                    },
                    frameByFrame: {
                        key: function (event) {
                            return event.key === '.';
                        },
                        handler: function (player) {
                            player.pause();
                            // Calculate movement distance (assuming 30 fps)
                            var dist = 1 / 30;
                            player.currentTime(player.currentTime() + dist);
                        }
                    }
                }
            }
        });
    };
    PeertubePlayerManager.getAutoPlayValue = function (autoplay) {
        if (autoplay !== true)
            return autoplay;
        // On first play, disable autoplay to avoid issues
        // But if the player already played videos, we can safely autoplay next ones
        if (utils_1.isIOS() || utils_1.isSafari()) {
            return PeertubePlayerManager.alreadyPlayed ? 'play' : false;
        }
        return 'play';
    };
    PeertubePlayerManager.alreadyPlayed = false;
    return PeertubePlayerManager;
}());
exports.PeertubePlayerManager = PeertubePlayerManager;
