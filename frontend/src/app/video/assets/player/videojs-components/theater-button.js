"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var video_js_1 = require("video.js");
var peertube_player_local_storage_1 = require("../peertube-player-local-storage");
var Button = video_js_1["default"].getComponent('Button');
var TheaterButton = /** @class */ (function (_super) {
    __extends(TheaterButton, _super);
    function TheaterButton(player, options) {
        var _this = _super.call(this, player, options) || this;
        var enabled = peertube_player_local_storage_1.getStoredTheater();
        if (enabled === true) {
            _this.player().addClass(TheaterButton.THEATER_MODE_CLASS);
            _this.handleTheaterChange();
        }
        _this.controlText('Theater mode');
        _this.player().theaterEnabled = enabled;
        return _this;
    }
    TheaterButton.prototype.buildCSSClass = function () {
        return "vjs-theater-control " + _super.prototype.buildCSSClass.call(this);
    };
    TheaterButton.prototype.handleTheaterChange = function () {
        var theaterEnabled = this.isTheaterEnabled();
        if (theaterEnabled) {
            this.controlText('Normal mode');
        }
        else {
            this.controlText('Theater mode');
        }
        peertube_player_local_storage_1.saveTheaterInStore(theaterEnabled);
        this.player_.trigger('theaterChange', theaterEnabled);
    };
    TheaterButton.prototype.handleClick = function () {
        this.player_.toggleClass(TheaterButton.THEATER_MODE_CLASS);
        this.handleTheaterChange();
    };
    TheaterButton.prototype.isTheaterEnabled = function () {
        return this.player_.hasClass(TheaterButton.THEATER_MODE_CLASS);
    };
    TheaterButton.THEATER_MODE_CLASS = 'vjs-theater-enabled';
    return TheaterButton;
}(Button));
video_js_1["default"].registerComponent('TheaterButton', TheaterButton);
