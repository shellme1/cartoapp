"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.SettingsButton = void 0;
// Thanks to Yanko Shterev: https://github.com/yshterev/videojs-settings-menu
var settings_menu_item_1 = require("./settings-menu-item");
var utils_1 = require("../utils");
var video_js_1 = require("video.js");
var Button = video_js_1["default"].getComponent('Button');
var Menu = video_js_1["default"].getComponent('Menu');
var Component = video_js_1["default"].getComponent('Component');
var SettingsButton = /** @class */ (function (_super) {
    __extends(SettingsButton, _super);
    function SettingsButton(player, options) {
        var _this = _super.call(this, player, options) || this;
        _this.settingsButtonOptions = options;
        _this.controlText('Settings');
        _this.dialog = _this.player().addChild('settingsDialog');
        _this.dialogEl = _this.dialog.el();
        _this.menu = null;
        _this.panel = _this.dialog.addChild('settingsPanel');
        _this.panelChild = _this.panel.addChild('settingsPanelChild');
        _this.addClass('vjs-settings');
        _this.el().setAttribute('aria-label', 'Settings Button');
        // Event handlers
        _this.addSettingsItemHandler = _this.onAddSettingsItem.bind(_this);
        _this.disposeSettingsItemHandler = _this.onDisposeSettingsItem.bind(_this);
        _this.playerClickHandler = _this.onPlayerClick.bind(_this);
        _this.userInactiveHandler = _this.onUserInactive.bind(_this);
        _this.buildMenu();
        _this.bindEvents();
        // Prepare the dialog
        _this.player().one('play', function () { return _this.hideDialog(); });
        return _this;
    }
    SettingsButton.prototype.onPlayerClick = function (event) {
        var element = event.target;
        if (element.classList.contains('vjs-settings') || element.parentElement.classList.contains('vjs-settings')) {
            return;
        }
        if (!this.dialog.hasClass('vjs-hidden')) {
            this.hideDialog();
        }
    };
    SettingsButton.prototype.onDisposeSettingsItem = function (event, name) {
        if (name === undefined) {
            var children = this.menu.children();
            while (children.length > 0) {
                children[0].dispose();
                this.menu.removeChild(children[0]);
            }
            this.addClass('vjs-hidden');
        }
        else {
            var item = this.menu.getChild(name);
            if (item) {
                item.dispose();
                this.menu.removeChild(item);
            }
        }
        this.hideDialog();
        if (this.settingsButtonOptions.entries.length === 0) {
            this.addClass('vjs-hidden');
        }
    };
    SettingsButton.prototype.onAddSettingsItem = function (event, data) {
        var entry = data[0], options = data[1];
        this.addMenuItem(entry, options);
        this.removeClass('vjs-hidden');
    };
    SettingsButton.prototype.onUserInactive = function () {
        if (!this.dialog.hasClass('vjs-hidden')) {
            this.hideDialog();
        }
    };
    SettingsButton.prototype.bindEvents = function () {
        this.player().on('click', this.playerClickHandler);
        this.player().on('addsettingsitem', this.addSettingsItemHandler);
        this.player().on('disposesettingsitem', this.disposeSettingsItemHandler);
        this.player().on('userinactive', this.userInactiveHandler);
    };
    SettingsButton.prototype.buildCSSClass = function () {
        return "vjs-icon-settings " + _super.prototype.buildCSSClass.call(this);
    };
    SettingsButton.prototype.handleClick = function () {
        if (this.dialog.hasClass('vjs-hidden')) {
            this.showDialog();
        }
        else {
            this.hideDialog();
        }
    };
    SettingsButton.prototype.showDialog = function () {
        this.player().peertube().onMenuOpen();
        this.menu.el().style.opacity = '1';
        this.dialog.show();
        this.setDialogSize(this.getComponentSize(this.menu));
    };
    SettingsButton.prototype.hideDialog = function () {
        this.player_.peertube().onMenuClosed();
        this.dialog.hide();
        this.setDialogSize(this.getComponentSize(this.menu));
        this.menu.el().style.opacity = '1';
        this.resetChildren();
    };
    SettingsButton.prototype.getComponentSize = function (element) {
        var width = null;
        var height = null;
        // Could be component or just DOM element
        if (element instanceof Component) {
            var el = element.el();
            width = el.offsetWidth;
            height = el.offsetHeight;
            element.width = width;
            element.height = height;
        }
        else {
            width = element.offsetWidth;
            height = element.offsetHeight;
        }
        return [width, height];
    };
    SettingsButton.prototype.setDialogSize = function (_a) {
        var width = _a[0], height = _a[1];
        if (typeof height !== 'number') {
            return;
        }
        var offset = this.settingsButtonOptions.setup.maxHeightOffset;
        var maxHeight = this.player().el().offsetHeight - offset; // FIXME: typings
        var panelEl = this.panel.el();
        if (height > maxHeight) {
            height = maxHeight;
            width += 17;
            panelEl.style.maxHeight = height + "px";
        }
        else if (panelEl.style.maxHeight !== '') {
            panelEl.style.maxHeight = '';
        }
        this.dialogEl.style.width = width + "px";
        this.dialogEl.style.height = height + "px";
    };
    SettingsButton.prototype.buildMenu = function () {
        this.menu = new Menu(this.player());
        this.menu.addClass('vjs-main-menu');
        var entries = this.settingsButtonOptions.entries;
        if (entries.length === 0) {
            this.addClass('vjs-hidden');
            this.panelChild.addChild(this.menu);
            return;
        }
        for (var _i = 0, entries_1 = entries; _i < entries_1.length; _i++) {
            var entry = entries_1[_i];
            this.addMenuItem(entry, this.settingsButtonOptions);
        }
        this.panelChild.addChild(this.menu);
    };
    SettingsButton.prototype.addMenuItem = function (entry, options) {
        var openSubMenu = function () {
            if (video_js_1["default"].dom.hasClass(this.el_, 'open')) {
                video_js_1["default"].dom.removeClass(this.el_, 'open');
            }
            else {
                video_js_1["default"].dom.addClass(this.el_, 'open');
            }
        };
        options.name = utils_1.toTitleCase(entry);
        var newOptions = Object.assign({}, options, { entry: entry, menuButton: this });
        var settingsMenuItem = new settings_menu_item_1.SettingsMenuItem(this.player(), newOptions);
        this.menu.addChild(settingsMenuItem);
        // Hide children to avoid sub menus stacking on top of each other
        // or having multiple menus open
        settingsMenuItem.on('click', video_js_1["default"].bind(this, this.hideChildren));
        // Whether to add or remove selected class on the settings sub menu element
        settingsMenuItem.on('click', openSubMenu);
    };
    SettingsButton.prototype.resetChildren = function () {
        for (var _i = 0, _a = this.menu.children(); _i < _a.length; _i++) {
            var menuChild = _a[_i];
            menuChild.reset();
        }
    };
    /**
     * Hide all the sub menus
     */
    SettingsButton.prototype.hideChildren = function () {
        for (var _i = 0, _a = this.menu.children(); _i < _a.length; _i++) {
            var menuChild = _a[_i];
            menuChild.hideSubMenu();
        }
    };
    return SettingsButton;
}(Button));
exports.SettingsButton = SettingsButton;
Component.registerComponent('SettingsButton', SettingsButton);
