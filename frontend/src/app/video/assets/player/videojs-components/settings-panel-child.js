"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.SettingsPanelChild = void 0;
var video_js_1 = require("video.js");
var Component = video_js_1["default"].getComponent('Component');
var SettingsPanelChild = /** @class */ (function (_super) {
    __extends(SettingsPanelChild, _super);
    function SettingsPanelChild(player, options) {
        return _super.call(this, player, options) || this;
    }
    SettingsPanelChild.prototype.createEl = function () {
        return _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-settings-panel-child',
            innerHTML: '',
            tabIndex: -1
        });
    };
    return SettingsPanelChild;
}(Component));
exports.SettingsPanelChild = SettingsPanelChild;
Component.registerComponent('SettingsPanelChild', SettingsPanelChild);
