"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var video_js_1 = require("video.js");
var Component = video_js_1["default"].getComponent('Component');
var PeerTubeLoadProgressBar = /** @class */ (function (_super) {
    __extends(PeerTubeLoadProgressBar, _super);
    function PeerTubeLoadProgressBar(player, options) {
        var _this = _super.call(this, player, options) || this;
        _this.on(player, 'progress', _this.update);
        return _this;
    }
    PeerTubeLoadProgressBar.prototype.createEl = function () {
        return _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-load-progress',
            innerHTML: "<span class=\"vjs-control-text\"><span>" + this.localize('Loaded') + "</span>: 0%</span>"
        });
    };
    PeerTubeLoadProgressBar.prototype.dispose = function () {
        _super.prototype.dispose.call(this);
    };
    PeerTubeLoadProgressBar.prototype.update = function () {
        var torrent = this.player().webtorrent().getTorrent();
        if (!torrent)
            return;
        // FIXME: typings
        this.el().style.width = (torrent.progress * 100) + '%';
    };
    return PeerTubeLoadProgressBar;
}(Component));
Component.registerComponent('PeerTubeLoadProgressBar', PeerTubeLoadProgressBar);
