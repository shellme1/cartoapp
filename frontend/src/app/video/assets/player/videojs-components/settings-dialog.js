"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.SettingsDialog = void 0;
var video_js_1 = require("video.js");
var Component = video_js_1["default"].getComponent('Component');
var SettingsDialog = /** @class */ (function (_super) {
    __extends(SettingsDialog, _super);
    function SettingsDialog(player) {
        var _this = _super.call(this, player) || this;
        _this.hide();
        return _this;
    }
    /**
     * Create the component's DOM element
     *
     * @return {Element}
     * @method createEl
     */
    SettingsDialog.prototype.createEl = function () {
        var uniqueId = this.id();
        var dialogLabelId = 'TTsettingsDialogLabel-' + uniqueId;
        var dialogDescriptionId = 'TTsettingsDialogDescription-' + uniqueId;
        return _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-settings-dialog vjs-modal-overlay',
            innerHTML: '',
            tabIndex: -1
        }, {
            'role': 'dialog',
            'aria-labelledby': dialogLabelId,
            'aria-describedby': dialogDescriptionId
        });
    };
    return SettingsDialog;
}(Component));
exports.SettingsDialog = SettingsDialog;
Component.registerComponent('SettingsDialog', SettingsDialog);
