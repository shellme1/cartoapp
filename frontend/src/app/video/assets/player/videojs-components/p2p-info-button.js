"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var video_js_1 = require("video.js");
var utils_1 = require("../utils");
var Button = video_js_1["default"].getComponent('Button');
var P2pInfoButton = /** @class */ (function (_super) {
    __extends(P2pInfoButton, _super);
    function P2pInfoButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    P2pInfoButton.prototype.createEl = function () {
        var _this = this;
        var div = video_js_1["default"].dom.createEl('div', {
            className: 'vjs-peertube'
        });
        var subDivWebtorrent = video_js_1["default"].dom.createEl('div', {
            className: 'vjs-peertube-hidden' // Hide the stats before we get the info
        });
        div.appendChild(subDivWebtorrent);
        var downloadIcon = video_js_1["default"].dom.createEl('span', {
            className: 'icon icon-download'
        });
        subDivWebtorrent.appendChild(downloadIcon);
        var downloadSpeedText = video_js_1["default"].dom.createEl('span', {
            className: 'download-speed-text'
        });
        var downloadSpeedNumber = video_js_1["default"].dom.createEl('span', {
            className: 'download-speed-number'
        });
        var downloadSpeedUnit = video_js_1["default"].dom.createEl('span');
        downloadSpeedText.appendChild(downloadSpeedNumber);
        downloadSpeedText.appendChild(downloadSpeedUnit);
        subDivWebtorrent.appendChild(downloadSpeedText);
        var uploadIcon = video_js_1["default"].dom.createEl('span', {
            className: 'icon icon-upload'
        });
        subDivWebtorrent.appendChild(uploadIcon);
        var uploadSpeedText = video_js_1["default"].dom.createEl('span', {
            className: 'upload-speed-text'
        });
        var uploadSpeedNumber = video_js_1["default"].dom.createEl('span', {
            className: 'upload-speed-number'
        });
        var uploadSpeedUnit = video_js_1["default"].dom.createEl('span');
        uploadSpeedText.appendChild(uploadSpeedNumber);
        uploadSpeedText.appendChild(uploadSpeedUnit);
        subDivWebtorrent.appendChild(uploadSpeedText);
        var peersText = video_js_1["default"].dom.createEl('span', {
            className: 'peers-text'
        });
        var peersNumber = video_js_1["default"].dom.createEl('span', {
            className: 'peers-number'
        });
        subDivWebtorrent.appendChild(peersNumber);
        subDivWebtorrent.appendChild(peersText);
        var subDivHttp = video_js_1["default"].dom.createEl('div', {
            className: 'vjs-peertube-hidden'
        });
        var subDivHttpText = video_js_1["default"].dom.createEl('span', {
            className: 'http-fallback',
            textContent: 'HTTP'
        });
        subDivHttp.appendChild(subDivHttpText);
        div.appendChild(subDivHttp);
        this.player_.on('p2pInfo', function (event, data) {
            // We are in HTTP fallback
            if (!data) {
                subDivHttp.className = 'vjs-peertube-displayed';
                subDivWebtorrent.className = 'vjs-peertube-hidden';
                return;
            }
            var p2pStats = data.p2p;
            var httpStats = data.http;
            var downloadSpeed = utils_1.bytes(p2pStats.downloadSpeed + httpStats.downloadSpeed);
            var uploadSpeed = utils_1.bytes(p2pStats.uploadSpeed + httpStats.uploadSpeed);
            var totalDownloaded = utils_1.bytes(p2pStats.downloaded + httpStats.downloaded);
            var totalUploaded = utils_1.bytes(p2pStats.uploaded + httpStats.uploaded);
            var numPeers = p2pStats.numPeers;
            subDivWebtorrent.title = _this.player().localize('Total downloaded: ') + totalDownloaded.join(' ') + '\n';
            if (data.source === 'p2p-media-loader') {
                var downloadedFromServer = utils_1.bytes(httpStats.downloaded).join(' ');
                var downloadedFromPeers = utils_1.bytes(p2pStats.downloaded).join(' ');
                subDivWebtorrent.title +=
                    ' * ' + _this.player().localize('From servers: ') + downloadedFromServer + '\n' +
                        ' * ' + _this.player().localize('From peers: ') + downloadedFromPeers + '\n';
            }
            subDivWebtorrent.title += _this.player().localize('Total uploaded: ') + totalUploaded.join(' ');
            downloadSpeedNumber.textContent = downloadSpeed[0];
            downloadSpeedUnit.textContent = ' ' + downloadSpeed[1];
            uploadSpeedNumber.textContent = uploadSpeed[0];
            uploadSpeedUnit.textContent = ' ' + uploadSpeed[1];
            peersNumber.textContent = numPeers.toString();
            peersText.textContent = ' ' + (numPeers > 1 ? _this.player().localize('peers') : _this.player_.localize('peer'));
            subDivHttp.className = 'vjs-peertube-hidden';
            subDivWebtorrent.className = 'vjs-peertube-displayed';
        });
        return div;
    };
    return P2pInfoButton;
}(Button));
video_js_1["default"].registerComponent('P2PInfoButton', P2pInfoButton);
