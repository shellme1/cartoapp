"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var video_js_1 = require("video.js");
var resolution_menu_item_1 = require("./resolution-menu-item");
var Menu = video_js_1["default"].getComponent('Menu');
var MenuButton = video_js_1["default"].getComponent('MenuButton');
var ResolutionMenuButton = /** @class */ (function (_super) {
    __extends(ResolutionMenuButton, _super);
    function ResolutionMenuButton(player, options) {
        var _this = _super.call(this, player, options) || this;
        _this.controlText('Quality');
        player.tech(true).on('loadedqualitydata', function (e, data) { return _this.buildQualities(data); });
        player.peertube().on('resolutionChange', function () { return setTimeout(function () { return _this.trigger('updateLabel'); }, 0); });
        return _this;
    }
    ResolutionMenuButton.prototype.createEl = function () {
        var el = _super.prototype.createEl.call(this);
        this.labelEl_ = video_js_1["default"].dom.createEl('div', {
            className: 'vjs-resolution-value'
        });
        el.appendChild(this.labelEl_);
        return el;
    };
    ResolutionMenuButton.prototype.updateARIAAttributes = function () {
        this.el().setAttribute('aria-label', 'Quality');
    };
    ResolutionMenuButton.prototype.createMenu = function () {
        return new Menu(this.player_);
    };
    ResolutionMenuButton.prototype.buildCSSClass = function () {
        return _super.prototype.buildCSSClass.call(this) + ' vjs-resolution-button';
    };
    ResolutionMenuButton.prototype.buildWrapperCSSClass = function () {
        return 'vjs-resolution-control ' + _super.prototype.buildWrapperCSSClass.call(this);
    };
    ResolutionMenuButton.prototype.addClickListener = function (component) {
        var _this = this;
        component.on('click', function () {
            var children = _this.menu.children();
            for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
                var child = children_1[_i];
                if (component !== child) {
                    child.selected(false);
                }
            }
        });
    };
    ResolutionMenuButton.prototype.buildQualities = function (data) {
        // The automatic resolution item will need other labels
        var labels = {};
        data.qualityData.video.sort(function (a, b) {
            if (a.id > b.id)
                return -1;
            if (a.id === b.id)
                return 0;
            return 1;
        });
        for (var _i = 0, _a = data.qualityData.video; _i < _a.length; _i++) {
            var d = _a[_i];
            // Skip auto resolution, we'll add it ourselves
            if (d.id === -1)
                continue;
            var label = d.label === '0p'
                ? this.player().localize('Audio-only')
                : d.label;
            this.menu.addChild(new resolution_menu_item_1.ResolutionMenuItem(this.player_, {
                id: d.id,
                label: label,
                selected: d.selected,
                callback: data.qualitySwitchCallback
            }));
            labels[d.id] = d.label;
        }
        this.menu.addChild(new resolution_menu_item_1.ResolutionMenuItem(this.player_, {
            id: -1,
            label: this.player_.localize('Auto'),
            labels: labels,
            callback: data.qualitySwitchCallback,
            selected: true // By default, in auto mode
        }));
        for (var _b = 0, _c = this.menu.children(); _b < _c.length; _b++) {
            var m = _c[_b];
            this.addClickListener(m);
        }
        this.trigger('menuChanged');
    };
    return ResolutionMenuButton;
}(MenuButton));
video_js_1["default"].registerComponent('ResolutionMenuButton', ResolutionMenuButton);
