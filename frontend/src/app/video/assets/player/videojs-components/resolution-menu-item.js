"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.ResolutionMenuItem = void 0;
var video_js_1 = require("video.js");
var MenuItem = video_js_1["default"].getComponent('MenuItem');
var ResolutionMenuItem = /** @class */ (function (_super) {
    __extends(ResolutionMenuItem, _super);
    function ResolutionMenuItem(player, options) {
        var _this = this;
        options.selectable = true;
        _this = _super.call(this, player, options) || this;
        _this.autoResolutionPossible = true;
        _this.currentResolutionLabel = '';
        _this.resolutionId = options.id;
        _this.label = options.label;
        _this.labels = options.labels;
        _this.callback = options.callback;
        player.peertube().on('resolutionChange', function (_, data) { return _this.updateSelection(data); });
        // We only want to disable the "Auto" item
        if (_this.resolutionId === -1) {
            player.peertube().on('autoResolutionChange', function (_, data) { return _this.updateAutoResolution(data); });
        }
        return _this;
    }
    ResolutionMenuItem.prototype.handleClick = function (event) {
        // Auto button disabled?
        if (this.autoResolutionPossible === false && this.resolutionId === -1)
            return;
        _super.prototype.handleClick.call(this, event);
        this.callback(this.resolutionId, 'video');
    };
    ResolutionMenuItem.prototype.updateSelection = function (data) {
        if (this.resolutionId === -1) {
            this.currentResolutionLabel = this.labels[data.id];
        }
        // Automatic resolution only
        if (data.auto === true) {
            this.selected(this.resolutionId === -1);
            return;
        }
        this.selected(this.resolutionId === data.id);
    };
    ResolutionMenuItem.prototype.updateAutoResolution = function (data) {
        // Check if the auto resolution is enabled or not
        if (data.possible === false) {
            this.addClass('disabled');
        }
        else {
            this.removeClass('disabled');
        }
        this.autoResolutionPossible = data.possible;
    };
    ResolutionMenuItem.prototype.getLabel = function () {
        if (this.resolutionId === -1) {
            return this.label + ' <small>' + this.currentResolutionLabel + '</small>';
        }
        return this.label;
    };
    return ResolutionMenuItem;
}(MenuItem));
exports.ResolutionMenuItem = ResolutionMenuItem;
video_js_1["default"].registerComponent('ResolutionMenuItem', ResolutionMenuItem);
