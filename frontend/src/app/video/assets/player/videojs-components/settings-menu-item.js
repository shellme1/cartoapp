"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.SettingsMenuItem = void 0;
// Thanks to Yanko Shterev: https://github.com/yshterev/videojs-settings-menu
var utils_1 = require("../utils");
var video_js_1 = require("video.js");
var MenuItem = video_js_1["default"].getComponent('MenuItem');
var component = video_js_1["default"].getComponent('Component');
var SettingsMenuItem = /** @class */ (function (_super) {
    __extends(SettingsMenuItem, _super);
    function SettingsMenuItem(player, options) {
        var _this = _super.call(this, player, options) || this;
        _this.settingsButton = options.menuButton;
        _this.dialog = _this.settingsButton.dialog;
        _this.mainMenu = _this.settingsButton.menu;
        _this.panel = _this.dialog.getChild('settingsPanel');
        _this.panelChild = _this.panel.getChild('settingsPanelChild');
        _this.panelChildEl = _this.panelChild.el();
        _this.size = null;
        // keep state of what menu type is loading next
        _this.menuToLoad = 'mainmenu';
        var subMenuName = utils_1.toTitleCase(options.entry);
        var SubMenuComponent = video_js_1["default"].getComponent(subMenuName);
        if (!SubMenuComponent) {
            throw new Error("Component " + subMenuName + " does not exist");
        }
        var newOptions = Object.assign({}, options, { entry: options.menuButton, menuButton: _this });
        _this.subMenu = new SubMenuComponent(_this.player(), newOptions); // FIXME: typings
        var subMenuClass = _this.subMenu.buildCSSClass().split(' ')[0];
        _this.settingsSubMenuEl_.className += ' ' + subMenuClass;
        _this.eventHandlers();
        player.ready(function () {
            // Voodoo magic for IOS
            setTimeout(function () {
                // Player was destroyed
                if (!_this.player_)
                    return;
                _this.build();
                // Update on rate change
                player.on('ratechange', _this.submenuClickHandler);
                if (subMenuName === 'CaptionsButton') {
                    // Hack to regenerate captions on HTTP fallback
                    player.on('captionsChanged', function () {
                        setTimeout(function () {
                            _this.settingsSubMenuEl_.innerHTML = '';
                            _this.settingsSubMenuEl_.appendChild(_this.subMenu.menu.el());
                            _this.update();
                            _this.bindClickEvents();
                        }, 0);
                    });
                }
                _this.reset();
            }, 0);
        });
        return _this;
    }
    SettingsMenuItem.prototype.eventHandlers = function () {
        this.submenuClickHandler = this.onSubmenuClick.bind(this);
        this.transitionEndHandler = this.onTransitionEnd.bind(this);
    };
    SettingsMenuItem.prototype.onSubmenuClick = function (event) {
        var _this = this;
        var target = null;
        if (event.type === 'tap') {
            target = event.target;
        }
        else {
            target = event.currentTarget;
        }
        if (target && target.classList.contains('vjs-back-button')) {
            this.loadMainMenu();
            return;
        }
        // To update the sub menu value on click, setTimeout is needed because
        // updating the value is not instant
        setTimeout(function () { return _this.update(event); }, 0);
        // Seems like videojs adds a vjs-hidden class on the caption menu after a click
        // We don't need it
        this.subMenu.menu.removeClass('vjs-hidden');
    };
    /**
     * Create the component's DOM element
     *
     * @return {Element}
     * @method createEl
     */
    SettingsMenuItem.prototype.createEl = function () {
        var el = video_js_1["default"].dom.createEl('li', {
            className: 'vjs-menu-item'
        });
        this.settingsSubMenuTitleEl_ = video_js_1["default"].dom.createEl('div', {
            className: 'vjs-settings-sub-menu-title'
        });
        el.appendChild(this.settingsSubMenuTitleEl_);
        this.settingsSubMenuValueEl_ = video_js_1["default"].dom.createEl('div', {
            className: 'vjs-settings-sub-menu-value'
        });
        el.appendChild(this.settingsSubMenuValueEl_);
        this.settingsSubMenuEl_ = video_js_1["default"].dom.createEl('div', {
            className: 'vjs-settings-sub-menu'
        });
        return el;
    };
    /**
     * Handle click on menu item
     *
     * @method handleClick
     */
    SettingsMenuItem.prototype.handleClick = function (event) {
        var _this = this;
        this.menuToLoad = 'submenu';
        // Remove open class to ensure only the open submenu gets this class
        video_js_1["default"].dom.removeClass(this.el(), 'open');
        _super.prototype.handleClick.call(this, event);
        this.mainMenu.el().style.opacity = '0';
        // Whether to add or remove vjs-hidden class on the settingsSubMenuEl element
        if (video_js_1["default"].dom.hasClass(this.settingsSubMenuEl_, 'vjs-hidden')) {
            video_js_1["default"].dom.removeClass(this.settingsSubMenuEl_, 'vjs-hidden');
            // animation not played without timeout
            setTimeout(function () {
                _this.settingsSubMenuEl_.style.opacity = '1';
                _this.settingsSubMenuEl_.style.marginRight = '0px';
            }, 0);
            this.settingsButton.setDialogSize(this.size);
        }
        else {
            video_js_1["default"].dom.addClass(this.settingsSubMenuEl_, 'vjs-hidden');
        }
    };
    /**
     * Create back button
     *
     * @method createBackButton
     */
    SettingsMenuItem.prototype.createBackButton = function () {
        var button = this.subMenu.menu.addChild('MenuItem', {}, 0);
        button.addClass('vjs-back-button');
        button.el().innerHTML = this.player().localize(this.subMenu.controlText());
    };
    /**
     * Add/remove prefixed event listener for CSS Transition
     *
     * @method PrefixedEvent
     */
    SettingsMenuItem.prototype.PrefixedEvent = function (element, type, callback, action) {
        if (action === void 0) { action = 'addEvent'; }
        var prefix = ['webkit', 'moz', 'MS', 'o', ''];
        for (var p = 0; p < prefix.length; p++) {
            if (!prefix[p]) {
                type = type.toLowerCase();
            }
            if (action === 'addEvent') {
                element.addEventListener(prefix[p] + type, callback, false);
            }
            else if (action === 'removeEvent') {
                element.removeEventListener(prefix[p] + type, callback, false);
            }
        }
    };
    SettingsMenuItem.prototype.onTransitionEnd = function (event) {
        if (event.propertyName !== 'margin-right') {
            return;
        }
        if (this.menuToLoad === 'mainmenu') {
            // hide submenu
            video_js_1["default"].dom.addClass(this.settingsSubMenuEl_, 'vjs-hidden');
            // reset opacity to 0
            this.settingsSubMenuEl_.style.opacity = '0';
        }
    };
    SettingsMenuItem.prototype.reset = function () {
        video_js_1["default"].dom.addClass(this.settingsSubMenuEl_, 'vjs-hidden');
        this.settingsSubMenuEl_.style.opacity = '0';
        this.setMargin();
    };
    SettingsMenuItem.prototype.loadMainMenu = function () {
        var _this = this;
        var mainMenuEl = this.mainMenu.el();
        this.menuToLoad = 'mainmenu';
        this.mainMenu.show();
        mainMenuEl.style.opacity = '0';
        // back button will always take you to main menu, so set dialog sizes
        var mainMenuAny = this.mainMenu;
        this.settingsButton.setDialogSize([mainMenuAny.width, mainMenuAny.height]);
        // animation not triggered without timeout (some async stuff ?!?)
        setTimeout(function () {
            // animate margin and opacity before hiding the submenu
            // this triggers CSS Transition event
            _this.setMargin();
            mainMenuEl.style.opacity = '1';
        }, 0);
    };
    SettingsMenuItem.prototype.build = function () {
        var _this = this;
        this.subMenu.on('updateLabel', function () {
            _this.update();
        });
        this.subMenu.on('menuChanged', function () {
            _this.bindClickEvents();
            _this.setSize();
            _this.update();
        });
        this.settingsSubMenuTitleEl_.innerHTML = this.player().localize(this.subMenu.controlText());
        this.settingsSubMenuEl_.appendChild(this.subMenu.menu.el());
        this.panelChildEl.appendChild(this.settingsSubMenuEl_);
        this.update();
        this.createBackButton();
        this.setSize();
        this.bindClickEvents();
        // prefixed event listeners for CSS TransitionEnd
        this.PrefixedEvent(this.settingsSubMenuEl_, 'TransitionEnd', this.transitionEndHandler, 'addEvent');
    };
    SettingsMenuItem.prototype.update = function (event) {
        var _this = this;
        var target = null;
        var subMenu = this.subMenu.name();
        if (event && event.type === 'tap') {
            target = event.target;
        }
        else if (event) {
            target = event.currentTarget;
        }
        // Playback rate menu button doesn't get a vjs-selected class
        // or sets options_['selected'] on the selected playback rate.
        // Thus we get the submenu value based on the labelEl of playbackRateMenuButton
        if (subMenu === 'PlaybackRateMenuButton') {
            var html_1 = this.subMenu.labelEl_.innerHTML;
            setTimeout(function () { return _this.settingsSubMenuValueEl_.innerHTML = html_1; }, 250);
        }
        else {
            // Loop trough the submenu items to find the selected child
            for (var _i = 0, _a = this.subMenu.menu.children_; _i < _a.length; _i++) {
                var subMenuItem = _a[_i];
                if (!(subMenuItem instanceof component)) {
                    continue;
                }
                if (subMenuItem.hasClass('vjs-selected')) {
                    var subMenuItemUntyped = subMenuItem;
                    // Prefer to use the function
                    if (typeof subMenuItemUntyped.getLabel === 'function') {
                        this.settingsSubMenuValueEl_.innerHTML = subMenuItemUntyped.getLabel();
                        break;
                    }
                    this.settingsSubMenuValueEl_.innerHTML = subMenuItemUntyped.options_.label;
                }
            }
        }
        if (target && !target.classList.contains('vjs-back-button')) {
            this.settingsButton.hideDialog();
        }
    };
    SettingsMenuItem.prototype.bindClickEvents = function () {
        for (var _i = 0, _a = this.subMenu.menu.children(); _i < _a.length; _i++) {
            var item = _a[_i];
            if (!(item instanceof component)) {
                continue;
            }
            item.on(['tap', 'click'], this.submenuClickHandler);
        }
    };
    // save size of submenus on first init
    // if number of submenu items change dynamically more logic will be needed
    SettingsMenuItem.prototype.setSize = function () {
        this.dialog.removeClass('vjs-hidden');
        video_js_1["default"].dom.removeClass(this.settingsSubMenuEl_, 'vjs-hidden');
        this.size = this.settingsButton.getComponentSize(this.settingsSubMenuEl_);
        this.setMargin();
        this.dialog.addClass('vjs-hidden');
        video_js_1["default"].dom.addClass(this.settingsSubMenuEl_, 'vjs-hidden');
    };
    SettingsMenuItem.prototype.setMargin = function () {
        if (!this.size)
            return;
        var width = this.size[0];
        this.settingsSubMenuEl_.style.marginRight = "-" + width + "px";
    };
    /**
     * Hide the sub menu
     */
    SettingsMenuItem.prototype.hideSubMenu = function () {
        // after removing settings item this.el_ === null
        if (!this.el()) {
            return;
        }
        if (video_js_1["default"].dom.hasClass(this.el(), 'open')) {
            video_js_1["default"].dom.addClass(this.settingsSubMenuEl_, 'vjs-hidden');
            video_js_1["default"].dom.removeClass(this.el(), 'open');
        }
    };
    return SettingsMenuItem;
}(MenuItem));
exports.SettingsMenuItem = SettingsMenuItem;
SettingsMenuItem.prototype.contentElType = 'button';
video_js_1["default"].registerComponent('SettingsMenuItem', SettingsMenuItem);
