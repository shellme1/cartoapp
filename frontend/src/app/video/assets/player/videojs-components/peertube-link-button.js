"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var utils_1 = require("../utils");
var video_js_1 = require("video.js");
var Button = video_js_1["default"].getComponent('Button');
var PeerTubeLinkButton = /** @class */ (function (_super) {
    __extends(PeerTubeLinkButton, _super);
    function PeerTubeLinkButton(player, options) {
        return _super.call(this, player, options) || this;
    }
    PeerTubeLinkButton.prototype.createEl = function () {
        return this.buildElement();
    };
    PeerTubeLinkButton.prototype.updateHref = function () {
        this.el().setAttribute('href', utils_1.buildVideoLink({ startTime: this.player().currentTime() }));
    };
    PeerTubeLinkButton.prototype.handleClick = function () {
        this.player().pause();
    };
    PeerTubeLinkButton.prototype.buildElement = function () {
        var _this = this;
        var el = video_js_1["default"].dom.createEl('a', {
            href: utils_1.buildVideoLink(),
            innerHTML: 'PeerTube',
            title: this.player().localize('Video page (new window)'),
            className: 'vjs-peertube-link',
            target: '_blank'
        });
        el.addEventListener('mouseenter', function () { return _this.updateHref(); });
        return el;
    };
    return PeerTubeLinkButton;
}(Button));
video_js_1["default"].registerComponent('PeerTubeLinkButton', PeerTubeLinkButton);
