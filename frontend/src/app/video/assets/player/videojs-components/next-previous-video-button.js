"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var video_js_1 = require("video.js");
var Button = video_js_1["default"].getComponent('Button');
var NextPreviousVideoButton = /** @class */ (function (_super) {
    __extends(NextPreviousVideoButton, _super);
    function NextPreviousVideoButton(player, options) {
        var _this = _super.call(this, player, options) || this;
        _this.nextPreviousVideoButtonOptions = options;
        _this.update();
        return _this;
    }
    NextPreviousVideoButton.prototype.createEl = function () {
        var type = this.options_.type;
        var button = video_js_1["default"].dom.createEl('button', {
            className: 'vjs-' + type + '-video'
        });
        var nextIcon = video_js_1["default"].dom.createEl('span', {
            className: 'icon icon-' + type
        });
        button.appendChild(nextIcon);
        if (type === 'next') {
            button.title = this.player_.localize('Next video');
        }
        else {
            button.title = this.player_.localize('Previous video');
        }
        return button;
    };
    NextPreviousVideoButton.prototype.handleClick = function () {
        this.nextPreviousVideoButtonOptions.handler();
    };
    NextPreviousVideoButton.prototype.update = function () {
        var disabled = this.nextPreviousVideoButtonOptions.isDisabled();
        if (disabled)
            this.addClass('vjs-disabled');
        else
            this.removeClass('vjs-disabled');
    };
    return NextPreviousVideoButton;
}(Button));
video_js_1["default"].registerComponent('NextVideoButton', NextPreviousVideoButton);
video_js_1["default"].registerComponent('PreviousVideoButton', NextPreviousVideoButton);
