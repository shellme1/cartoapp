"use strict";
// Thanks https://github.com/streamroot/videojs-hlsjs-plugin
// We duplicated this plugin to choose the hls.js version we want, because streamroot only provide a bundled file
exports.__esModule = true;
exports.registerConfigPlugin = exports.registerSourceHandler = exports.Html5Hlsjs = void 0;
var Hlsjs = require("hls.js/dist/hls.light.js");
var registerSourceHandler = function (vjs) {
    if (!Hlsjs.isSupported()) {
        console.warn('Hls.js is not supported in this browser!');
        return;
    }
    var html5 = vjs.getTech('Html5');
    if (!html5) {
        console.error('Not supported version if video.js');
        return;
    }
    // FIXME: typings
    html5.registerSourceHandler({
        canHandleSource: function (source) {
            var hlsTypeRE = /^application\/x-mpegURL|application\/vnd\.apple\.mpegurl$/i;
            var hlsExtRE = /\.m3u8/i;
            if (hlsTypeRE.test(source.type))
                return 'probably';
            if (hlsExtRE.test(source.src))
                return 'maybe';
            return '';
        },
        handleSource: function (source, tech) {
            if (tech.hlsProvider) {
                tech.hlsProvider.dispose();
            }
            tech.hlsProvider = new Html5Hlsjs(vjs, source, tech);
            return tech.hlsProvider;
        }
    }, 0);
    // FIXME: typings
    vjs.Html5Hlsjs = Html5Hlsjs;
};
exports.registerSourceHandler = registerSourceHandler;
function hlsjsConfigHandler(options) {
    var player = this;
    if (!options)
        return;
    if (!player.srOptions_) {
        player.srOptions_ = {};
    }
    if (!player.srOptions_.hlsjsConfig) {
        player.srOptions_.hlsjsConfig = options.hlsjsConfig;
    }
    if (!player.srOptions_.captionConfig) {
        player.srOptions_.captionConfig = options.captionConfig;
    }
    if (options.levelLabelHandler && !player.srOptions_.levelLabelHandler) {
        player.srOptions_.levelLabelHandler = options.levelLabelHandler;
    }
}
var registerConfigPlugin = function (vjs) {
    // Used in Brightcove since we don't pass options directly there
    var registerVjsPlugin = vjs.registerPlugin || vjs.plugin;
    registerVjsPlugin('hlsjs', hlsjsConfigHandler);
};
exports.registerConfigPlugin = registerConfigPlugin;
var Html5Hlsjs = /** @class */ (function () {
    function Html5Hlsjs(vjs, source, tech) {
        var _this = this;
        this.errorCounts = {};
        this.hlsjsConfig = null;
        this._duration = null;
        this.metadata = null;
        this.isLive = null;
        this.dvrDuration = null;
        this.edgeMargin = null;
        this.handlers = {
            play: null,
            addtrack: null,
            playing: null,
            textTracksChange: null,
            audioTracksChange: null
        };
        this.uiTextTrackHandled = false;
        this.vjs = vjs;
        this.source = source;
        this.tech = tech;
        this.tech.name_ = 'Hlsjs';
        this.videoElement = tech.el();
        this.player = vjs(tech.options_.playerId);
        this.videoElement.addEventListener('error', function (event) {
            var errorTxt;
            var mediaError = event.currentTarget.error;
            switch (mediaError.code) {
                case mediaError.MEDIA_ERR_ABORTED:
                    errorTxt = 'You aborted the video playback';
                    break;
                case mediaError.MEDIA_ERR_DECODE:
                    errorTxt = 'The video playback was aborted due to a corruption problem or because the video used features your browser did not support';
                    _this._handleMediaError(mediaError);
                    break;
                case mediaError.MEDIA_ERR_NETWORK:
                    errorTxt = 'A network error caused the video download to fail part-way';
                    break;
                case mediaError.MEDIA_ERR_SRC_NOT_SUPPORTED:
                    errorTxt = 'The video could not be loaded, either because the server or network failed or because the format is not supported';
                    break;
                default:
                    errorTxt = mediaError.message;
            }
            console.error('MEDIA_ERROR: ', errorTxt);
        });
        this.initialize();
    }
    Html5Hlsjs.prototype.duration = function () {
        return this._duration || this.videoElement.duration || 0;
    };
    Html5Hlsjs.prototype.seekable = function () {
        if (this.hls.media) {
            if (!this.isLive) {
                return this.vjs.createTimeRanges(0, this.hls.media.duration);
            }
            // Video.js doesn't seem to like floating point timeranges
            var startTime = Math.round(this.hls.media.duration - this.dvrDuration);
            var endTime = Math.round(this.hls.media.duration - this.edgeMargin);
            return this.vjs.createTimeRanges(startTime, endTime);
        }
        return this.vjs.createTimeRanges();
    };
    // See comment for `initialize` method.
    Html5Hlsjs.prototype.dispose = function () {
        this.videoElement.removeEventListener('play', this.handlers.play);
        this.videoElement.textTracks.removeEventListener('addtrack', this.handlers.addtrack);
        this.videoElement.removeEventListener('playing', this.handlers.playing);
        this.player.textTracks().removeEventListener('change', this.handlers.textTracksChange);
        this.uiTextTrackHandled = false;
        this.hls.destroy();
    };
    Html5Hlsjs.addHook = function (type, callback) {
        Html5Hlsjs.hooks[type] = this.hooks[type] || [];
        Html5Hlsjs.hooks[type].push(callback);
    };
    Html5Hlsjs.removeHook = function (type, callback) {
        if (Html5Hlsjs.hooks[type] === undefined)
            return false;
        var index = Html5Hlsjs.hooks[type].indexOf(callback);
        if (index === -1)
            return false;
        Html5Hlsjs.hooks[type].splice(index, 1);
        return true;
    };
    Html5Hlsjs.prototype._executeHooksFor = function (type) {
        if (Html5Hlsjs.hooks[type] === undefined) {
            return;
        }
        // ES3 and IE < 9
        for (var i = 0; i < Html5Hlsjs.hooks[type].length; i++) {
            Html5Hlsjs.hooks[type][i](this.player, this.hls);
        }
    };
    Html5Hlsjs.prototype._handleMediaError = function (error) {
        if (this.errorCounts[Hlsjs.ErrorTypes.MEDIA_ERROR] === 1) {
            console.info('trying to recover media error');
            this.hls.recoverMediaError();
            return;
        }
        if (this.errorCounts[Hlsjs.ErrorTypes.MEDIA_ERROR] === 2) {
            console.info('2nd try to recover media error (by swapping audio codec');
            this.hls.swapAudioCodec();
            this.hls.recoverMediaError();
            return;
        }
        if (this.errorCounts[Hlsjs.ErrorTypes.MEDIA_ERROR] > 2) {
            console.info('bubbling media error up to VIDEOJS');
            this.hls.destroy();
            this.tech.error = function () { return error; };
            this.tech.trigger('error');
            return;
        }
    };
    Html5Hlsjs.prototype._onError = function (_event, data) {
        var error = {
            message: "HLS.js error: " + data.type + " - fatal: " + data.fatal + " - " + data.details
        };
        console.error(error.message);
        // increment/set error count
        if (this.errorCounts[data.type])
            this.errorCounts[data.type] += 1;
        else
            this.errorCounts[data.type] = 1;
        // Implement simple error handling based on hls.js documentation
        // https://github.com/dailymotion/hls.js/blob/master/API.md#fifth-step-error-handling
        if (data.fatal) {
            switch (data.type) {
                case Hlsjs.ErrorTypes.NETWORK_ERROR:
                    console.info('bubbling network error up to VIDEOJS');
                    error.code = 2;
                    this.tech.error = function () { return error; };
                    this.tech.trigger('error');
                    break;
                case Hlsjs.ErrorTypes.MEDIA_ERROR:
                    error.code = 3;
                    this._handleMediaError(error);
                    break;
                default:
                    // cannot recover
                    this.hls.destroy();
                    console.info('bubbling error up to VIDEOJS');
                    this.tech.error = function () { return error; };
                    this.tech.trigger('error');
                    break;
            }
        }
    };
    Html5Hlsjs.prototype.switchQuality = function (qualityId) {
        this.hls.nextLevel = qualityId;
    };
    Html5Hlsjs.prototype._levelLabel = function (level) {
        if (this.player.srOptions_.levelLabelHandler) {
            return this.player.srOptions_.levelLabelHandler(level);
        }
        if (level.height)
            return level.height + 'p';
        if (level.width)
            return Math.round(level.width * 9 / 16) + 'p';
        if (level.bitrate)
            return (level.bitrate / 1000) + 'kbps';
        return 0;
    };
    Html5Hlsjs.prototype._relayQualityChange = function (qualityLevels) {
        // Determine if it is "Auto" (all tracks enabled)
        var isAuto = true;
        for (var i = 0; i < qualityLevels.length; i++) {
            if (!qualityLevels[i]._enabled) {
                isAuto = false;
                break;
            }
        }
        // Interact with ME
        if (isAuto) {
            this.hls.currentLevel = -1;
            return;
        }
        // Find ID of highest enabled track
        var selectedTrack;
        for (selectedTrack = qualityLevels.length - 1; selectedTrack >= 0; selectedTrack--) {
            if (qualityLevels[selectedTrack]._enabled) {
                break;
            }
        }
        this.hls.currentLevel = selectedTrack;
    };
    Html5Hlsjs.prototype._handleQualityLevels = function () {
        if (!this.metadata)
            return;
        var qualityLevels = this.player.qualityLevels && this.player.qualityLevels();
        if (!qualityLevels)
            return;
        var _loop_1 = function (i) {
            var details = this_1.metadata.levels[i];
            var representation = {
                id: i,
                width: details.width,
                height: details.height,
                bandwidth: details.bitrate,
                bitrate: details.bitrate,
                _enabled: true
            };
            var self_1 = this_1;
            representation.enabled = function (level, toggle) {
                // Brightcove switcher works TextTracks-style (enable tracks that it wants to ABR on)
                if (typeof toggle === 'boolean') {
                    this[level]._enabled = toggle;
                    self_1._relayQualityChange(this);
                }
                return this[level]._enabled;
            };
            qualityLevels.addQualityLevel(representation);
        };
        var this_1 = this;
        for (var i = 0; i < this.metadata.levels.length; i++) {
            _loop_1(i);
        }
    };
    Html5Hlsjs.prototype._notifyVideoQualities = function () {
        var _this = this;
        if (!this.metadata)
            return;
        var cleanTracklist = [];
        if (this.metadata.levels.length > 1) {
            var autoLevel = {
                id: -1,
                label: 'auto',
                selected: this.hls.manualLevel === -1
            };
            cleanTracklist.push(autoLevel);
        }
        this.metadata.levels.forEach(function (level, index) {
            // Don't write in level (shared reference with Hls.js)
            var quality = {
                id: index,
                selected: index === _this.hls.manualLevel,
                label: _this._levelLabel(level)
            };
            cleanTracklist.push(quality);
        });
        var payload = {
            qualityData: { video: cleanTracklist },
            qualitySwitchCallback: this.switchQuality.bind(this)
        };
        this.tech.trigger('loadedqualitydata', payload);
        // Self-de-register so we don't raise the payload multiple times
        this.videoElement.removeEventListener('playing', this.handlers.playing);
    };
    Html5Hlsjs.prototype._updateSelectedAudioTrack = function () {
        var playerAudioTracks = this.tech.audioTracks();
        for (var j = 0; j < playerAudioTracks.length; j++) {
            // FIXME: typings
            if (playerAudioTracks[j].enabled) {
                this.hls.audioTrack = j;
                break;
            }
        }
    };
    Html5Hlsjs.prototype._onAudioTracks = function () {
        var hlsAudioTracks = this.hls.audioTracks;
        var playerAudioTracks = this.tech.audioTracks();
        if (hlsAudioTracks.length > 1 && playerAudioTracks.length === 0) {
            // Add Hls.js audio tracks if not added yet
            for (var i = 0; i < hlsAudioTracks.length; i++) {
                playerAudioTracks.addTrack(new this.vjs.AudioTrack({
                    id: i.toString(),
                    kind: 'alternative',
                    label: hlsAudioTracks[i].name || hlsAudioTracks[i].lang,
                    language: hlsAudioTracks[i].lang,
                    enabled: i === this.hls.audioTrack
                }));
            }
            // Handle audio track change event
            this.handlers.audioTracksChange = this._updateSelectedAudioTrack.bind(this);
            playerAudioTracks.addEventListener('change', this.handlers.audioTracksChange);
        }
    };
    Html5Hlsjs.prototype._getTextTrackLabel = function (textTrack) {
        // Label here is readable label and is optional (used in the UI so if it is there it should be different)
        return textTrack.label ? textTrack.label : textTrack.language;
    };
    Html5Hlsjs.prototype._isSameTextTrack = function (track1, track2) {
        return this._getTextTrackLabel(track1) === this._getTextTrackLabel(track2)
            && track1.kind === track2.kind;
    };
    Html5Hlsjs.prototype._updateSelectedTextTrack = function () {
        var playerTextTracks = this.player.textTracks();
        var activeTrack = null;
        for (var j = 0; j < playerTextTracks.length; j++) {
            if (playerTextTracks[j].mode === 'showing') {
                activeTrack = playerTextTracks[j];
                break;
            }
        }
        var hlsjsTracks = this.videoElement.textTracks;
        for (var k = 0; k < hlsjsTracks.length; k++) {
            if (hlsjsTracks[k].kind === 'subtitles' || hlsjsTracks[k].kind === 'captions') {
                hlsjsTracks[k].mode = activeTrack && this._isSameTextTrack(hlsjsTracks[k], activeTrack)
                    ? 'showing'
                    : 'disabled';
            }
        }
    };
    Html5Hlsjs.prototype._startLoad = function () {
        this.hls.startLoad(-1);
        this.videoElement.removeEventListener('play', this.handlers.play);
    };
    Html5Hlsjs.prototype._oneLevelObjClone = function (obj) {
        var result = {};
        var objKeys = Object.keys(obj);
        for (var i = 0; i < objKeys.length; i++) {
            result[objKeys[i]] = obj[objKeys[i]];
        }
        return result;
    };
    Html5Hlsjs.prototype._filterDisplayableTextTracks = function (textTracks) {
        var displayableTracks = [];
        // Filter out tracks that is displayable (captions or subtitles)
        for (var idx = 0; idx < textTracks.length; idx++) {
            if (textTracks[idx].kind === 'subtitles' || textTracks[idx].kind === 'captions') {
                displayableTracks.push(textTracks[idx]);
            }
        }
        return displayableTracks;
    };
    Html5Hlsjs.prototype._updateTextTrackList = function () {
        var displayableTracks = this._filterDisplayableTextTracks(this.videoElement.textTracks);
        var playerTextTracks = this.player.textTracks();
        // Add stubs to make the caption switcher shows up
        // Adding the Hls.js text track in will make us have double captions
        for (var idx = 0; idx < displayableTracks.length; idx++) {
            var isAdded = false;
            for (var jdx = 0; jdx < playerTextTracks.length; jdx++) {
                if (this._isSameTextTrack(displayableTracks[idx], playerTextTracks[jdx])) {
                    isAdded = true;
                    break;
                }
            }
            if (!isAdded) {
                var hlsjsTextTrack = displayableTracks[idx];
                this.player.addRemoteTextTrack({
                    kind: hlsjsTextTrack.kind,
                    label: this._getTextTrackLabel(hlsjsTextTrack),
                    language: hlsjsTextTrack.language,
                    srclang: hlsjsTextTrack.language
                }, false);
            }
        }
        // Handle UI switching
        this._updateSelectedTextTrack();
        if (!this.uiTextTrackHandled) {
            this.handlers.textTracksChange = this._updateSelectedTextTrack.bind(this);
            playerTextTracks.addEventListener('change', this.handlers.textTracksChange);
            this.uiTextTrackHandled = true;
        }
    };
    Html5Hlsjs.prototype._onMetaData = function (_event, data) {
        // This could arrive before 'loadedqualitydata' handlers is registered, remember it so we can raise it later
        this.metadata = data;
        this._handleQualityLevels();
    };
    Html5Hlsjs.prototype._createCueHandler = function (captionConfig) {
        return {
            newCue: function (track, startTime, endTime, captionScreen) {
                var row;
                var cue;
                var text;
                var VTTCue = window.VTTCue || window.TextTrackCue;
                for (var r = 0; r < captionScreen.rows.length; r++) {
                    row = captionScreen.rows[r];
                    text = '';
                    if (!row.isEmpty()) {
                        for (var c = 0; c < row.chars.length; c++) {
                            text += row.chars[c].ucharj;
                        }
                        cue = new VTTCue(startTime, endTime, text.trim());
                        // typeof null === 'object'
                        if (captionConfig != null && typeof captionConfig === 'object') {
                            // Copy client overridden property into the cue object
                            var configKeys = Object.keys(captionConfig);
                            for (var k = 0; k < configKeys.length; k++) {
                                cue[configKeys[k]] = captionConfig[configKeys[k]];
                            }
                        }
                        track.addCue(cue);
                        if (endTime === startTime)
                            track.addCue(new VTTCue(endTime + 5, ''));
                    }
                }
            }
        };
    };
    Html5Hlsjs.prototype._initHlsjs = function () {
        var _this = this;
        var techOptions = this.tech.options_;
        var srOptions_ = this.player.srOptions_;
        var hlsjsConfigRef = srOptions_ && srOptions_.hlsjsConfig || techOptions.hlsjsConfig;
        // Hls.js will write to the reference thus change the object for later streams
        this.hlsjsConfig = hlsjsConfigRef ? this._oneLevelObjClone(hlsjsConfigRef) : {};
        if (['', 'auto'].includes(this.videoElement.preload) && !this.videoElement.autoplay && this.hlsjsConfig.autoStartLoad === undefined) {
            this.hlsjsConfig.autoStartLoad = false;
        }
        var captionConfig = srOptions_ && srOptions_.captionConfig || techOptions.captionConfig;
        if (captionConfig) {
            this.hlsjsConfig.cueHandler = this._createCueHandler(captionConfig);
        }
        // If the user explicitly sets autoStartLoad to false, we're not going to enter the if block above
        // That's why we have a separate if block here to set the 'play' listener
        if (this.hlsjsConfig.autoStartLoad === false) {
            this.handlers.play = this._startLoad.bind(this);
            this.videoElement.addEventListener('play', this.handlers.play);
        }
        // _notifyVideoQualities sometimes runs before the quality picker event handler is registered -> no video switcher
        this.handlers.playing = this._notifyVideoQualities.bind(this);
        this.videoElement.addEventListener('playing', this.handlers.playing);
        this.hls = new Hlsjs(this.hlsjsConfig);
        this._executeHooksFor('beforeinitialize');
        this.hls.on(Hlsjs.Events.ERROR, function (event, data) { return _this._onError(event, data); });
        this.hls.on(Hlsjs.Events.AUDIO_TRACKS_UPDATED, function () { return _this._onAudioTracks(); });
        this.hls.on(Hlsjs.Events.MANIFEST_PARSED, function (event, data) { return _this._onMetaData(event, data); }); // FIXME: typings
        this.hls.on(Hlsjs.Events.LEVEL_LOADED, function (event, data) {
            // The DVR plugin will auto seek to "live edge" on start up
            if (_this.hlsjsConfig.liveSyncDuration) {
                _this.edgeMargin = _this.hlsjsConfig.liveSyncDuration;
            }
            else if (_this.hlsjsConfig.liveSyncDurationCount) {
                _this.edgeMargin = _this.hlsjsConfig.liveSyncDurationCount * data.details.targetduration;
            }
            _this.isLive = data.details.live;
            _this.dvrDuration = data.details.totalduration;
            _this._duration = _this.isLive ? Infinity : data.details.totalduration;
        });
        this.hls.once(Hlsjs.Events.FRAG_LOADED, function () {
            // Emit custom 'loadedmetadata' event for parity with `videojs-contrib-hls`
            // Ref: https://github.com/videojs/videojs-contrib-hls#loadedmetadata
            _this.tech.trigger('loadedmetadata');
        });
        this.hls.attachMedia(this.videoElement);
        this.handlers.addtrack = this._updateTextTrackList.bind(this);
        this.videoElement.textTracks.addEventListener('addtrack', this.handlers.addtrack);
        this.hls.loadSource(this.source.src);
    };
    Html5Hlsjs.prototype.initialize = function () {
        this._initHlsjs();
    };
    Html5Hlsjs.hooks = {};
    return Html5Hlsjs;
}());
exports.Html5Hlsjs = Html5Hlsjs;
