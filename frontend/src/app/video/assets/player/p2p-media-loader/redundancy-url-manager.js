"use strict";
exports.__esModule = true;
exports.RedundancyUrlManager = void 0;
var path_1 = require("path");
var RedundancyUrlManager = /** @class */ (function () {
    function RedundancyUrlManager(baseUrls) {
        if (baseUrls === void 0) { baseUrls = []; }
        this.baseUrls = baseUrls;
        // empty
    }
    RedundancyUrlManager.prototype.removeBySegmentUrl = function (segmentUrl) {
        console.log('Removing redundancy of segment URL %s.', segmentUrl);
        var baseUrl = path_1.dirname(segmentUrl);
        this.baseUrls = this.baseUrls.filter(function (u) { return u !== baseUrl && u !== baseUrl + '/'; });
    };
    RedundancyUrlManager.prototype.buildUrl = function (url) {
        var max = this.baseUrls.length + 1;
        var i = this.getRandomInt(max);
        if (i === max - 1)
            return url;
        var newBaseUrl = this.baseUrls[i];
        var slashPart = newBaseUrl.endsWith('/') ? '' : '/';
        return newBaseUrl + slashPart + path_1.basename(url);
    };
    RedundancyUrlManager.prototype.countBaseUrls = function () {
        return this.baseUrls.length;
    };
    RedundancyUrlManager.prototype.getRandomInt = function (max) {
        return Math.floor(Math.random() * Math.floor(max));
    };
    return RedundancyUrlManager;
}());
exports.RedundancyUrlManager = RedundancyUrlManager;
