"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.P2pMediaLoaderPlugin = void 0;
var video_js_1 = require("video.js");
var p2p_media_loader_hlsjs_1 = require("p2p-media-loader-hlsjs");
var p2p_media_loader_core_1 = require("p2p-media-loader-core");
var utils_1 = require("../utils");
var hls_plugin_1 = require("./hls-plugin");
var Hlsjs = require("hls.js/dist/hls.light.js");
hls_plugin_1.registerConfigPlugin(video_js_1["default"]);
hls_plugin_1.registerSourceHandler(video_js_1["default"]);
var Plugin = video_js_1["default"].getPlugin('plugin');
var P2pMediaLoaderPlugin = /** @class */ (function (_super) {
    __extends(P2pMediaLoaderPlugin, _super);
    function P2pMediaLoaderPlugin(player, options) {
        var _this = _super.call(this, player) || this;
        _this.CONSTANTS = {
            INFO_SCHEDULER: 1000 // Don't change this
        };
        _this.statsP2PBytes = {
            pendingDownload: [],
            pendingUpload: [],
            numPeers: 0,
            totalDownload: 0,
            totalUpload: 0
        };
        _this.statsHTTPBytes = {
            pendingDownload: [],
            pendingUpload: [],
            totalDownload: 0,
            totalUpload: 0
        };
        _this.options = options;
        // FIXME: typings https://github.com/Microsoft/TypeScript/issues/14080
        if (!video_js_1["default"].Html5Hlsjs) {
            console.warn('HLS.js does not seem to be supported. Try to fallback to built in HLS.');
            if (!player.canPlayType('application/vnd.apple.mpegurl')) {
                var message_1 = 'Cannot fallback to built-in HLS';
                console.warn(message_1);
                player.ready(function () { return player.trigger('error', new Error(message_1)); });
                return _this;
            }
        }
        else {
            // FIXME: typings https://github.com/Microsoft/TypeScript/issues/14080
            video_js_1["default"].Html5Hlsjs.addHook('beforeinitialize', function (videojsPlayer, hlsjs) {
                _this.hlsjs = hlsjs;
            });
            p2p_media_loader_hlsjs_1.initVideoJsContribHlsJsPlayer(player);
        }
        _this.startTime = utils_1.timeToInt(options.startTime);
        player.src({
            type: options.type,
            src: options.src
        });
        player.ready(function () {
            _this.initializeCore();
            if (video_js_1["default"].Html5Hlsjs) {
                _this.initializePlugin();
            }
        });
        return _this;
    }
    P2pMediaLoaderPlugin.prototype.dispose = function () {
        if (this.hlsjs)
            this.hlsjs.destroy();
        if (this.p2pEngine)
            this.p2pEngine.destroy();
        clearInterval(this.networkInfoInterval);
    };
    P2pMediaLoaderPlugin.prototype.getHLSJS = function () {
        return this.hlsjs;
    };
    P2pMediaLoaderPlugin.prototype.initializeCore = function () {
        var _this = this;
        this.player.one('play', function () {
            _this.player.addClass('vjs-has-big-play-button-clicked');
        });
        this.player.one('canplay', function () {
            if (_this.startTime) {
                _this.player.currentTime(_this.startTime);
            }
        });
    };
    P2pMediaLoaderPlugin.prototype.initializePlugin = function () {
        var _this = this;
        p2p_media_loader_hlsjs_1.initHlsJsPlayer(this.hlsjs);
        // FIXME: typings
        var options = this.player.tech(true).options_;
        this.p2pEngine = options.hlsjsConfig.loader.getEngine();
        this.hlsjs.on(Hlsjs.Events.LEVEL_SWITCHING, function (_, data) {
            _this.trigger('resolutionChange', { auto: _this.hlsjs.autoLevelEnabled, resolutionId: data.height });
        });
        this.p2pEngine.on(p2p_media_loader_core_1.Events.SegmentError, function (segment, err) {
            console.error('Segment error.', segment, err);
            _this.options.redundancyUrlManager.removeBySegmentUrl(segment.requestUrl);
        });
        this.statsP2PBytes.numPeers = 1 + this.options.redundancyUrlManager.countBaseUrls();
        this.runStats();
    };
    P2pMediaLoaderPlugin.prototype.runStats = function () {
        var _this = this;
        this.p2pEngine.on(p2p_media_loader_core_1.Events.PieceBytesDownloaded, function (method, size) {
            var elem = method === 'p2p' ? _this.statsP2PBytes : _this.statsHTTPBytes;
            elem.pendingDownload.push(size);
            elem.totalDownload += size;
        });
        this.p2pEngine.on(p2p_media_loader_core_1.Events.PieceBytesUploaded, function (method, size) {
            var elem = method === 'p2p' ? _this.statsP2PBytes : _this.statsHTTPBytes;
            elem.pendingUpload.push(size);
            elem.totalUpload += size;
        });
        this.p2pEngine.on(p2p_media_loader_core_1.Events.PeerConnect, function () { return _this.statsP2PBytes.numPeers++; });
        this.p2pEngine.on(p2p_media_loader_core_1.Events.PeerClose, function () { return _this.statsP2PBytes.numPeers--; });
        this.networkInfoInterval = setInterval(function () {
            var p2pDownloadSpeed = _this.arraySum(_this.statsP2PBytes.pendingDownload);
            var p2pUploadSpeed = _this.arraySum(_this.statsP2PBytes.pendingUpload);
            var httpDownloadSpeed = _this.arraySum(_this.statsHTTPBytes.pendingDownload);
            var httpUploadSpeed = _this.arraySum(_this.statsHTTPBytes.pendingUpload);
            _this.statsP2PBytes.pendingDownload = [];
            _this.statsP2PBytes.pendingUpload = [];
            _this.statsHTTPBytes.pendingDownload = [];
            _this.statsHTTPBytes.pendingUpload = [];
            return _this.player.trigger('p2pInfo', {
                source: 'p2p-media-loader',
                http: {
                    downloadSpeed: httpDownloadSpeed,
                    uploadSpeed: httpUploadSpeed,
                    downloaded: _this.statsHTTPBytes.totalDownload,
                    uploaded: _this.statsHTTPBytes.totalUpload
                },
                p2p: {
                    downloadSpeed: p2pDownloadSpeed,
                    uploadSpeed: p2pUploadSpeed,
                    numPeers: _this.statsP2PBytes.numPeers,
                    downloaded: _this.statsP2PBytes.totalDownload,
                    uploaded: _this.statsP2PBytes.totalUpload
                }
            });
        }, this.CONSTANTS.INFO_SCHEDULER);
    };
    P2pMediaLoaderPlugin.prototype.arraySum = function (data) {
        return data.reduce(function (a, b) { return a + b; }, 0);
    };
    return P2pMediaLoaderPlugin;
}(Plugin));
exports.P2pMediaLoaderPlugin = P2pMediaLoaderPlugin;
video_js_1["default"].registerPlugin('p2pMediaLoader', P2pMediaLoaderPlugin);
