"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.segmentValidatorFactory = void 0;
var utils_1 = require("@root-helpers/utils");
var path_1 = require("path");
var maxRetries = 3;
function segmentValidatorFactory(segmentsSha256Url, isLive) {
    var segmentsJSON = fetchSha256Segments(segmentsSha256Url);
    var regex = /bytes=(\d+)-(\d+)/;
    return function segmentValidator(segment, _method, _peerId, retry) {
        if (retry === void 0) { retry = 1; }
        return __awaiter(this, void 0, void 0, function () {
            var filename, segmentValue, hashShouldBe, range, captured, calculatedSha;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!isLive) return [3 /*break*/, 2];
                        return [4 /*yield*/, utils_1.wait(1000)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        filename = path_1.basename(segment.url);
                        return [4 /*yield*/, segmentsJSON];
                    case 3:
                        segmentValue = (_a.sent())[filename];
                        if (!segmentValue && retry > maxRetries) {
                            throw new Error("Unknown segment name " + filename + " in segment validator");
                        }
                        if (!!segmentValue) return [3 /*break*/, 6];
                        console.log('Refetching sha segments for %s.', filename);
                        return [4 /*yield*/, utils_1.wait(1000)];
                    case 4:
                        _a.sent();
                        segmentsJSON = fetchSha256Segments(segmentsSha256Url);
                        return [4 /*yield*/, segmentValidator(segment, _method, _peerId, retry + 1)];
                    case 5:
                        _a.sent();
                        return [2 /*return*/];
                    case 6:
                        range = '';
                        if (typeof segmentValue === 'string') {
                            hashShouldBe = segmentValue;
                        }
                        else {
                            captured = regex.exec(segment.range);
                            range = captured[1] + '-' + captured[2];
                            hashShouldBe = segmentValue[range];
                        }
                        if (hashShouldBe === undefined) {
                            throw new Error("Unknown segment name " + filename + "/" + range + " in segment validator");
                        }
                        return [4 /*yield*/, sha256Hex(segment.data)];
                    case 7:
                        calculatedSha = _a.sent();
                        if (calculatedSha !== hashShouldBe) {
                            throw new Error("Hashes does not correspond for segment " + filename + "/" + range +
                                ("(expected: " + hashShouldBe + " instead of " + calculatedSha + ")"));
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
}
exports.segmentValidatorFactory = segmentValidatorFactory;
// ---------------------------------------------------------------------------
function fetchSha256Segments(url) {
    return fetch(url)
        .then(function (res) { return res.json(); })["catch"](function (err) {
        console.error('Cannot get sha256 segments', err);
        return {};
    });
}
function sha256Hex(data) {
    return __awaiter(this, void 0, void 0, function () {
        var shaModule;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!data)
                        return [2 /*return*/, undefined];
                    if (window.crypto.subtle) {
                        return [2 /*return*/, window.crypto.subtle.digest('SHA-256', data)
                                .then(function (data) { return bufferToHex(data); })];
                    }
                    return [4 /*yield*/, Promise.resolve().then(function () { return require('sha.js'); })];
                case 1:
                    shaModule = _a.sent();
                    return [2 /*return*/, new shaModule.sha256().update(Buffer.from(data)).digest('hex')];
            }
        });
    });
}
// Thanks: https://stackoverflow.com/a/53307879
function bufferToHex(buffer) {
    if (!buffer)
        return '';
    var s = '';
    var h = '0123456789abcdef';
    var o = new Uint8Array(buffer);
    o.forEach(function (v) { return s += h[v >> 4] + h[v & 15]; });
    return s;
}
