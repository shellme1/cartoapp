"use strict";
exports.__esModule = true;
exports.segmentUrlBuilderFactory = void 0;
function segmentUrlBuilderFactory(redundancyUrlManager) {
    return function segmentBuilder(segment) {
        return redundancyUrlManager.buildUrl(segment.url);
    };
}
exports.segmentUrlBuilderFactory = segmentUrlBuilderFactory;
