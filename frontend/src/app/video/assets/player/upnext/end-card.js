"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var video_js_1 = require("video.js");
function getMainTemplate(options) {
    return "\n    <div class=\"vjs-upnext-top\">\n      <span class=\"vjs-upnext-headtext\">" + options.headText + "</span>\n      <div class=\"vjs-upnext-title\"></div>\n    </div>\n    <div class=\"vjs-upnext-autoplay-icon\">\n      <svg height=\"100%\" version=\"1.1\" viewbox=\"0 0 98 98\" width=\"100%\">\n        <circle class=\"vjs-upnext-svg-autoplay-circle\" cx=\"49\" cy=\"49\" fill=\"#000\" fill-opacity=\"0.8\" r=\"48\"></circle>\n        <circle class=\"vjs-upnext-svg-autoplay-ring\" cx=\"-49\" cy=\"49\" fill-opacity=\"0\" r=\"46.5\" stroke=\"#FFFFFF\" stroke-width=\"4\" transform=\"rotate(-90)\"></circle>\n        <polygon class=\"vjs-upnext-svg-autoplay-triangle\" fill=\"#fff\" points=\"32,27 72,49 32,71\"></polygon></svg>\n    </div>\n    <span class=\"vjs-upnext-bottom\">\n      <span class=\"vjs-upnext-cancel\">\n        <button class=\"vjs-upnext-cancel-button\" tabindex=\"0\" aria-label=\"Cancel autoplay\">" + options.cancelText + "</button>\n      </span>\n      <span class=\"vjs-upnext-suspended\">" + options.suspendedText + "</span>\n    </span>\n  ";
}
var Component = video_js_1["default"].getComponent('Component');
var EndCard = /** @class */ (function (_super) {
    __extends(EndCard, _super);
    function EndCard(player, options) {
        var _this = _super.call(this, player, options) || this;
        _this.dashOffsetTotal = 586;
        _this.dashOffsetStart = 293;
        _this.interval = 50;
        _this.upNextEvents = new video_js_1["default"].EventTarget();
        _this.ticks = 0;
        _this.totalTicks = _this.options_.timeout / _this.interval;
        player.on('ended', function (_) {
            if (!_this.options_.condition())
                return;
            player.addClass('vjs-upnext--showing');
            _this.showCard(function (canceled) {
                player.removeClass('vjs-upnext--showing');
                _this.container.style.display = 'none';
                if (!canceled) {
                    _this.options_.next();
                }
            });
        });
        player.on('playing', function () {
            _this.upNextEvents.trigger('playing');
        });
        return _this;
    }
    EndCard.prototype.createEl = function () {
        var _this = this;
        var container = _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-upnext-content',
            innerHTML: getMainTemplate(this.options_)
        });
        this.container = container;
        container.style.display = 'none';
        this.autoplayRing = container.getElementsByClassName('vjs-upnext-svg-autoplay-ring')[0];
        this.title = container.getElementsByClassName('vjs-upnext-title')[0];
        this.cancelButton = container.getElementsByClassName('vjs-upnext-cancel-button')[0];
        this.suspendedMessage = container.getElementsByClassName('vjs-upnext-suspended')[0];
        this.nextButton = container.getElementsByClassName('vjs-upnext-autoplay-icon')[0];
        this.cancelButton.onclick = function () {
            _this.upNextEvents.trigger('cancel');
        };
        this.nextButton.onclick = function () {
            _this.upNextEvents.trigger('next');
        };
        return container;
    };
    EndCard.prototype.showCard = function (cb) {
        var _this = this;
        var timeout;
        this.autoplayRing.setAttribute('stroke-dasharray', '' + this.dashOffsetStart);
        this.autoplayRing.setAttribute('stroke-dashoffset', '' + -this.dashOffsetStart);
        this.title.innerHTML = this.options_.getTitle();
        this.upNextEvents.one('cancel', function () {
            clearTimeout(timeout);
            cb(true);
        });
        this.upNextEvents.one('playing', function () {
            clearTimeout(timeout);
            cb(true);
        });
        this.upNextEvents.one('next', function () {
            clearTimeout(timeout);
            cb(false);
        });
        var goToPercent = function (percent) {
            var newOffset = Math.max(-_this.dashOffsetTotal, -_this.dashOffsetStart - percent * _this.dashOffsetTotal / 2 / 100);
            _this.autoplayRing.setAttribute('stroke-dashoffset', '' + newOffset);
        };
        var tick = function () {
            goToPercent((_this.ticks++) * 100 / _this.totalTicks);
        };
        var update = function () {
            if (_this.options_.suspended()) {
                _this.suspendedMessage.innerText = _this.options_.suspendedText;
                goToPercent(0);
                _this.ticks = 0;
                timeout = setTimeout(update.bind(_this), 300); // checks once supsended can be a bit longer
            }
            else if (_this.ticks >= _this.totalTicks) {
                clearTimeout(timeout);
                cb(false);
            }
            else {
                _this.suspendedMessage.innerText = '';
                tick();
                timeout = setTimeout(update.bind(_this), _this.interval);
            }
        };
        this.container.style.display = 'block';
        timeout = setTimeout(update.bind(this), this.interval);
    };
    return EndCard;
}(Component));
video_js_1["default"].registerComponent('EndCard', EndCard);
