"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.UpNextPlugin = void 0;
var video_js_1 = require("video.js");
var Plugin = video_js_1["default"].getPlugin('plugin');
var UpNextPlugin = /** @class */ (function (_super) {
    __extends(UpNextPlugin, _super);
    function UpNextPlugin(player, options) {
        if (options === void 0) { options = {}; }
        var _this = this;
        var settings = {
            next: options.next,
            getTitle: options.getTitle,
            timeout: options.timeout || 5000,
            cancelText: options.cancelText || 'Cancel',
            headText: options.headText || 'Up Next',
            suspendedText: options.suspendedText || 'Autoplay is suspended',
            condition: options.condition,
            suspended: options.suspended
        };
        _this = _super.call(this, player) || this;
        _this.player.ready(function () {
            player.addClass('vjs-upnext');
        });
        player.addChild('EndCard', settings);
        return _this;
    }
    return UpNextPlugin;
}(Plugin));
exports.UpNextPlugin = UpNextPlugin;
video_js_1["default"].registerPlugin('upnext', UpNextPlugin);
