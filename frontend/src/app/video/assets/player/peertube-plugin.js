"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.PeerTubePlugin = void 0;
var video_js_1 = require("video.js");
require("./videojs-components/settings-menu-button");
var utils_1 = require("./utils");
var peertube_player_local_storage_1 = require("./peertube-player-local-storage");
var Plugin = video_js_1["default"].getPlugin('plugin');
var PeerTubePlugin = /** @class */ (function (_super) {
    __extends(PeerTubePlugin, _super);
    function PeerTubePlugin(player, options) {
        var _this = _super.call(this, player) || this;
        _this.CONSTANTS = {
            USER_WATCHING_VIDEO_INTERVAL: 5000 // Every 5 seconds, notify the user is watching the video
        };
        _this.menuOpened = false;
        _this.mouseInControlBar = false;
        _this.videoViewUrl = options.videoViewUrl;
        _this.videoDuration = options.videoDuration;
        _this.videoCaptions = options.videoCaptions;
        _this.isLive = options.isLive;
        _this.savedInactivityTimeout = player.options_.inactivityTimeout;
        if (options.autoplay)
            _this.player.addClass('vjs-has-autoplay');
        _this.player.on('autoplay-failure', function () {
            _this.player.removeClass('vjs-has-autoplay');
        });
        _this.player.ready(function () {
            var playerOptions = _this.player.options_;
            if (options.mode === 'webtorrent') {
                _this.player.webtorrent().on('resolutionChange', function (_, d) { return _this.handleResolutionChange(d); });
                _this.player.webtorrent().on('autoResolutionChange', function (_, d) { return _this.trigger('autoResolutionChange', d); });
            }
            if (options.mode === 'p2p-media-loader') {
                _this.player.p2pMediaLoader().on('resolutionChange', function (_, d) { return _this.handleResolutionChange(d); });
            }
            _this.player.tech(true).on('loadedqualitydata', function () {
                setTimeout(function () {
                    // Replay a resolution change, now we loaded all quality data
                    if (_this.lastResolutionChange)
                        _this.handleResolutionChange(_this.lastResolutionChange);
                }, 0);
            });
            var volume = peertube_player_local_storage_1.getStoredVolume();
            if (volume !== undefined)
                _this.player.volume(volume);
            var muted = playerOptions.muted !== undefined ? playerOptions.muted : peertube_player_local_storage_1.getStoredMute();
            if (muted !== undefined)
                _this.player.muted(muted);
            _this.defaultSubtitle = options.subtitle || peertube_player_local_storage_1.getStoredLastSubtitle();
            _this.player.on('volumechange', function () {
                peertube_player_local_storage_1.saveVolumeInStore(_this.player.volume());
                peertube_player_local_storage_1.saveMuteInStore(_this.player.muted());
            });
            if (options.stopTime) {
                var stopTime_1 = utils_1.timeToInt(options.stopTime);
                var self_1 = _this;
                _this.player.on('timeupdate', function onTimeUpdate() {
                    if (self_1.player.currentTime() > stopTime_1) {
                        self_1.player.pause();
                        self_1.player.trigger('stopped');
                        self_1.player.off('timeupdate', onTimeUpdate);
                    }
                });
            }
            _this.player.textTracks().on('change', function () {
                var showing = _this.player.textTracks().tracks_.find(function (t) {
                    return t.kind === 'captions' && t.mode === 'showing';
                });
                if (!showing) {
                    peertube_player_local_storage_1.saveLastSubtitle('off');
                    return;
                }
                peertube_player_local_storage_1.saveLastSubtitle(showing.language);
            });
            _this.player.on('sourcechange', function () { return _this.initCaptions(); });
            _this.player.duration(options.videoDuration);
            _this.initializePlayer();
            _this.runViewAdd();
            if (options.userWatching)
                _this.runUserWatchVideo(options.userWatching);
        });
        return _this;
    }
    PeerTubePlugin.prototype.dispose = function () {
        if (this.videoViewInterval)
            clearInterval(this.videoViewInterval);
        if (this.userWatchingVideoInterval)
            clearInterval(this.userWatchingVideoInterval);
    };
    PeerTubePlugin.prototype.onMenuOpen = function () {
        this.menuOpened = false;
        this.alterInactivity();
    };
    PeerTubePlugin.prototype.onMenuClosed = function () {
        this.menuOpened = true;
        this.alterInactivity();
    };
    PeerTubePlugin.prototype.initializePlayer = function () {
        if (utils_1.isMobile())
            this.player.addClass('vjs-is-mobile');
        this.initSmoothProgressBar();
        this.initCaptions();
        this.listenControlBarMouse();
    };
    PeerTubePlugin.prototype.runViewAdd = function () {
        var _this = this;
        this.clearVideoViewInterval();
        // After 30 seconds (or 3/4 of the video), add a view to the video
        var minSecondsToView = 30;
        if (!this.isLive && this.videoDuration < minSecondsToView) {
            minSecondsToView = (this.videoDuration * 3) / 4;
        }
        var secondsViewed = 0;
        this.videoViewInterval = setInterval(function () {
            if (_this.player && !_this.player.paused()) {
                secondsViewed += 1;
                if (secondsViewed > minSecondsToView) {
                    // Restart the loop if this is a live
                    if (_this.isLive) {
                        secondsViewed = 0;
                    }
                    else {
                        _this.clearVideoViewInterval();
                    }
                    _this.addViewToVideo()["catch"](function (err) { return console.error(err); });
                }
            }
        }, 1000);
    };
    PeerTubePlugin.prototype.runUserWatchVideo = function (options) {
        var _this = this;
        var lastCurrentTime = 0;
        this.userWatchingVideoInterval = setInterval(function () {
            var currentTime = Math.floor(_this.player.currentTime());
            if (currentTime - lastCurrentTime >= 1) {
                lastCurrentTime = currentTime;
                _this.notifyUserIsWatching(currentTime, options.url, options.authorizationHeader)["catch"](function (err) { return console.error('Cannot notify user is watching.', err); });
            }
        }, this.CONSTANTS.USER_WATCHING_VIDEO_INTERVAL);
    };
    PeerTubePlugin.prototype.clearVideoViewInterval = function () {
        if (this.videoViewInterval !== undefined) {
            clearInterval(this.videoViewInterval);
            this.videoViewInterval = undefined;
        }
    };
    PeerTubePlugin.prototype.addViewToVideo = function () {
        if (!this.videoViewUrl)
            return Promise.resolve(undefined);
        return fetch(this.videoViewUrl, { method: 'POST' });
    };
    PeerTubePlugin.prototype.notifyUserIsWatching = function (currentTime, url, authorizationHeader) {
        var body = new URLSearchParams();
        body.append('currentTime', currentTime.toString());
        var headers = new Headers({ 'Authorization': authorizationHeader });
        return fetch(url, { method: 'PUT', body: body, headers: headers });
    };
    PeerTubePlugin.prototype.handleResolutionChange = function (data) {
        this.lastResolutionChange = data;
        var qualityLevels = this.player.qualityLevels();
        for (var i = 0; i < qualityLevels.length; i++) {
            if (qualityLevels[i].height === data.resolutionId) {
                data.id = qualityLevels[i].id;
                break;
            }
        }
        this.trigger('resolutionChange', data);
    };
    PeerTubePlugin.prototype.listenControlBarMouse = function () {
        var _this = this;
        this.player.controlBar.on('mouseenter', function () {
            _this.mouseInControlBar = true;
            _this.alterInactivity();
        });
        this.player.controlBar.on('mouseleave', function () {
            _this.mouseInControlBar = false;
            _this.alterInactivity();
        });
    };
    PeerTubePlugin.prototype.alterInactivity = function () {
        if (this.menuOpened) {
            this.player.options_.inactivityTimeout = this.savedInactivityTimeout;
            return;
        }
        if (!this.mouseInControlBar && !this.isTouchEnabled()) {
            this.player.options_.inactivityTimeout = 1;
        }
    };
    PeerTubePlugin.prototype.isTouchEnabled = function () {
        return ('ontouchstart' in window) ||
            navigator.maxTouchPoints > 0 ||
            navigator.msMaxTouchPoints > 0;
    };
    PeerTubePlugin.prototype.initCaptions = function () {
        for (var _i = 0, _a = this.videoCaptions; _i < _a.length; _i++) {
            var caption = _a[_i];
            this.player.addRemoteTextTrack({
                kind: 'captions',
                label: caption.label,
                language: caption.language,
                id: caption.language,
                src: caption.src,
                "default": this.defaultSubtitle === caption.language
            }, false);
        }
        this.player.trigger('captionsChanged');
    };
    // Thanks: https://github.com/videojs/video.js/issues/4460#issuecomment-312861657
    PeerTubePlugin.prototype.initSmoothProgressBar = function () {
        var SeekBar = video_js_1["default"].getComponent('SeekBar');
        SeekBar.prototype.getPercent = function getPercent() {
            // Allows for smooth scrubbing, when player can't keep up.
            // const time = (this.player_.scrubbing()) ?
            //   this.player_.getCache().currentTime :
            //   this.player_.currentTime()
            var time = this.player_.currentTime();
            var percent = time / this.player_.duration();
            return percent >= 1 ? 1 : percent;
        };
        SeekBar.prototype.handleMouseMove = function handleMouseMove(event) {
            var newTime = this.calculateDistance(event) * this.player_.duration();
            if (newTime === this.player_.duration()) {
                newTime = newTime - 0.1;
            }
            this.player_.currentTime(newTime);
            this.update();
        };
    };
    return PeerTubePlugin;
}(Plugin));
exports.PeerTubePlugin = PeerTubePlugin;
video_js_1["default"].registerPlugin('peertube', PeerTubePlugin);
