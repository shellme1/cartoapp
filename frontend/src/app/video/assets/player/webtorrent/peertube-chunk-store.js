"use strict";
// From https://github.com/MinEduTDF/idb-chunk-store
// We use temporary IndexDB (all data are removed on destroy) to avoid RAM issues
// Thanks @santiagogil and @Feross
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.PeertubeChunkStore = void 0;
var events_1 = require("events");
var dexie_1 = require("dexie");
var ChunkDatabase = /** @class */ (function (_super) {
    __extends(ChunkDatabase, _super);
    function ChunkDatabase(dbname) {
        var _this = _super.call(this, dbname) || this;
        _this.version(1).stores({
            chunks: 'id'
        });
        return _this;
    }
    return ChunkDatabase;
}(dexie_1["default"]));
var ExpirationDatabase = /** @class */ (function (_super) {
    __extends(ExpirationDatabase, _super);
    function ExpirationDatabase() {
        var _this = _super.call(this, 'webtorrent-expiration') || this;
        _this.version(1).stores({
            databases: 'name,expiration'
        });
        return _this;
    }
    return ExpirationDatabase;
}(dexie_1["default"]));
var PeertubeChunkStore = /** @class */ (function (_super) {
    __extends(PeertubeChunkStore, _super);
    function PeertubeChunkStore(chunkLength, opts) {
        var _this = _super.call(this) || this;
        _this.pendingPut = [];
        // If the store is full
        _this.memoryChunks = {};
        _this.databaseName = 'webtorrent-chunks-';
        if (!opts)
            opts = {};
        if (opts.torrent && opts.torrent.infoHash)
            _this.databaseName += opts.torrent.infoHash;
        else
            _this.databaseName += '-default';
        _this.setMaxListeners(100);
        _this.chunkLength = Number(chunkLength);
        if (!_this.chunkLength)
            throw new Error('First argument must be a chunk length');
        _this.length = Number(opts.length) || Infinity;
        if (_this.length !== Infinity) {
            _this.lastChunkLength = (_this.length % _this.chunkLength) || _this.chunkLength;
            _this.lastChunkIndex = Math.ceil(_this.length / _this.chunkLength) - 1;
        }
        _this.db = new ChunkDatabase(_this.databaseName);
        // Track databases that expired
        _this.expirationDB = new ExpirationDatabase();
        _this.runCleaner();
        return _this;
    }
    PeertubeChunkStore.prototype.put = function (index, buf, cb) {
        var _this = this;
        var isLastChunk = (index === this.lastChunkIndex);
        if (isLastChunk && buf.length !== this.lastChunkLength) {
            return this.nextTick(cb, new Error('Last chunk length must be ' + this.lastChunkLength));
        }
        if (!isLastChunk && buf.length !== this.chunkLength) {
            return this.nextTick(cb, new Error('Chunk length must be ' + this.chunkLength));
        }
        // Specify we have this chunk
        this.memoryChunks[index] = true;
        // Add it to the pending put
        this.pendingPut.push({ id: index, buf: buf, cb: cb });
        // If it's already planned, return
        if (this.putBulkTimeout)
            return;
        // Plan a future bulk insert
        this.putBulkTimeout = setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
            var processing, err_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        processing = this.pendingPut;
                        this.pendingPut = [];
                        this.putBulkTimeout = undefined;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        return [4 /*yield*/, this.db.transaction('rw', this.db.chunks, function () {
                                return _this.db.chunks.bulkPut(processing.map(function (p) { return ({ id: p.id, buf: p.buf }); }));
                            })];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        err_1 = _a.sent();
                        console.log('Cannot bulk insert chunks. Store them in memory.', { err: err_1 });
                        processing.forEach(function (p) { return _this.memoryChunks[p.id] = p.buf; });
                        return [3 /*break*/, 5];
                    case 4:
                        processing.forEach(function (p) { return p.cb(); });
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        }); }, PeertubeChunkStore.BUFFERING_PUT_MS);
    };
    PeertubeChunkStore.prototype.get = function (index, opts, cb) {
        var _this = this;
        if (typeof opts === 'function')
            return this.get(index, null, opts);
        // IndexDB could be slow, use our memory index first
        var memoryChunk = this.memoryChunks[index];
        if (memoryChunk === undefined) {
            var err_2 = new Error('Chunk not found');
            err_2['notFound'] = true;
            return process.nextTick(function () { return cb(err_2); });
        }
        // Chunk in memory
        if (memoryChunk !== true)
            return cb(null, memoryChunk);
        // Chunk in store
        this.db.transaction('r', this.db.chunks, function () { return __awaiter(_this, void 0, void 0, function () {
            var result, buf, offset, len;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.db.chunks.get({ id: index })];
                    case 1:
                        result = _a.sent();
                        if (result === undefined)
                            return [2 /*return*/, cb(null, Buffer.alloc(0))];
                        buf = result.buf;
                        if (!opts)
                            return [2 /*return*/, this.nextTick(cb, null, buf)];
                        offset = opts.offset || 0;
                        len = opts.length || (buf.length - offset);
                        return [2 /*return*/, cb(null, buf.slice(offset, len + offset))];
                }
            });
        }); })["catch"](function (err) {
            console.error(err);
            return cb(err);
        });
    };
    PeertubeChunkStore.prototype.close = function (cb) {
        return this.destroy(cb);
    };
    PeertubeChunkStore.prototype.destroy = function (cb) {
        return __awaiter(this, void 0, void 0, function () {
            var err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (this.pendingPut) {
                            clearTimeout(this.putBulkTimeout);
                            this.pendingPut = null;
                        }
                        if (this.cleanerInterval) {
                            clearInterval(this.cleanerInterval);
                            this.cleanerInterval = null;
                        }
                        if (!this.db) return [3 /*break*/, 2];
                        this.db.close();
                        return [4 /*yield*/, this.dropDatabase(this.databaseName)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (this.expirationDB) {
                            this.expirationDB.close();
                            this.expirationDB = null;
                        }
                        return [2 /*return*/, cb()];
                    case 3:
                        err_3 = _a.sent();
                        console.error('Cannot destroy peertube chunk store.', err_3);
                        return [2 /*return*/, cb(err_3)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    PeertubeChunkStore.prototype.runCleaner = function () {
        var _this = this;
        this.checkExpiration();
        this.cleanerInterval = setInterval(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.checkExpiration();
                return [2 /*return*/];
            });
        }); }, PeertubeChunkStore.CLEANER_INTERVAL_MS);
    };
    PeertubeChunkStore.prototype.checkExpiration = function () {
        return __awaiter(this, void 0, void 0, function () {
            var databasesToDeleteInfo, err_4, _i, databasesToDeleteInfo_1, databaseToDeleteInfo;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        databasesToDeleteInfo = [];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.expirationDB.transaction('rw', this.expirationDB.databases, function () { return __awaiter(_this, void 0, void 0, function () {
                                var now;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: 
                                        // Update our database expiration since we are alive
                                        return [4 /*yield*/, this.expirationDB.databases.put({
                                                name: this.databaseName,
                                                expiration: new Date().getTime() + PeertubeChunkStore.CLEANER_EXPIRATION_MS
                                            })];
                                        case 1:
                                            // Update our database expiration since we are alive
                                            _a.sent();
                                            now = new Date().getTime();
                                            return [4 /*yield*/, this.expirationDB.databases.where('expiration').below(now).toArray()];
                                        case 2:
                                            databasesToDeleteInfo = _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_4 = _a.sent();
                        console.error('Cannot update expiration of fetch expired databases.', err_4);
                        return [3 /*break*/, 4];
                    case 4:
                        _i = 0, databasesToDeleteInfo_1 = databasesToDeleteInfo;
                        _a.label = 5;
                    case 5:
                        if (!(_i < databasesToDeleteInfo_1.length)) return [3 /*break*/, 8];
                        databaseToDeleteInfo = databasesToDeleteInfo_1[_i];
                        return [4 /*yield*/, this.dropDatabase(databaseToDeleteInfo.name)];
                    case 6:
                        _a.sent();
                        _a.label = 7;
                    case 7:
                        _i++;
                        return [3 /*break*/, 5];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    PeertubeChunkStore.prototype.dropDatabase = function (databaseName) {
        return __awaiter(this, void 0, void 0, function () {
            var dbToDelete, err_5;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        dbToDelete = new ChunkDatabase(databaseName);
                        console.log('Destroying IndexDB database %s.', databaseName);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, dbToDelete["delete"]()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.expirationDB.transaction('rw', this.expirationDB.databases, function () {
                                return _this.expirationDB.databases.where({ name: databaseName })["delete"]();
                            })];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        err_5 = _a.sent();
                        console.error('Cannot delete %s.', databaseName, err_5);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    PeertubeChunkStore.prototype.nextTick = function (cb, err, val) {
        process.nextTick(function () { return cb(err, val); }, undefined);
    };
    PeertubeChunkStore.BUFFERING_PUT_MS = 1000;
    PeertubeChunkStore.CLEANER_INTERVAL_MS = 1000 * 60; // 1 minute
    PeertubeChunkStore.CLEANER_EXPIRATION_MS = 1000 * 60 * 5; // 5 minutes
    return PeertubeChunkStore;
}(events_1.EventEmitter));
exports.PeertubeChunkStore = PeertubeChunkStore;
