"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.WebTorrentPlugin = void 0;
var video_js_1 = require("video.js");
var WebTorrent = require("webtorrent");
var video_renderer_1 = require("./video-renderer");
var utils_1 = require("../utils");
var peertube_chunk_store_1 = require("./peertube-chunk-store");
var peertube_player_local_storage_1 = require("../peertube-player-local-storage");
var CacheChunkStore = require('cache-chunk-store');
var Plugin = video_js_1["default"].getPlugin('plugin');
var WebTorrentPlugin = /** @class */ (function (_super) {
    __extends(WebTorrentPlugin, _super);
    function WebTorrentPlugin(player, options) {
        var _this = _super.call(this, player) || this;
        _this.autoplay = false;
        _this.startTime = 0;
        _this.CONSTANTS = {
            INFO_SCHEDULER: 1000,
            AUTO_QUALITY_SCHEDULER: 3000,
            AUTO_QUALITY_THRESHOLD_PERCENT: 30,
            AUTO_QUALITY_OBSERVATION_TIME: 10000,
            AUTO_QUALITY_HIGHER_RESOLUTION_DELAY: 5000,
            BANDWIDTH_AVERAGE_NUMBER_OF_VALUES: 5 // Last 5 seconds to build average bandwidth
        };
        _this.webtorrent = new WebTorrent({
            tracker: {
                rtcConfig: utils_1.getRtcConfig()
            },
            dht: false
        });
        _this.destroyingFakeRenderer = false;
        _this.autoResolution = true;
        _this.autoResolutionPossible = true;
        _this.isAutoResolutionObservation = false;
        _this.playerRefusedP2P = false;
        _this.downloadSpeeds = [];
        _this.startTime = utils_1.timeToInt(options.startTime);
        // Disable auto play on iOS
        _this.autoplay = options.autoplay;
        _this.playerRefusedP2P = !peertube_player_local_storage_1.getStoredP2PEnabled();
        _this.videoFiles = options.videoFiles;
        _this.videoDuration = options.videoDuration;
        _this.savePlayerSrcFunction = _this.player.src;
        _this.playerElement = options.playerElement;
        _this.player.ready(function () {
            var playerOptions = _this.player.options_;
            var volume = peertube_player_local_storage_1.getStoredVolume();
            if (volume !== undefined)
                _this.player.volume(volume);
            var muted = playerOptions.muted !== undefined ? playerOptions.muted : peertube_player_local_storage_1.getStoredMute();
            if (muted !== undefined)
                _this.player.muted(muted);
            _this.player.duration(options.videoDuration);
            _this.initializePlayer();
            _this.runTorrentInfoScheduler();
            _this.player.one('play', function () {
                // Don't run immediately scheduler, wait some seconds the TCP connections are made
                _this.runAutoQualitySchedulerTimer = setTimeout(function () { return _this.runAutoQualityScheduler(); }, _this.CONSTANTS.AUTO_QUALITY_SCHEDULER);
            });
        });
        return _this;
    }
    WebTorrentPlugin.prototype.dispose = function () {
        clearTimeout(this.addTorrentDelay);
        clearTimeout(this.qualityObservationTimer);
        clearTimeout(this.runAutoQualitySchedulerTimer);
        clearInterval(this.torrentInfoInterval);
        clearInterval(this.autoQualityInterval);
        // Don't need to destroy renderer, video player will be destroyed
        this.flushVideoFile(this.currentVideoFile, false);
        this.destroyFakeRenderer();
    };
    WebTorrentPlugin.prototype.getCurrentResolutionId = function () {
        return this.currentVideoFile ? this.currentVideoFile.resolution.id : -1;
    };
    WebTorrentPlugin.prototype.updateVideoFile = function (videoFile, options, done) {
        var _this = this;
        if (options === void 0) { options = {}; }
        if (done === void 0) { done = function () { }; }
        // Automatically choose the adapted video file
        if (!videoFile) {
            var savedAverageBandwidth = peertube_player_local_storage_1.getAverageBandwidthInStore();
            videoFile = savedAverageBandwidth
                ? this.getAppropriateFile(savedAverageBandwidth)
                : this.pickAverageVideoFile();
        }
        if (!videoFile) {
            throw Error("Can't update video file since videoFile is undefined.");
        }
        // Don't add the same video file once again
        if (this.currentVideoFile !== undefined && this.currentVideoFile.magnetUri === videoFile.magnetUri) {
            return;
        }
        // Do not display error to user because we will have multiple fallback
        this.disableErrorDisplay();
        // Hack to "simulate" src link in video.js >= 6
        // Without this, we can't play the video after pausing it
        // https://github.com/videojs/video.js/blob/master/src/js/player.js#L1633
        this.player.src = function () { return true; };
        var oldPlaybackRate = this.player.playbackRate();
        var previousVideoFile = this.currentVideoFile;
        this.currentVideoFile = videoFile;
        // Don't try on iOS that does not support MediaSource
        // Or don't use P2P if webtorrent is disabled
        if (utils_1.isIOS() || this.playerRefusedP2P) {
            return this.fallbackToHttp(options, function () {
                _this.player.playbackRate(oldPlaybackRate);
                return done();
            });
        }
        this.addTorrent(this.currentVideoFile.magnetUri, previousVideoFile, options, function () {
            _this.player.playbackRate(oldPlaybackRate);
            return done();
        });
        this.changeQuality();
        this.trigger('resolutionChange', { auto: this.autoResolution, resolutionId: this.currentVideoFile.resolution.id });
    };
    WebTorrentPlugin.prototype.updateResolution = function (resolutionId, delay) {
        if (delay === void 0) { delay = 0; }
        // Remember player state
        var currentTime = this.player.currentTime();
        var isPaused = this.player.paused();
        // Hide bigPlayButton
        if (!isPaused) {
            this.player.bigPlayButton.hide();
        }
        // Audio-only (resolutionId === 0) gets special treatment
        if (resolutionId === 0) {
            // Audio-only: show poster, do not auto-hide controls
            this.player.addClass('vjs-playing-audio-only-content');
            this.player.posterImage.show();
        }
        else {
            // Hide poster to have black background
            this.player.removeClass('vjs-playing-audio-only-content');
            this.player.posterImage.hide();
        }
        var newVideoFile = this.videoFiles.find(function (f) { return f.resolution.id === resolutionId; });
        var options = {
            forcePlay: false,
            delay: delay,
            seek: currentTime + (delay / 1000)
        };
        this.updateVideoFile(newVideoFile, options);
    };
    WebTorrentPlugin.prototype.flushVideoFile = function (videoFile, destroyRenderer) {
        if (destroyRenderer === void 0) { destroyRenderer = true; }
        if (videoFile !== undefined && this.webtorrent.get(videoFile.magnetUri)) {
            if (destroyRenderer === true && this.renderer && this.renderer.destroy)
                this.renderer.destroy();
            this.webtorrent.remove(videoFile.magnetUri);
            console.log('Removed ' + videoFile.magnetUri);
        }
    };
    WebTorrentPlugin.prototype.enableAutoResolution = function () {
        this.autoResolution = true;
        this.trigger('resolutionChange', { auto: this.autoResolution, resolutionId: this.getCurrentResolutionId() });
    };
    WebTorrentPlugin.prototype.disableAutoResolution = function (forbid) {
        if (forbid === void 0) { forbid = false; }
        if (forbid === true)
            this.autoResolutionPossible = false;
        this.autoResolution = false;
        this.trigger('autoResolutionChange', { possible: this.autoResolutionPossible });
        this.trigger('resolutionChange', { auto: this.autoResolution, resolutionId: this.getCurrentResolutionId() });
    };
    WebTorrentPlugin.prototype.isAutoResolutionPossible = function () {
        return this.autoResolutionPossible;
    };
    WebTorrentPlugin.prototype.getTorrent = function () {
        return this.torrent;
    };
    WebTorrentPlugin.prototype.getCurrentVideoFile = function () {
        return this.currentVideoFile;
    };
    WebTorrentPlugin.prototype.addTorrent = function (magnetOrTorrentUrl, previousVideoFile, options, done) {
        var _this = this;
        console.log('Adding ' + magnetOrTorrentUrl + '.');
        var oldTorrent = this.torrent;
        var torrentOptions = {
            // Don't use arrow function: it breaks webtorrent (that uses `new` keyword)
            store: function (chunkLength, storeOpts) {
                return new CacheChunkStore(new peertube_chunk_store_1.PeertubeChunkStore(chunkLength, storeOpts), {
                    max: 100
                });
            }
        };
        this.torrent = this.webtorrent.add(magnetOrTorrentUrl, torrentOptions, function (torrent) {
            console.log('Added ' + magnetOrTorrentUrl + '.');
            if (oldTorrent) {
                // Pause the old torrent
                _this.stopTorrent(oldTorrent);
                // We use a fake renderer so we download correct pieces of the next file
                if (options.delay)
                    _this.renderFileInFakeElement(torrent.files[0], options.delay);
            }
            // Render the video in a few seconds? (on resolution change for example, we wait some seconds of the new video resolution)
            _this.addTorrentDelay = setTimeout(function () {
                // We don't need the fake renderer anymore
                _this.destroyFakeRenderer();
                var paused = _this.player.paused();
                _this.flushVideoFile(previousVideoFile);
                // Update progress bar (just for the UI), do not wait rendering
                if (options.seek)
                    _this.player.currentTime(options.seek);
                var renderVideoOptions = { autoplay: false, controls: true };
                video_renderer_1.renderVideo(torrent.files[0], _this.playerElement, renderVideoOptions, function (err, renderer) {
                    _this.renderer = renderer;
                    if (err)
                        return _this.fallbackToHttp(options, done);
                    return _this.tryToPlay(function (err) {
                        if (err)
                            return done(err);
                        if (options.seek)
                            _this.seek(options.seek);
                        if (options.forcePlay === false && paused === true)
                            _this.player.pause();
                        return done();
                    });
                });
            }, options.delay || 0);
        });
        this.torrent.on('error', function (err) { return console.error(err); });
        this.torrent.on('warning', function (err) {
            // We don't support HTTP tracker but we don't care -> we use the web socket tracker
            if (err.message.indexOf('Unsupported tracker protocol') !== -1)
                return;
            // Users don't care about issues with WebRTC, but developers do so log it in the console
            if (err.message.indexOf('Ice connection failed') !== -1) {
                console.log(err);
                return;
            }
            // Magnet hash is not up to date with the torrent file, add directly the torrent file
            if (err.message.indexOf('incorrect info hash') !== -1) {
                console.error('Incorrect info hash detected, falling back to torrent file.');
                var newOptions = { forcePlay: true, seek: options.seek };
                return _this.addTorrent(_this.torrent['xs'], previousVideoFile, newOptions, done);
            }
            // Remote instance is down
            if (err.message.indexOf('from xs param') !== -1) {
                _this.handleError(err);
            }
            console.warn(err);
        });
    };
    WebTorrentPlugin.prototype.tryToPlay = function (done) {
        var _this = this;
        if (!done)
            done = function () { };
        var playPromise = this.player.play();
        if (playPromise !== undefined) {
            return playPromise.then(function () { return done(); })["catch"](function (err) {
                if (err.message.indexOf('The play() request was interrupted by a call to pause()') !== -1) {
                    return;
                }
                console.error(err);
                _this.player.pause();
                _this.player.posterImage.show();
                _this.player.removeClass('vjs-has-autoplay');
                _this.player.removeClass('vjs-has-big-play-button-clicked');
                _this.player.removeClass('vjs-playing-audio-only-content');
                return done();
            });
        }
        return done();
    };
    WebTorrentPlugin.prototype.seek = function (time) {
        this.player.currentTime(time);
        this.player.handleTechSeeked_();
    };
    WebTorrentPlugin.prototype.getAppropriateFile = function (averageDownloadSpeed) {
        var _this = this;
        if (this.videoFiles === undefined)
            return undefined;
        var files = this.videoFiles.filter(function (f) { return f.resolution.id !== 0; });
        if (files.length === 0)
            return undefined;
        if (files.length === 1)
            return files[0];
        // Don't change the torrent if the player ended
        if (this.torrent && this.torrent.progress === 1 && this.player.ended())
            return this.currentVideoFile;
        if (!averageDownloadSpeed)
            averageDownloadSpeed = this.getAndSaveActualDownloadSpeed();
        // Limit resolution according to player height
        var playerHeight = this.playerElement.offsetHeight;
        // We take the first resolution just above the player height
        // Example: player height is 530px, we want the 720p file instead of 480p
        var maxResolution = files[0].resolution.id;
        for (var i = files.length - 1; i >= 0; i--) {
            var resolutionId = files[i].resolution.id;
            if (resolutionId !== 0 && resolutionId >= playerHeight) {
                maxResolution = resolutionId;
                break;
            }
        }
        // Filter videos we can play according to our screen resolution and bandwidth
        var filteredFiles = files.filter(function (f) { return f.resolution.id <= maxResolution; })
            .filter(function (f) {
            var fileBitrate = (f.size / _this.videoDuration);
            var threshold = fileBitrate;
            // If this is for a higher resolution or an initial load: add a margin
            if (!_this.currentVideoFile || f.resolution.id > _this.currentVideoFile.resolution.id) {
                threshold += ((fileBitrate * _this.CONSTANTS.AUTO_QUALITY_THRESHOLD_PERCENT) / 100);
            }
            return averageDownloadSpeed > threshold;
        });
        // If the download speed is too bad, return the lowest resolution we have
        if (filteredFiles.length === 0)
            return utils_1.videoFileMinByResolution(files);
        return utils_1.videoFileMaxByResolution(filteredFiles);
    };
    WebTorrentPlugin.prototype.getAndSaveActualDownloadSpeed = function () {
        var start = Math.max(this.downloadSpeeds.length - this.CONSTANTS.BANDWIDTH_AVERAGE_NUMBER_OF_VALUES, 0);
        var lastDownloadSpeeds = this.downloadSpeeds.slice(start, this.downloadSpeeds.length);
        if (lastDownloadSpeeds.length === 0)
            return -1;
        var sum = lastDownloadSpeeds.reduce(function (a, b) { return a + b; });
        var averageBandwidth = Math.round(sum / lastDownloadSpeeds.length);
        // Save the average bandwidth for future use
        peertube_player_local_storage_1.saveAverageBandwidth(averageBandwidth);
        return averageBandwidth;
    };
    WebTorrentPlugin.prototype.initializePlayer = function () {
        var _this = this;
        this.buildQualities();
        if (this.autoplay) {
            this.player.posterImage.hide();
            return this.updateVideoFile(undefined, { forcePlay: true, seek: this.startTime });
        }
        // Proxy first play
        var oldPlay = this.player.play.bind(this.player);
        this.player.play = function () {
            _this.player.addClass('vjs-has-big-play-button-clicked');
            _this.player.play = oldPlay;
            _this.updateVideoFile(undefined, { forcePlay: true, seek: _this.startTime });
        };
    };
    WebTorrentPlugin.prototype.runAutoQualityScheduler = function () {
        var _this = this;
        this.autoQualityInterval = setInterval(function () {
            // Not initialized or in HTTP fallback
            if (_this.torrent === undefined || _this.torrent === null)
                return;
            if (_this.autoResolution === false)
                return;
            if (_this.isAutoResolutionObservation === true)
                return;
            var file = _this.getAppropriateFile();
            var changeResolution = false;
            var changeResolutionDelay = 0;
            // Lower resolution
            if (_this.isPlayerWaiting() && file.resolution.id < _this.currentVideoFile.resolution.id) {
                console.log('Downgrading automatically the resolution to: %s', file.resolution.label);
                changeResolution = true;
            }
            else if (file.resolution.id > _this.currentVideoFile.resolution.id) { // Higher resolution
                console.log('Upgrading automatically the resolution to: %s', file.resolution.label);
                changeResolution = true;
                changeResolutionDelay = _this.CONSTANTS.AUTO_QUALITY_HIGHER_RESOLUTION_DELAY;
            }
            if (changeResolution === true) {
                _this.updateResolution(file.resolution.id, changeResolutionDelay);
                // Wait some seconds in observation of our new resolution
                _this.isAutoResolutionObservation = true;
                _this.qualityObservationTimer = setTimeout(function () {
                    _this.isAutoResolutionObservation = false;
                }, _this.CONSTANTS.AUTO_QUALITY_OBSERVATION_TIME);
            }
        }, this.CONSTANTS.AUTO_QUALITY_SCHEDULER);
    };
    WebTorrentPlugin.prototype.isPlayerWaiting = function () {
        return this.player && this.player.hasClass('vjs-waiting');
    };
    WebTorrentPlugin.prototype.runTorrentInfoScheduler = function () {
        var _this = this;
        this.torrentInfoInterval = setInterval(function () {
            // Not initialized yet
            if (_this.torrent === undefined)
                return;
            // Http fallback
            if (_this.torrent === null)
                return _this.player.trigger('p2pInfo', false);
            // this.webtorrent.downloadSpeed because we need to take into account the potential old torrent too
            if (_this.webtorrent.downloadSpeed !== 0)
                _this.downloadSpeeds.push(_this.webtorrent.downloadSpeed);
            return _this.player.trigger('p2pInfo', {
                source: 'webtorrent',
                http: {
                    downloadSpeed: 0,
                    uploadSpeed: 0,
                    downloaded: 0,
                    uploaded: 0
                },
                p2p: {
                    downloadSpeed: _this.torrent.downloadSpeed,
                    numPeers: _this.torrent.numPeers,
                    uploadSpeed: _this.torrent.uploadSpeed,
                    downloaded: _this.torrent.downloaded,
                    uploaded: _this.torrent.uploaded
                }
            });
        }, this.CONSTANTS.INFO_SCHEDULER);
    };
    WebTorrentPlugin.prototype.fallbackToHttp = function (options, done) {
        var _this = this;
        var paused = this.player.paused();
        this.disableAutoResolution(true);
        this.flushVideoFile(this.currentVideoFile, true);
        this.torrent = null;
        // Enable error display now this is our last fallback
        this.player.one('error', function () { return _this.enableErrorDisplay(); });
        var httpUrl = this.currentVideoFile.fileUrl;
        this.player.src = this.savePlayerSrcFunction;
        this.player.src(httpUrl);
        this.changeQuality();
        // We changed the source, so reinit captions
        this.player.trigger('sourcechange');
        return this.tryToPlay(function (err) {
            if (err && done)
                return done(err);
            if (options.seek)
                _this.seek(options.seek);
            if (options.forcePlay === false && paused === true)
                _this.player.pause();
            if (done)
                return done();
        });
    };
    WebTorrentPlugin.prototype.handleError = function (err) {
        return this.player.trigger('customError', { err: err });
    };
    WebTorrentPlugin.prototype.enableErrorDisplay = function () {
        this.player.addClass('vjs-error-display-enabled');
    };
    WebTorrentPlugin.prototype.disableErrorDisplay = function () {
        this.player.removeClass('vjs-error-display-enabled');
    };
    WebTorrentPlugin.prototype.pickAverageVideoFile = function () {
        if (this.videoFiles.length === 1)
            return this.videoFiles[0];
        return this.videoFiles[Math.floor(this.videoFiles.length / 2)];
    };
    WebTorrentPlugin.prototype.stopTorrent = function (torrent) {
        torrent.pause();
        // Pause does not remove actual peers (in particular the webseed peer)
        torrent.removePeer(torrent['ws']);
    };
    WebTorrentPlugin.prototype.renderFileInFakeElement = function (file, delay) {
        var _this = this;
        this.destroyingFakeRenderer = false;
        var fakeVideoElem = document.createElement('video');
        video_renderer_1.renderVideo(file, fakeVideoElem, { autoplay: false, controls: false }, function (err, renderer) {
            _this.fakeRenderer = renderer;
            // The renderer returns an error when we destroy it, so skip them
            if (_this.destroyingFakeRenderer === false && err) {
                console.error('Cannot render new torrent in fake video element.', err);
            }
            // Load the future file at the correct time (in delay MS - 2 seconds)
            fakeVideoElem.currentTime = _this.player.currentTime() + (delay - 2000);
        });
    };
    WebTorrentPlugin.prototype.destroyFakeRenderer = function () {
        if (this.fakeRenderer) {
            this.destroyingFakeRenderer = true;
            if (this.fakeRenderer.destroy) {
                try {
                    this.fakeRenderer.destroy();
                }
                catch (err) {
                    console.log('Cannot destroy correctly fake renderer.', err);
                }
            }
            this.fakeRenderer = undefined;
        }
    };
    WebTorrentPlugin.prototype.buildQualities = function () {
        var _this = this;
        var qualityLevelsPayload = [];
        for (var _i = 0, _a = this.videoFiles; _i < _a.length; _i++) {
            var file = _a[_i];
            var representation = {
                id: file.resolution.id,
                label: this.buildQualityLabel(file),
                height: file.resolution.id,
                _enabled: true
            };
            this.player.qualityLevels().addQualityLevel(representation);
            qualityLevelsPayload.push({
                id: representation.id,
                label: representation.label,
                selected: false
            });
        }
        var payload = {
            qualitySwitchCallback: function (d) { return _this.qualitySwitchCallback(d); },
            qualityData: {
                video: qualityLevelsPayload
            }
        };
        this.player.tech(true).trigger('loadedqualitydata', payload);
    };
    WebTorrentPlugin.prototype.buildQualityLabel = function (file) {
        var label = file.resolution.label;
        if (file.fps && file.fps >= 50) {
            label += file.fps;
        }
        return label;
    };
    WebTorrentPlugin.prototype.qualitySwitchCallback = function (id) {
        if (id === -1) {
            if (this.autoResolutionPossible === true)
                this.enableAutoResolution();
            return;
        }
        this.disableAutoResolution();
        this.updateResolution(id);
    };
    WebTorrentPlugin.prototype.changeQuality = function () {
        var resolutionId = this.currentVideoFile.resolution.id;
        var qualityLevels = this.player.qualityLevels();
        if (resolutionId === -1) {
            qualityLevels.selectedIndex = -1;
            return;
        }
        for (var i = 0; i < qualityLevels.length; i++) {
            var q = qualityLevels[i];
            if (q.height === resolutionId)
                qualityLevels.selectedIndex_ = i;
        }
    };
    return WebTorrentPlugin;
}(Plugin));
exports.WebTorrentPlugin = WebTorrentPlugin;
video_js_1["default"].registerPlugin('webtorrent', WebTorrentPlugin);
