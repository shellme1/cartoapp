"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.PlaylistButton = void 0;
var video_js_1 = require("video.js");
var ClickableComponent = video_js_1["default"].getComponent('ClickableComponent');
var PlaylistButton = /** @class */ (function (_super) {
    __extends(PlaylistButton, _super);
    function PlaylistButton(player, options) {
        return _super.call(this, player, options) || this;
    }
    PlaylistButton.prototype.createEl = function () {
        this.wrapper = _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-playlist-button',
            innerHTML: '',
            tabIndex: -1
        });
        var icon = _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-playlist-icon',
            innerHTML: '',
            tabIndex: -1
        });
        this.playlistInfoElement = _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-playlist-info',
            innerHTML: '',
            tabIndex: -1
        });
        this.wrapper.appendChild(icon);
        this.wrapper.appendChild(this.playlistInfoElement);
        this.update();
        return this.wrapper;
    };
    PlaylistButton.prototype.update = function () {
        var options = this.options_;
        this.playlistInfoElement.innerHTML = options.getCurrentPosition() + '/' + options.playlist.videosLength;
        this.wrapper.title = this.player().localize('Playlist: {1}', [options.playlist.displayName]);
    };
    PlaylistButton.prototype.handleClick = function () {
        var playlistMenu = this.getPlaylistMenu();
        playlistMenu.open();
    };
    PlaylistButton.prototype.getPlaylistMenu = function () {
        return this.options_.playlistMenu;
    };
    return PlaylistButton;
}(ClickableComponent));
exports.PlaylistButton = PlaylistButton;
video_js_1["default"].registerComponent('PlaylistButton', PlaylistButton);
