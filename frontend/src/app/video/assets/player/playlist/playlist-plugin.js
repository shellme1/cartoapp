"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.PlaylistPlugin = void 0;
var video_js_1 = require("video.js");
var playlist_button_1 = require("./playlist-button");
var playlist_menu_1 = require("./playlist-menu");
var Plugin = video_js_1["default"].getPlugin('plugin');
var PlaylistPlugin = /** @class */ (function (_super) {
    __extends(PlaylistPlugin, _super);
    function PlaylistPlugin(player, options) {
        var _this = _super.call(this, player, options) || this;
        _this.options = options;
        _this.player.ready(function () {
            player.addClass('vjs-playlist');
        });
        _this.playlistMenu = new playlist_menu_1.PlaylistMenu(player, options);
        _this.playlistButton = new playlist_button_1.PlaylistButton(player, Object.assign({}, options, { playlistMenu: _this.playlistMenu }));
        player.addChild(_this.playlistMenu, options);
        player.addChild(_this.playlistButton, options);
        return _this;
    }
    PlaylistPlugin.prototype.updateSelected = function () {
        this.playlistMenu.updateSelected(this.options.getCurrentPosition());
    };
    return PlaylistPlugin;
}(Plugin));
exports.PlaylistPlugin = PlaylistPlugin;
video_js_1["default"].registerPlugin('playlist', PlaylistPlugin);
