"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.PlaylistMenu = void 0;
var video_js_1 = require("video.js");
var playlist_menu_item_1 = require("./playlist-menu-item");
var Component = video_js_1["default"].getComponent('Component');
var PlaylistMenu = /** @class */ (function (_super) {
    __extends(PlaylistMenu, _super);
    function PlaylistMenu(player, options) {
        var _this = _super.call(this, player, options) || this;
        var self = _this;
        function userInactiveHandler() {
            self.close();
        }
        _this.el().addEventListener('mouseenter', function () {
            _this.player().off('userinactive', userInactiveHandler);
        });
        _this.el().addEventListener('mouseleave', function () {
            _this.player().one('userinactive', userInactiveHandler);
        });
        _this.player().on('click', function (event) {
            var current = event.target;
            do {
                if (current.classList.contains('vjs-playlist-menu') ||
                    current.classList.contains('vjs-playlist-button')) {
                    return;
                }
                current = current.parentElement;
            } while (current);
            _this.close();
        });
        return _this;
    }
    PlaylistMenu.prototype.createEl = function () {
        var _this = this;
        this.menuItems = [];
        var options = this.getOptions();
        var menu = _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-playlist-menu',
            innerHTML: '',
            tabIndex: -1
        });
        var header = _super.prototype.createEl.call(this, 'div', {
            className: 'header'
        });
        var headerLeft = _super.prototype.createEl.call(this, 'div');
        var leftTitle = _super.prototype.createEl.call(this, 'div', {
            innerHTML: options.playlist.displayName,
            className: 'title'
        });
        var playlistChannel = options.playlist.videoChannel;
        var leftSubtitle = _super.prototype.createEl.call(this, 'div', {
            innerHTML: playlistChannel
                ? this.player().localize('By {1}', [playlistChannel.displayName])
                : '',
            className: 'channel'
        });
        headerLeft.appendChild(leftTitle);
        headerLeft.appendChild(leftSubtitle);
        var tick = _super.prototype.createEl.call(this, 'div', {
            className: 'cross'
        });
        tick.addEventListener('click', function () { return _this.close(); });
        header.appendChild(headerLeft);
        header.appendChild(tick);
        var list = _super.prototype.createEl.call(this, 'ol');
        var _loop_1 = function (playlistElement) {
            var item = new playlist_menu_item_1.PlaylistMenuItem(this_1.player(), {
                element: playlistElement,
                onClicked: function () { return _this.onItemClicked(playlistElement); }
            });
            list.appendChild(item.el());
            this_1.menuItems.push(item);
        };
        var this_1 = this;
        for (var _i = 0, _a = options.elements; _i < _a.length; _i++) {
            var playlistElement = _a[_i];
            _loop_1(playlistElement);
        }
        menu.appendChild(header);
        menu.appendChild(list);
        return menu;
    };
    PlaylistMenu.prototype.update = function () {
        var options = this.getOptions();
        this.updateSelected(options.getCurrentPosition());
    };
    PlaylistMenu.prototype.open = function () {
        this.player().addClass('playlist-menu-displayed');
    };
    PlaylistMenu.prototype.close = function () {
        this.player().removeClass('playlist-menu-displayed');
    };
    PlaylistMenu.prototype.updateSelected = function (newPosition) {
        for (var _i = 0, _a = this.menuItems; _i < _a.length; _i++) {
            var item = _a[_i];
            item.setSelected(item.getElement().position === newPosition);
        }
    };
    PlaylistMenu.prototype.getOptions = function () {
        return this.options_;
    };
    PlaylistMenu.prototype.onItemClicked = function (element) {
        this.getOptions().onItemClicked(element);
    };
    return PlaylistMenu;
}(Component));
exports.PlaylistMenu = PlaylistMenu;
Component.registerComponent('PlaylistMenu', PlaylistMenu);
