"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.PlaylistMenuItem = void 0;
var video_js_1 = require("video.js");
var utils_1 = require("../utils");
var Component = video_js_1["default"].getComponent('Component');
var PlaylistMenuItem = /** @class */ (function (_super) {
    __extends(PlaylistMenuItem, _super);
    function PlaylistMenuItem(player, options) {
        var _this = _super.call(this, player, options) || this;
        _this.emitTapEvents();
        _this.element = options.element;
        _this.on(['click', 'tap'], function () { return _this.switchPlaylistItem(); });
        _this.on('keydown', function (event) { return _this.handleKeyDown(event); });
        return _this;
    }
    PlaylistMenuItem.prototype.createEl = function () {
        var options = this.options_;
        var li = _super.prototype.createEl.call(this, 'li', {
            className: 'vjs-playlist-menu-item',
            innerHTML: ''
        });
        if (!options.element.video) {
            li.classList.add('vjs-disabled');
        }
        var positionBlock = _super.prototype.createEl.call(this, 'div', {
            className: 'item-position-block'
        });
        var position = _super.prototype.createEl.call(this, 'div', {
            className: 'item-position',
            innerHTML: options.element.position
        });
        positionBlock.appendChild(position);
        li.appendChild(positionBlock);
        if (options.element.video) {
            this.buildAvailableVideo(li, positionBlock, options);
        }
        else {
            this.buildUnavailableVideo(li);
        }
        return li;
    };
    PlaylistMenuItem.prototype.setSelected = function (selected) {
        if (selected)
            this.addClass('vjs-selected');
        else
            this.removeClass('vjs-selected');
    };
    PlaylistMenuItem.prototype.getElement = function () {
        return this.element;
    };
    PlaylistMenuItem.prototype.buildAvailableVideo = function (li, positionBlock, options) {
        var videoElement = options.element;
        var player = _super.prototype.createEl.call(this, 'div', {
            className: 'item-player'
        });
        positionBlock.appendChild(player);
        var thumbnail = _super.prototype.createEl.call(this, 'img', {
            src: window.location.origin + videoElement.video.thumbnailPath
        });
        var infoBlock = _super.prototype.createEl.call(this, 'div', {
            className: 'info-block'
        });
        var title = _super.prototype.createEl.call(this, 'div', {
            innerHTML: videoElement.video.name,
            className: 'title'
        });
        var channel = _super.prototype.createEl.call(this, 'div', {
            innerHTML: videoElement.video.channel.displayName,
            className: 'channel'
        });
        infoBlock.appendChild(title);
        infoBlock.appendChild(channel);
        if (videoElement.startTimestamp || videoElement.stopTimestamp) {
            var html = '';
            if (videoElement.startTimestamp)
                html += utils_1.secondsToTime(videoElement.startTimestamp);
            if (videoElement.stopTimestamp)
                html += ' - ' + utils_1.secondsToTime(videoElement.stopTimestamp);
            var timestamps = _super.prototype.createEl.call(this, 'div', {
                innerHTML: html,
                className: 'timestamps'
            });
            infoBlock.append(timestamps);
        }
        li.append(thumbnail);
        li.append(infoBlock);
    };
    PlaylistMenuItem.prototype.buildUnavailableVideo = function (li) {
        var block = _super.prototype.createEl.call(this, 'div', {
            className: 'item-unavailable',
            innerHTML: this.player().localize('Unavailable video')
        });
        li.appendChild(block);
    };
    PlaylistMenuItem.prototype.handleKeyDown = function (event) {
        if (event.code === 'Space' || event.code === 'Enter') {
            this.switchPlaylistItem();
        }
    };
    PlaylistMenuItem.prototype.switchPlaylistItem = function () {
        var options = this.options_;
        options.onClicked();
    };
    return PlaylistMenuItem;
}(Component));
exports.PlaylistMenuItem = PlaylistMenuItem;
Component.registerComponent('PlaylistMenuItem', PlaylistMenuItem);
