"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.BezelsPlugin = void 0;
var video_js_1 = require("video.js");
require("./pause-bezel");
var Plugin = video_js_1["default"].getPlugin('plugin');
var BezelsPlugin = /** @class */ (function (_super) {
    __extends(BezelsPlugin, _super);
    function BezelsPlugin(player, options) {
        var _this = _super.call(this, player) || this;
        _this.player.ready(function () {
            player.addClass('vjs-bezels');
        });
        player.addChild('PauseBezel', options);
        return _this;
    }
    return BezelsPlugin;
}(Plugin));
exports.BezelsPlugin = BezelsPlugin;
video_js_1["default"].registerPlugin('bezels', BezelsPlugin);
