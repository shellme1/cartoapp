"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var video_js_1 = require("video.js");
function getPauseBezel() {
    return "\n  <div class=\"vjs-bezels-pause\">\n    <div class=\"vjs-bezel\" role=\"status\" aria-label=\"Pause\">\n      <div class=\"vjs-bezel-icon\">\n        <svg height=\"100%\" version=\"1.1\" viewBox=\"0 0 36 36\" width=\"100%\">\n          <use class=\"vjs-svg-shadow\" xlink:href=\"#vjs-id-1\"></use>\n          <path class=\"vjs-svg-fill\" d=\"M 12,26 16,26 16,10 12,10 z M 21,26 25,26 25,10 21,10 z\" id=\"vjs-id-1\"></path>\n        </svg>\n      </div>\n    </div>\n  </div>\n  ";
}
function getPlayBezel() {
    return "\n  <div class=\"vjs-bezels-play\">\n    <div class=\"vjs-bezel\" role=\"status\" aria-label=\"Play\">\n      <div class=\"vjs-bezel-icon\">\n        <svg height=\"100%\" version=\"1.1\" viewBox=\"0 0 36 36\" width=\"100%\">\n          <use class=\"vjs-svg-shadow\" xlink:href=\"#vjs-id-2\"></use>\n          <path class=\"vjs-svg-fill\" d=\"M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z\" id=\"ytp-id-2\"></path>\n        </svg>\n      </div>\n    </div>\n  </div>\n  ";
}
var Component = video_js_1["default"].getComponent('Component');
var PauseBezel = /** @class */ (function (_super) {
    __extends(PauseBezel, _super);
    function PauseBezel(player, options) {
        var _this = _super.call(this, player, options) || this;
        player.on('pause', function (_) {
            if (player.seeking() || player.ended())
                return;
            _this.container.innerHTML = getPauseBezel();
            _this.showBezel();
        });
        player.on('play', function (_) {
            if (player.seeking())
                return;
            _this.container.innerHTML = getPlayBezel();
            _this.showBezel();
        });
        return _this;
    }
    PauseBezel.prototype.createEl = function () {
        this.container = _super.prototype.createEl.call(this, 'div', {
            className: 'vjs-bezels-content'
        });
        this.container.style.display = 'none';
        return this.container;
    };
    PauseBezel.prototype.showBezel = function () {
        var _this = this;
        this.container.style.display = 'inherit';
        setTimeout(function () {
            _this.container.style.display = 'none';
        }, 500); // matching the animation duration
    };
    return PauseBezel;
}(Component));
video_js_1["default"].registerComponent('PauseBezel', PauseBezel);
