"use strict";
exports.__esModule = true;
exports.UserRole = void 0;
// Keep the order
var UserRole;
(function (UserRole) {
    UserRole[UserRole["ADMINISTRATOR"] = 0] = "ADMINISTRATOR";
    UserRole[UserRole["MODERATOR"] = 1] = "MODERATOR";
    UserRole[UserRole["USER"] = 2] = "USER";
})(UserRole = exports.UserRole || (exports.UserRole = {}));
