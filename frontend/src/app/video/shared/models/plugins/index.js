"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
exports.__esModule = true;
__exportStar(require("./client-hook.model"), exports);
__exportStar(require("./hook-type.enum"), exports);
__exportStar(require("./install-plugin.model"), exports);
__exportStar(require("./manage-plugin.model"), exports);
__exportStar(require("./peertube-plugin-index-list.model"), exports);
__exportStar(require("./peertube-plugin-index.model"), exports);
__exportStar(require("./peertube-plugin-latest-version.model"), exports);
__exportStar(require("./peertube-plugin.model"), exports);
__exportStar(require("./plugin-client-scope.type"), exports);
__exportStar(require("./plugin-package-json.model"), exports);
__exportStar(require("./plugin-playlist-privacy-manager.model"), exports);
__exportStar(require("./plugin-settings-manager.model"), exports);
__exportStar(require("./plugin-storage-manager.model"), exports);
__exportStar(require("./plugin-translation.model"), exports);
__exportStar(require("./plugin-video-category-manager.model"), exports);
__exportStar(require("./plugin-video-language-manager.model"), exports);
__exportStar(require("./plugin-video-licence-manager.model"), exports);
__exportStar(require("./plugin-video-privacy-manager.model"), exports);
__exportStar(require("./plugin.type"), exports);
__exportStar(require("./public-server.setting"), exports);
__exportStar(require("./register-client-hook.model"), exports);
__exportStar(require("./register-client-form-field.model"), exports);
__exportStar(require("./register-server-hook.model"), exports);
__exportStar(require("./register-server-setting.model"), exports);
__exportStar(require("./server-hook.model"), exports);
