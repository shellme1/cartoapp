"use strict";
exports.__esModule = true;
exports.PluginType = void 0;
var PluginType;
(function (PluginType) {
    PluginType[PluginType["PLUGIN"] = 1] = "PLUGIN";
    PluginType[PluginType["THEME"] = 2] = "THEME";
})(PluginType = exports.PluginType || (exports.PluginType = {}));
