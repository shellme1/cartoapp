"use strict";
exports.__esModule = true;
exports.VideoFileMetadata = void 0;
var VideoFileMetadata = /** @class */ (function () {
    function VideoFileMetadata(hash) {
        this.chapters = hash.chapters;
        this.format = hash.format;
        this.streams = hash.streams;
        delete this.format.filename;
    }
    return VideoFileMetadata;
}());
exports.VideoFileMetadata = VideoFileMetadata;
