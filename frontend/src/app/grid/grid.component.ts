import { Directive, Component, OnInit, ComponentFactoryResolver, Injector } from '@angular/core';
import * as L from 'leaflet';
import { formatWithOptions } from 'util';
import {FormTileComponent} from "../form-tile/form-tile.component";
import { ApiService } from '../services/api.service'

@Directive(
  { selector: 'grid-comp' }
)
export class GridComponent{
  interractiveCells : L.Layer;
  cbTileForm : Function;
  clicked : L.Layer;
  alreadyDrawn = {};
  crs;
  existingTiles;
  myTiles;


  constructor(private componentFactoryResolver: ComponentFactoryResolver, private injector: Injector, private api: ApiService) {
    this.interractiveCells = L.featureGroup();
    this.myTiles = [];
  }

  ngOnInit(): void {

  }

  private _init_callback_circles():void{
    const componentFactoryResolver = this.componentFactoryResolver;
    const injector = this.injector;
    const cbTileForm = this.cbTileForm;
    var clicked = undefined;
    this.interractiveCells.on("click", function(e){
      const factory = componentFactoryResolver.resolveComponentFactory(FormTileComponent);
      let comp = factory.create(injector);
      comp.changeDetectorRef.detectChanges();
      comp.instance.init(e.layer._latlng.lat, e.layer._latlng.lng, cbTileForm);
      const form = comp.location.nativeElement;

      clicked = e.layer;
      clicked.setStyle({fillOpacity:0.3});
      clicked.bindPopup(form).openPopup();
      clicked.getPopup().on("remove", function(){
        if(clicked!== undefined){
          clicked.setStyle({fillOpacity:1});
          clicked = undefined;
          comp.destroy();
        }
      })
    });
    this.interractiveCells.on("mouseover", function(e){
      e.layer.setStyle({fillOpacity:0.3});
    });

    this.interractiveCells.on("mouseout", function(e){
      if(clicked !== e.layer){
        e.layer.setStyle({fillOpacity:1});
      }
    });
  }

  public change(bounds:L.latLngBounds):void{
    const _pntSouthWest = this.crs.latLngToPoint( bounds._southWest, 15.5)
    const _pntNorthEast = this.crs.latLngToPoint( bounds._northEast, 15.5)
    for(let startX=Math.round(_pntSouthWest.x)-(Math.round(_pntSouthWest.x)%1000); startX<_pntNorthEast.x; startX+=1000){
      for(let startY=Math.round(_pntNorthEast.y)-(Math.round(_pntNorthEast.y)%1404); startY<_pntSouthWest.y; startY+=1404){
        const cir = L.circle(this.crs.pointToLatLng(L.point(startX, startY), 15.5), {radius:200}).setStyle({color:"#fcb03f", fillOpacity:1});
        const key = startX.toString()+";"+startY.toString();
        if(!(key in this.alreadyDrawn)){
          this.interractiveCells.addLayer(cir);
          this.alreadyDrawn[key] = cir;
        }
      }
    }
  }

  public init(cbFormUrl, crs){
    this.crs = crs;
    this.cbTileForm = cbFormUrl;
    this._init_callback_circles();
  }

  private maskFreePoints(){
    this.existingTiles.forEach(tile => {
      const centerPntxy = this.crs.latLngToPoint( L.latLng(tile.centerLat, tile.centerLng), 15.5);
      this.alreadyDrawn[Math.round(centerPntxy.x).toString()+";"+Math.round(centerPntxy.y).toString()] = 1;
      }
    );
  }



  public setExistingTiles(existingTiles){
    this.existingTiles = existingTiles;
    this.maskFreePoints();
  }

  public populate(bounds:L.latLngBounds):void{
    this.maskFreePoints();
    this.change(bounds);
  }

  public clean():void{
    this.interractiveCells.clearLayers();
    this.alreadyDrawn = {};
  }

}
