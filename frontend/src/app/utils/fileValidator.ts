import { FormControl } from '@angular/forms';

export class FileValidator {

  /**
   * extensions must not contain dot
   */
  static fileExtensions(allowedExtensions: Array<string>){
    const validatorFn = (filename: string) => {
      if (allowedExtensions.length === 0) {
        return null;
      }
        const ext = FileValidator.getExtension(filename);
        if (allowedExtensions.indexOf(ext) === -1) {
            return { fileExtension: { allowedExtensions: allowedExtensions} };
        }
    };
    return FileValidator.fileValidation(validatorFn);
  }

  private static getExtension(filename: string): null|string {
    if (filename.indexOf('.') === -1) {
      return null;
    }
    return filename.split('.').pop();
  }

  private static fileValidation(validatorFn: (File) => null|object){
    return (formControl: FormControl) => {
      if (!formControl.value) {
        return null;
      }

      const files: File[] = [];
      const isMultiple = Array.isArray(formControl.value);
      isMultiple
        ? formControl.value.forEach((file: File) => files.push(file))
        : files.push(formControl.value);

      for (const file of files) {
        return validatorFn(file);
      }

      return null;
    };
  }

}