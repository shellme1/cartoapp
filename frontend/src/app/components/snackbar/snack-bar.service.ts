import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
  
@Injectable({
  providedIn: 'root'
})
export class SnackBarService {
  
//create an instance of MatSnackBar
  
  constructor(private snackBar:MatSnackBar) { }
  
/* It takes three parameters 
    1.the message string 
    2.the action 
    3.the duration, alignment, etc. */
  
  openSnackBarSuccess(message: string, action: string) {
    this.snackBar.open(message, action, {
        duration: 5000,
        panelClass: ['mat-toolbar', 'mat-success']
    });
  }

  openSnackBarError(message: string, action: string) {
    this.snackBar.open(message, action, {
       duration: 5000,
       panelClass: ['mat-toolbar', 'mat-warn']
    });
  }

  openSnackBarPanic(message: string, action: string) {
    this.snackBar.open(message, action, {
       duration: 50000000,
       panelClass: ['mat-toolbar', 'mat-warn']
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
       duration: 5000,
       panelClass: ['mat-toolbar', 'mat-primary']
    });
  }
}