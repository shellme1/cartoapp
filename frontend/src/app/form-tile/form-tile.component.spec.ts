import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTileComponent } from './form-tile.component';

describe('FormTileComponent', () => {
  let component: FormTileComponent;
  let fixture: ComponentFixture<FormTileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
