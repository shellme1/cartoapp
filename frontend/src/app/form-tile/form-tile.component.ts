import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {FileValidator} from "../utils/fileValidator";
import {SnackBarService} from '../components/snackbar/snack-bar.service';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-form-tile',
  templateUrl: './form-tile.component.html',
  styleUrls: ['./form-tile.component.scss']
})
export class FormTileComponent implements OnInit {
  cbVideoValidationFormUrl : Function;

  imagePath;
  formData;
  formVideo;
  isDragover;
  allowedExtensions;
  isValidVideoUrl;
  regexUrl;
  tileId;
  centerLat;
  centerLng;


  constructor(private ref: ChangeDetectorRef, private snackBService: SnackBarService, private apiService: ApiService) {
    this.tileId = undefined;
    this.centerLat = undefined;
    this.centerLng = undefined;

    this.isDragover = 0;
    this.allowedExtensions = ['jpg','jpeg','png', "PNG", "JPG", "JPEG"];
    this.isValidVideoUrl = false;
    this.regexUrl = new RegExp(/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi);
  }
  
  get errorMessage() { return this._errorMessage; }
  private _errorMessage = 'No Error';

  ngOnInit(): void {
    this.formData = new FormGroup({
      imagePath:new FormControl("", [
        Validators.required,
        FileValidator.fileExtensions(this.allowedExtensions),
      ])
    })
    this.formVideo = new FormGroup({
      peertubeUrl :new FormControl("")
    })
  }

  //Callback triggered when the input change
  changeImgPath(event){
    const file = event.target.files[0]
    const error = this.formData.getError("fileExtension","imagePath");
    if(error != null){
      this.snackBService.openSnackBar("Accepted extensions are : "+error.allowedExtensions.toString(), "OK");
    }
    else{
      let uploadData = new FormData();
      uploadData.append('file', file);
      uploadData.append('centerLng', this.centerLng);
      uploadData.append('centerLat', this.centerLat);
      this.apiService.newTileRequest(uploadData)
      .subscribe(
        (response:any) => {
          if(response.hasOwnProperty("tile_id")){
            this.tileId = response.tile_id;
            document.getElementById("formImage").style.display="none";
            document.getElementById("formVideo").style.display="block";
            this.snackBService.openSnackBarSuccess(response.message, "OK");
          }else{
            this.snackBService.openSnackBarError(response.message, "OK");
          }
        },
        (error) => {
          if(error.hasOwnProperty("networkError")){
            this.snackBService.openSnackBarError("Network error occured...", "OK");
          }else{
            if(error.status === 403){
              this.snackBService.openSnackBarError("You must be login to perform this action !", "OK");
            }else{
              this.snackBService.openSnackBarError("Network error occured...", "OK");
            }
            
          }
        }
      )
    }
    document.getElementById("file-input").blur();
    this.ref.detectChanges();
  }

  triggerChange(){
    this.ref.detectChanges()
  }

  cbChangeUrl(value){
    if(value.match(this.regexUrl)){
      this.isValidVideoUrl = true;
    }else{
      this.isValidVideoUrl = false;
    }
    this.triggerChange();
  }

  sendUpdateVideoUrl(peertubeUrl){
    this.apiService.sendVideoUrl(this.tileId, peertubeUrl)
    .subscribe(
      (response:any) => {
        this.snackBService.openSnackBarSuccess("Success", "OK");
        if(response.hasOwnProperty("skipped")){
          this.cbVideoValidationFormUrl(this.tileId, undefined, undefined);
        }else{
          if(response.hasOwnProperty("previewVideoUrl") && response.hasOwnProperty("imageUrl")){
            
            const previewVideoUrl = response.previewVideoUrl;
            const imageUrl = response.imageUrl;
            this.cbVideoValidationFormUrl(this.tileId, previewVideoUrl, imageUrl);
          }
        }
      },
      (error) => {
        if(error.hasOwnProperty("networkError")){
          this.snackBService.openSnackBarError("Network error", "OK");
        }else{
          if(error.status === 500){
            this.snackBService.openSnackBarError(error.error.detail, "OK");
          }
        }
      }
    )
    this.ref.detectChanges();
  }

  skipVideoUrl(){
    this.sendUpdateVideoUrl("");
  }

  dragEnterImage(){
    this.isDragover+=1;
    this.ref.detectChanges();
  }

  dragLeaveImage(){
    this.isDragover-=1;
    this.ref.detectChanges();
  }
  onClickSubmitVideoForm(data){
    this.sendUpdateVideoUrl(data.peertubeUrl);
  }

  init(latCenter, lngCenter, cbValidationForm){
    this.cbVideoValidationFormUrl = cbValidationForm;
    this.centerLat = latCenter;
    this.centerLng = lngCenter;
  }

}
