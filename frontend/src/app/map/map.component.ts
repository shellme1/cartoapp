import { AfterViewInit, Component, HostListener,ElementRef } from '@angular/core';
import {ViewChild, Injectable, Renderer2, RendererFactory2} from '@angular/core';
import {AdminControlButton} from "../controls/buttonAdmin";
import {LocationIndicatorControl} from "../controls/locationIndicator";
import * as L from 'leaflet';
import 'leaflet.markercluster';
import {VideoComponent} from '../video/video.component'
import {GridComponent} from '../grid/grid.component';
import {ImageManagerComponent} from '../controls/image-manager/image-manager.component';
import { LoginFormComponent } from '../controls/login-form/login-form.component';
import { AuthService } from '../services/auth.service';
import { ApiService } from '../services/api.service';
import { ConfigService } from '../services/config.service';
import { ChangeDetectorRef } from '@angular/core';
import {SnackBarService} from '../components/snackbar/snack-bar.service';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

export class MapComponent implements AfterViewInit {
  private map;
  private buttonAdmin;
  private imageOverlays;
  private showAdminGrid : boolean;
  private cluster;
  private buttonLogout;
  public placeVideo : boolean;
  public askLogin : boolean;
  public existingTiles;
  public myTiles;
  public deletableCells : L.Layer;

  //Callbacks for our components
  public boundCallBackButtonAdmin : Function;
  public boundCallBackButtonLogout : Function;
  public boundCallBackValidTileForm : Function;
  public boundCallBackImageManager : Function;

  @ViewChild(GridComponent) myGridComp;
  @ViewChild(ImageManagerComponent) myImageManager;
  public showImageManager : boolean;
  @ViewChild(LoginFormComponent) myLoginFormComponent;
  public showLoginForm : boolean;

  constructor(private renderer: Renderer2, 
    private authService: AuthService, 
    private apiService: ApiService,
    private configService: ConfigService, 
    private snackBService: SnackBarService,
    private ref: ChangeDetectorRef) { 
    this.showImageManager = false;
    this.showAdminGrid = false;
    this.existingTiles = [];
    this.myTiles = [];
    this.imageOverlays = L.layerGroup();
    this.deletableCells = L.featureGroup();
    this.cluster = undefined;
    this.initCluster();
  }

  private initCluster(){
    if(this.cluster === undefined){
      this.cluster = L.markerClusterGroup({
        disableClusteringAtZoom: 14,
        showCoverageOnHover:false,
        spiderfyOnMaxZoom:false,
        animate:true,
        singleMarkerMode:true,
      });
    }else{
      this.cluster.clearLayers()
    }

  }

  private disableInteraction(elemToShow){
    this.map.dragging.disable();
    this.map.touchZoom.disable();
    this.map.doubleClickZoom.disable();
    this.map.scrollWheelZoom.disable();
    this.map.boxZoom.disable();
    this.map.keyboard.disable();
    if (this.map.tap) this.map.tap.disable();
    for(var key in this.map._layers){
      if(this.map._layers.hasOwnProperty(key)){
        if(typeof this.map._layers[key].setOpacity === "function"){
          this.map._layers[key].setOpacity(0.4);
        }
      }
    };
    this.hideImageLayer();
    var controls = document.getElementsByClassName("leaflet-control") as HTMLCollectionOf<HTMLElement>;
    for(var key in controls){
      if(controls[key].style !== undefined){
        if(controls[key].id !== elemToShow.control._container.id){
          controls[key].style.display="none";
        }
      }
    };
    this.toggleButtonAdmin();
  }

  private enableInteraction(elemToRemove){
    this.map.dragging.enable();
    this.map.touchZoom.enable();
    this.map.doubleClickZoom.enable();
    this.map.scrollWheelZoom.enable();
    this.map.boxZoom.enable();
    this.map.keyboard.enable();
    if (this.map.tap) this.map.tap.enable();
    for(var key in this.map._layers){
      if(typeof this.map._layers[key].setOpacity === "function"){
        this.map._layers[key].setOpacity(1);
      }
    };
    this.showImageLayer();
    var controls = document.getElementsByClassName("leaflet-control") as HTMLCollectionOf<HTMLElement>;
    for(var key in controls){
      if(controls[key].style !== undefined){
        if(controls[key].id !== elemToRemove.control._container.id){
          controls[key].style.display="block";
        }else{
          controls[key].style.display="none";
        }
      }
    };
  }
  
  private initMap(): void {
    var defaultView = [50.127, 2.3166];
    var defaultZoom = 6;
    if(window.location.hash) {
      
      const pos = window.location.hash.replace("#", "").split(",")
      if (pos.length > 1){
        const lat = parseFloat(pos[0]);
        const lng = parseFloat(pos[1]);
        
        if(lat !== NaN && lng !== NaN){
          if(Math.abs(lat) <= 90 && Math.abs(lng) <=180){
            defaultView = [lat, lng];
          }
        }
        if (pos.length > 2){
          const askZoom = parseInt(pos[2]);
          if(askZoom!==NaN){
            defaultZoom = askZoom;
          }
        }
      }
    }

    this.map = L.map('map', {
      drawControl:true,
      doubleClickZoom: false,
      zoomSnap: 0.75,
      maxBoundsViscosity: 1.0,
      minZoom:this.configService.getMinZoom(),
      maxZoom:this.configService.getMaxZoom(),
      attributionControl: false,
    }).setView(defaultView, defaultZoom);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      className: "map-tiles"
    }).addTo(this.map);
    
    document.body.classList.add("dark-theme")

    this.map.setMaxBounds(  [[-90,-180],   [90,180]]  )
    this.map.addLayer(this.imageOverlays);
    this.map.on("moveend", function(e){
      window.location.hash = "#"+e.target.getCenter().lat.toFixed(6).toString()+","+e.target.getCenter().lng.toFixed(6).toString()+","+e.target.getZoom()
    })
    const refLayerImage = this.imageOverlays;
    const refCluster = this.cluster;
    const refMap = this.map;
    
    this.map.on('zoomend', function () {
      if (refMap.getZoom() < 9 && refMap.hasLayer(refLayerImage)) {
        refMap.removeLayer(refLayerImage);
      }
      if (refMap.getZoom() > 9 && refMap.hasLayer(refLayerImage) == false){
        refMap.addLayer(refLayerImage);
      }
      if(refMap.hasLayer(refCluster) == false){
        refMap.addLayer(refCluster);
      }
      if (refMap.getZoom() >= 12 && refMap.hasLayer(refCluster)){
        refMap.removeLayer(refCluster)
      }
  });
  }

  private initGridComponent(): void{
    this.map.addLayer(this.myGridComp.interractiveCells);
    this.map.addLayer(this.deletableCells);
    this.myGridComp.init(this.boundCallBackValidTileForm, this.map.options.crs  );
  }

  private _handleZoom():boolean{
    this.map.options.minZoom = 12;
    if(this.map.getZoom() < 12 || this.map.getZoom() > 15){
      this.map.flyTo(this.map.getCenter(), 12,{
        "animate":true,
        "duration":0.5
      })
      return true
    }
    return false
  }

  private toggleButtonAdmin(){
    const setColor = "#eb8634";
    if(this.buttonAdmin._touch){
      this.buttonAdmin.style.backgroundColor = "#ffffff";
      document.getElementById("logoutButton").style.display = "none";
    }else{
      this.buttonAdmin.style.backgroundColor = setColor;
      document.getElementById("logoutButton").style.display = "block";
    }
    this.buttonAdmin._touch = !this.buttonAdmin._touch;
  }

  private hideImageLayer(){
    this.imageOverlays.getLayers().forEach(image => {
      image.setOpacity(0.4);
    });
  }

  private showImageLayer(){
    this.imageOverlays.getLayers().forEach(image => {
      image.setOpacity(1);
    });
  } 

  private toggleGridAdmin(){
    this.toggleButtonAdmin();
    this.showAdminGrid = !this.showAdminGrid;
    
      if(this.showAdminGrid){
        const cpMap = this.map;
        this.hideImageLayer();
        this.createDeletableCells();
        const cpGridComp = this.myGridComp;    
        const shouldTO = this._handleZoom();  
        setTimeout(() => 
        {
          this.myGridComp.populate(cpMap.getBounds(), cpMap.options.crs);
          this.map.on("move", function(){
            cpGridComp.change(cpMap.getBounds(), cpMap.options.crs)
          });
        },
        shouldTO ? 500:0);
      }else{
        this.showImageLayer();
        this.myGridComp.clean();
        this.deletableCells.clearLayers();
        this.map.off("move");
        this.map.options.maxZoom = this.configService.getMaxZoom();
        this.map.options.minZoom = this.configService.getMinZoom();
      }
  }

  private createDeletableCells(){
    this.apiService.getMyTilesRequest()
    .subscribe(
      (response:any) => {
        if(response.hasOwnProperty("tiles")){
          this.existingTiles.forEach(tile => {
            if(response.tiles.includes(tile.id)){
              const deleteIcon = new L.divIcon({className:"custom-div-icon",html: "<div></div><i style='color:#F22613; font-size:56px'class='material-icons'>clear</i>", iconAnchor: [25,25]})
              var delMarker = L.marker([tile.centerLat,tile.centerLng],{icon:deleteIcon});
              delMarker._tileId = tile.id;
              this.deletableCells.addLayer(delMarker);
            }
          });
        }
      },
      (error) => {
        this.myTiles = [];
      }
    )
    
  }

  private _callbackCloseAdminForm(ctx, success):void{
    ctx.enableInteraction(ctx.myLoginFormComponent);
    this.showLoginForm = false;
  }

  private _callbackImageManager(ctx):void{
    ctx.enableInteraction(ctx.myImageManager)
    this.showImageManager = false;
  }

  private _callbackAdmin(e, button){
    
    this.buttonAdmin = button;
    if(this.authService.checkAuthenticated()){
      this.toggleGridAdmin();
    }else{
      this.showLoginForm = true;
      this.ref.detectChanges();
      this.map.addControl(this.myLoginFormComponent.getControl({}));
      this.myLoginFormComponent.init(this._callbackCloseAdminForm, this);
      this.disableInteraction(this.myLoginFormComponent);
      this.toggleButtonAdmin();
    }
  }

  private _callbackLogout(e, button){
    localStorage.clear()
    location.reload();
  }

  private _showImageManager(tileId, previewUrl, imageUrl):void{
    this.showImageManager = true;
    this.ref.detectChanges();
    const imageUri = this.apiService.getUrl()+"images/"+imageUrl;
    const previewUri = this.apiService.getUrl()+"images/"+previewUrl
    this.toggleGridAdmin();
    this.myImageManager.init(tileId, previewUri, imageUri, this.boundCallBackImageManager, this);
    
    this.map.addControl(this.myImageManager.getControl({
      width:this.map._size.x, 
      height:this.map._size.y, 
      MAX_WIDTH:this.configService.getMaxWidth(), 
      MAX_HEIGHT:this.configService.getMaxHeight(),
    }));
    this.disableInteraction(this.myImageManager);
    
  }


  //Calback called when the user press skip or validate form URL peertube
  //If the user press skip, previewUrl contains None
  //Else, we have to fireup the image-manager component with previewUrl
  private _callbackForm(tileId, previewUrl, imageUrl){
    if(previewUrl !== undefined){
      this._showImageManager(tileId, previewUrl, imageUrl);
    }else{
      this.initExistingTiles();
      this.toggleGridAdmin();

    }
    this.map.closePopup();
  }

  private initControls():void{
    //Setup our callbacks...
    this.boundCallBackValidTileForm = this._callbackForm.bind(this);
    this.boundCallBackButtonAdmin = this._callbackAdmin.bind(this);
    this.boundCallBackButtonLogout = this._callbackLogout.bind(this);
    this.boundCallBackImageManager = this._callbackImageManager.bind(this);

    //Setup controls permanents controls...
    this.map.addControl(new AdminControlButton({onEdit:this.boundCallBackButtonAdmin, onLogout:this.boundCallBackButtonLogout}));
    const locationIndicator = new LocationIndicatorControl();
    this.map.addControl(locationIndicator);
    locationIndicator.update(undefined);
  }

  private panic(){
    
    this.snackBService.openSnackBarPanic("Impossible de contacter le serveur", "OK");
    this.disableInteraction(1);
  }

  private async createVideo(x, y, width, height, url, videoId, poster){
    let vidComp = new VideoComponent(undefined, url, videoId);
    await vidComp.init()
    const bounds = [this.map.options.crs.pointToLatLng(L.point(x, y), 15.5), this.map.options.crs.pointToLatLng(L.point(x+width, y+height), 15.5)]
    vidComp.playerElement.style.objectFit="fill";
    vidComp.playerElement.controls=true;
    var videoOverlay = L.videoOverlay( vidComp.playerElement, bounds, {
        opacity: 1,
        interactive:true,
    })

    this.imageOverlays.addLayer(videoOverlay)
    videoOverlay.setZIndex(100);
    videoOverlay.getElement().poster = poster
  }

  private async drawTiles(){
    this.initCluster()
    this.existingTiles.forEach(tile => {
      const crs = this.map.options.crs;
      const centerPnt = crs.latLngToPoint({lat:tile.centerLat, lng:tile.centerLng}, 15.5);
      const bounds = [crs.pointToLatLng(L.point(centerPnt.x-(1000/2), centerPnt.y+(1404/2)), 15.5), crs.pointToLatLng(L.point(centerPnt.x+(1000/2), centerPnt.y-(1404/2)), 15.5)]
      const image = L.imageOverlay(this.apiService.getUrl()+"images/"+tile.imageName, bounds);
      this.cluster.addLayer(L.marker(crs.pointToLatLng(L.point(centerPnt.x, centerPnt.y), 15.5), { clickable: false, icon: L.divIcon({})}));
      const previewUri = this.apiService.getUrl()+"images/"+tile.videoPoster
      
      if(tile.vidTopLeftX !== null){
        this.createVideo(centerPnt.x-(1000/2)+tile.vidTopLeftX, centerPnt.y-(1404/2)+tile.vidTopLeftY, tile.vidWidth, tile.vidHeight, tile.videoUrl, tile.videoId, previewUri);
      }
      this.imageOverlays.addLayer(image)
      
    });
    if (this.map.getZoom() > 9){
      if(this.map.hasLayer(this.imageOverlays) == false){
        this.map.addLayer(this.imageOverlays);
      }
    }
    if (this.map.getZoom() < 12){
      this.map.addLayer(this.cluster)
    }
  }

  private initExistingTiles(){
    
    this.apiService.getTilesRequest()
    .subscribe(
      (response:any) => {
        if(response.hasOwnProperty("tiles")){
          if(response.tiles.length !== 0){
            this.existingTiles = response.tiles;
            this.drawTiles();
          }else{
            this.existingTiles = []
          }
          this.myGridComp.setExistingTiles(this.existingTiles);
        }
      },
      (error) => {
        this.panic()
      }
    )

    this.apiService.getMyTilesRequest()
    .subscribe(
      (response:any) => {
        if(response.hasOwnProperty("tiles")){
          this.myTiles = response.tiles;
        }
      },
      (error) => {
        this.myTiles = [];
      }
    )
  }

  private initDeletableLayer():void{
    const apiService = this.apiService;
    const ref = this;
    this.deletableCells.on("click", function(e){
      apiService.removeTileRequest(e.layer._tileId).subscribe(
        (response:any) => {
          location.reload()
        },
        (error) => {

        }
    )});
  }

  ngAfterViewInit(): void {
    this.initMap();
    this.initControls();
    this.initGridComponent();
    this.initExistingTiles();
    this.initDeletableLayer();
  }
}