import * as L from 'leaflet';

export var AdminControlButton = L.Control.extend({
    options: {
      position: 'topright'
    },
    initialize: function (options) {
      this._button = {};
      this.setButton(options);
    },
  
    onAdd: function (map) {
      this._map = map;

      this._container = L.DomUtil.create('div', 'leaflet-control-button leaflet-bar btn-group');
  
      this._update();
      return this._container;
    },
  
    onRemove: function (map) {
      this._button = {};
      this._update();
    },
  
    setButton: function (options) {
      var button = {
        'icon':"mode_edit",
        'onEdit': options.onEdit,
        'onLogout' : options.onLogout,
      };
      this._button = button;
      this._update();
    },
  
    _update: function () {
      if (!this._map) {
        return;
      }
  
      this._container.innerHTML = '';
      this._makeButton(this._button);
    },
  
    _makeButton: function (button) {
      var editButton = L.DomUtil.create('a', 'leaflet-buttons-control-button material-icons', this._container);
      editButton.href = '#';
      editButton.id = "adminButton";
      editButton._touch = false;
      editButton.role = "button";
      editButton.innerText='mode_edit';
      editButton.value = button.icon;

      var logoutButton = L.DomUtil.create('a', 'leaflet-buttons-control-button material-icons', this._container);
      logoutButton.href = '#';
      logoutButton.id = "logoutButton";
      logoutButton.role = "button";
      logoutButton.innerText='power_settings_new';
      
      const onClickEdit = function(event) {
        button.onEdit(event, editButton);
        event.preventDefault();
      };

      const onClickLogout = function(event) {
        button.onLogout(event, logoutButton);
        event.preventDefault();
      };
      logoutButton.style.display = "none";
      L.DomEvent.addListener(editButton, 'click', onClickEdit, this);
      L.DomEvent.addListener(logoutButton, 'click', onClickLogout, this);
    }
  
});