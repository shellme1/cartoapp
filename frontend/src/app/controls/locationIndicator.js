import * as L from 'leaflet';

export var LocationIndicatorControl = L.Control.extend({
    options: {
      position: 'bottomright'
    },
    initialize: function (options) {
      this._button = {};
    },
  
    onAdd: function (map) {
      this._map = map;

      this._container = L.DomUtil.create('div', '');
      this._container.style.backgroundColor="#e6a40b";
      this.textLocation = L.DomUtil.create('p', '', this._container);
      this.textLocation.style.fontWeight = "bold";
      this.textLocation.style.fontSize = "12px";
      this.textLocation.style.padding = "2px";
      this.textLocation.style.paddingTop = "10px";
      this.textLocation.style.whiteSpace="nowrap";
      this.textLocation.style.textAlign="center";
      this._container.style.width="100%";
      this._container.style.marginBottom="5px";
      this._container.style.boxShadow="0 0 5px";
      this.init();
      this.set_text(this._map.getCenter());
      return this._container;
    },
  
    onRemove: function (map) {
      this._button = {};
    },
    
    round: function(to_padd, max){
        const decimals = to_padd.split(".");
        var idx = 0;
        if (decimals.length<2){
            to_padd+="."
        }else{
            idx = to_padd.split(".")[1].length;
        }
        while(idx<max){
            to_padd+="0";
            idx+=1;
        }
        return to_padd;
    },

    set_text : function(latLng){
        var displayLat = (Math.round(latLng.lat*1000)/1000).toString();
        var displayLng = (Math.round(latLng.lng*1000)/1000).toString();
        this.textLocation.innerText="Lat "+this.round(displayLat, 3)+" ; Long "+this.round(displayLng, 3);
    },

    update : function(event){
      if(this._container.parentElement.style.left!="50%"){
        //this._container.parentElement.style.marginLeft="50%";
        this._container.parentElement.style.marginRight="40%";
      }
      if(event !== undefined){
        if(event.latlng!=undefined){
          this.set_text(event.latlng);
        }
      }
    },
    init: function () {
      this._map.on('mousemove', this.update, this);
    }
  
});