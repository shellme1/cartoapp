import * as L from 'leaflet';
import { Component, OnInit, HostListener, ElementRef, ViewChild } from '@angular/core';
import {SnackBarService} from '../../components/snackbar/snack-bar.service';
import { ApiService } from '../../services/api.service';
import { ConfigService } from 'src/app/services/config.service';

@Component({
  selector: 'app-image-manager',
  templateUrl: './image-manager.component.html',
  styleUrls: ['./image-manager.component.scss']
})
export class ImageManagerComponent implements OnInit {
  MAX_WIDTH;
  MAX_HEIGHT;

  cbCloseComponent;
  cbCtx;
  tileId;
  previewVideoUri;
  imageUri;
  imgPreview;
  public canSave;
  
  control;
  domImg;
  initialized;
  canvasContext;
  currentImageHeight;
  currentImageWidth;
  drag;
  currentHandle;
  rect;
  handlesSize;
  offsetLeft;
  offsetTop;
  mapDom;
  precedentWidth;
  precedentHeight;
  @ViewChild("canvasPosition")
  canvasPosition: ElementRef<HTMLCanvasElement>;

  constructor(private apiService: ApiService, private snackBService: SnackBarService, private configService: ConfigService) {
    this.initialized = false;
    this.rect={
      x:0,
      y:0,
      w:0,
      h:0,
      initialized:false
    };
    this.canSave = false;
    this.handlesSize = 8;
    this.mapDom = document.getElementById('map');
    this.precedentWidth = 0;
    this.precedentHeight = 0;
  }

  private dist(p1, p2) {
    return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
  }

  private point(x, y){
    return {
      x:x,
      y:y
    }
  }

  mouseDown = (e) => {
    if (this.currentHandle) this.drag = true;
    this.draw();
  }

  mouseUp = (e) => {
    this.drag = false;
    this.currentHandle = false;
    this.draw();
  }

  mouseMove = (e) => {
    var rect = this.canvasPosition.nativeElement.getBoundingClientRect()
    var previousHandle = this.currentHandle;
    if (!this.drag) this.currentHandle = this.getHandle(this.point(e.pageX - rect.left, e.pageY - rect.top));
    if (this.currentHandle && this.drag) {
        var mousePos = this.point(e.pageX - rect.left, e.pageY - rect.top);
        switch (this.currentHandle) {
            case 'topleft':
                this.rect.w += this.rect.x - mousePos.x;
                this.rect.h += this.rect.y - mousePos.y;
                this.rect.x = mousePos.x;
                this.rect.y = mousePos.y;
                break;
            case 'topright':
                this.rect.w = mousePos.x - this.rect.x;
                this.rect.h += this.rect.y - mousePos.y;
                this.rect.y = mousePos.y;
                break;
            case 'bottomleft':
                this.rect.w += this.rect.x - mousePos.x;
                this.rect.x = mousePos.x;
                this.rect.h = mousePos.y - this.rect.y;
                break;
            case 'bottomright':
                this.rect.w = mousePos.x - this.rect.x;
                this.rect.h = mousePos.y - this.rect.y;
                break;

            case 'top':
                this.rect.h += this.rect.y - mousePos.y;
                this.rect.y = mousePos.y;
                break;

            case 'left':
                this.rect.w += this.rect.x - mousePos.x;
                this.rect.x = mousePos.x;
                break;

            case 'bottom':
                this.rect.h = mousePos.y - this.rect.y;
                break;

            case 'right':
                this.rect.w = mousePos.x - this.rect.x;
                break;
            
            case 'center':
                this.rect.x = mousePos.x - this.rect.w / 2;
                this.rect.y = mousePos.y - this.rect.h / 2;
                break;
        }
    }
    if (this.drag || this.currentHandle != previousHandle) this.draw();
  }

  public cbValidateButton(){
    const canvasWidth = this.canvasPosition.nativeElement.width;
    const canvasHeight = this.canvasPosition.nativeElement.height;
    this.apiService.updateTileRequest(this.tileId, this.rect.x, this.rect.y, this.rect.h, this.rect.w, canvasWidth, canvasHeight)
    .subscribe(
      (response:any) => {
        this.snackBService.openSnackBarSuccess("Succès ! Rechargement de la page...", "OK");
        setTimeout(function(){ window.location.reload() }, 1500);
      },
      (error) => {
        if(error.hasOwnProperty("networkError")){
          this.snackBService.openSnackBarError("Network error", "OK");
        }else{
          if(error.status === 500){
            this.snackBService.openSnackBarError(error.error.detail, "OK");
          }
        }
      }
    )
    this.cbCloseComponent(this.cbCtx);
  }

  public cbCancelButton(){
    this.cbCloseComponent(this.cbCtx);
  }

  private draw() {
    const width = this.canvasPosition.nativeElement.clientWidth;
    const height = this.canvasPosition.nativeElement.clientHeight;
    this.canvasPosition.nativeElement.height = this.domImg.clientHeight;
    this.canvasPosition.nativeElement.width = this.domImg.clientWidth;
    if(this.precedentHeight != this.mapDom.clientHeight || this.precedentWidth != this.mapDom.clientWidth){
      const new_width = this.mapDom.clientWidth < this.configService.getMaxWidth() ? this.mapDom.clientWidth-20 : this.configService.getMaxWidth();
      const new_height = this.mapDom.clientHeight < this.configService.getMaxHeight() ? this.mapDom.clientHeight-20: this.configService.getMaxHeight();
      this.domImg.style.maxWidth = new_width.toString()+"px";
      this.domImg.style.maxHeight = new_height.toString()+"px";
      this.precedentHeight = this.mapDom.clientHeight;
      this.precedentWidth = this.mapDom.clientWidth;
    }
    this.canvasContext.clearRect(0, 0, width, height);
    //If first draw
    const computedX = width/2-50;
    const computedY = height/2-150;

    //Initialize first rectangle
    if(!this.rect.initialized && (computedX>=0 || computedY>=0) ){
      this.rect = {
        x:computedX,
        y:computedY,
        w:150,
        h:50,
        initialized:true
      }
      this.canSave = true;
    }

    //Prevent overlap
    if(this.rect.x < 0){
      this.rect.x += (-1)*this.rect.x;
    }
    if(this.rect.y < 0){
      this.rect.y += (-1)*this.rect.y;
    }
    if(this.rect.x+this.rect.w>width){
      this.rect.x -= (this.rect.x+this.rect.w)-width;
    }
    if(this.rect.y+this.rect.h>height){
      this.rect.y -= (this.rect.y+this.rect.h)-height;
    }
    if(this.rect.w < 20){
      this.rect.w += (20-this.rect.w)
    }
    if(this.rect.h < 20){
      this.rect.h +=(20-this.rect.h)
    }

    if(this.imgPreview !== undefined){
      this.canvasContext.drawImage(this.imgPreview, this.rect.x, this.rect.y, this.rect.w, this.rect.h);   
    }
     
    if (this.currentHandle) {
        var posHandle = this.point(0, 0);
        switch (this.currentHandle) {
            case 'topleft':
                posHandle.x = this.rect.x;
                posHandle.y = this.rect.y;
                break;
            case 'topright':
                posHandle.x = this.rect.x + this.rect.w;
                posHandle.y = this.rect.y;
                break;
            case 'bottomleft':
                posHandle.x = this.rect.x;
                posHandle.y = this.rect.y + this.rect.h;
                break;
            case 'bottomright':
                posHandle.x = this.rect.x + this.rect.w;
                posHandle.y = this.rect.y + this.rect.h;
                break;
            case 'top':
                posHandle.x = this.rect.x + this.rect.w / 2;
                posHandle.y = this.rect.y;
                break;
            case 'left':
                posHandle.x = this.rect.x;
                posHandle.y = this.rect.y + this.rect.h / 2;
                break;
            case 'bottom':
                posHandle.x = this.rect.x + this.rect.w / 2;
                posHandle.y = this.rect.y + this.rect.h;
                break;
            case 'right':
                posHandle.x = this.rect.x + this.rect.w;
                posHandle.y = this.rect.y + this.rect.h / 2;
                break;
            case 'center':
                posHandle.x = this.rect.x + this.rect.w / 2;
                posHandle.y = this.rect.y + this.rect.h / 2;
                break;
        }
        this.canvasContext.globalCompositeOperation = 'xor';
        this.canvasContext.beginPath();
        this.canvasContext.arc(posHandle.x, posHandle.y, this.handlesSize, 0, 2 * Math.PI);
        this.canvasContext.fill();
        this.canvasContext.globalCompositeOperation = 'source-over';
    }
  } 

  private getHandle(mouse) {
      if (this.dist(mouse, this.point(this.rect.x, this.rect.y)) <= this.handlesSize) return 'topleft';
      if (this.dist(mouse, this.point(this.rect.x + this.rect.w, this.rect.y)) <= this.handlesSize) return 'topright';
      if (this.dist(mouse, this.point(this.rect.x, this.rect.y + this.rect.h)) <= this.handlesSize) return 'bottomleft';
      if (this.dist(mouse, this.point(this.rect.x + this.rect.w, this.rect.y + this.rect.h)) <= this.handlesSize) return 'bottomright';
      if (this.dist(mouse, this.point(this.rect.x + this.rect.w / 2, this.rect.y)) <= this.handlesSize) return 'top';
      if (this.dist(mouse, this.point(this.rect.x, this.rect.y + this.rect.h / 2)) <= this.handlesSize) return 'left';
      if (this.dist(mouse, this.point(this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h)) <= this.handlesSize) return 'bottom';
      if (this.dist(mouse, this.point(this.rect.x + this.rect.w, this.rect.y + this.rect.h / 2)) <= this.handlesSize) return 'right';
      if (this.dist(mouse, this.point(this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2)) <= this.handlesSize) return 'center';
      return false;
  }

  ngAfterViewInit(): void{
    this.canvasContext = this.canvasPosition.nativeElement.getContext("2d");
    this.canvasContext.canvas.addEventListener('mousedown', this.mouseDown, false);
    this.canvasContext.canvas.addEventListener('mouseup', this.mouseUp, false);
    this.canvasContext.canvas.addEventListener('mousemove', this.mouseMove, false);
    this.canvasPosition.nativeElement.style.padding="4px";
    this.offsetLeft = this.canvasPosition.nativeElement.offsetLeft;
    this.offsetTop = this.canvasPosition.nativeElement.offsetTop;
    setTimeout(() =>
      this.draw(), 1000
    );
    
  }

  init(tileId, previewVideoUri, imageUri, cbClose, ctx):void{
    this.cbCloseComponent = cbClose;
    this.cbCtx = ctx;
    this.tileId = tileId;
    this.previewVideoUri = previewVideoUri;
    this.imageUri = imageUri;
  }

  ngOnInit(): void {
    var domImgManager = L.DomUtil.get('imageManager');
    this.domImg = domImgManager.childNodes[0].childNodes[0];
  }

  @HostListener('window:resize', ['$event'])
  resizeCb(options) {
    if(this.domImg && this.initialized){
      const mousedownEvent = new Event('mousedown');
      this.draw();
      this.mouseDown(mousedownEvent);
    }
  }

  getControl(options):L.Control{
    const src = this.imageUri;
    this.imgPreview = new Image()
    this.imgPreview.src = this.previewVideoUri
    let controlObj = L.Control.extend({
      initialize(options){
        this.options = options;
        this.cb = options.cb;
      },
      onAdd(map: L.Map) {
        let domImgManager = L.DomUtil.get('imageManager');  
        let domImg = domImgManager.childNodes[0].childNodes[0];
        domImg.src = src;
        
        const new_width = options.width < options.MAX_WIDTH ? options.width-20 : options.MAX_WIDTH;
        const new_height = options.height < options.MAX_HEIGHT ? options.height-20 : options.MAX_HEIGHT;
        domImg.style.maxWidth = new_width.toString()+"px";
        domImg.style.maxHeight = new_height.toString()+"px";
        
        return domImgManager;
      },
      onRemove(map: L.Map) {}
    });
    this.control=new controlObj({
        position: "bottomleft",
    });
    this.initialized = true;
    return this.control;
  }

}
