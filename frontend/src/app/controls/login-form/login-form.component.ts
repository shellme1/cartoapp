import * as L from 'leaflet';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  control;
  initialized;
  form;
  loginInvalid;
  serverError;
  formSubmitAttempt;
  cbClose;
  ctxCb;

  constructor(private fb: FormBuilder, private authService: AuthService) { 
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async ngOnInit(): Promise<void> {

  }

  async onSubmit(): Promise<void>{
    this.loginInvalid = false;
    this.serverError = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const username = this.form.get('username')?.value;
        const password = this.form.get('password')?.value;
        await this.authService.signIn(username, password);
        this.cbClose(this.ctxCb, true);
      } catch (err) {
        if(err.status !== 401){
          this.serverError = true;
        }else{
          this.loginInvalid = true;
        }
        
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }

  inputFieldChange(){
    this.loginInvalid = false;
    this.serverError = false;
  }

  closeForm(){
    this.cbClose(this.ctxCb, false);
  }

  init(cb, ctx){
    this.ctxCb = ctx;
    this.cbClose = cb
    let _container = L.DomUtil.get('formLogin');
    _container.style.marginRight="50%";
  }

  getControl(options):L.Control{
    let controlObj = L.Control.extend({
      initialize(options){
        this.options = options;
        this.cb = options.cb;
      },
      onAdd(map: L.Map) {
        let formLogin = L.DomUtil.get('formLogin');
        formLogin.style.display="block";
        let button = L.DomUtil.get('closeFormLogin');
        L.DomEvent.on(button, 'click', function(e){
          L.DomEvent.stop(e);
        }, this);
        return formLogin;
      },
      onRemove(map: L.Map) {}
    });
    this.control=new controlObj({
        position: "topright",
    });
    this.initialized = true;
    return this.control;
  }

}
